(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-order-detail-add-order-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/add-order-detail/add-order-detail.page.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/add-order-detail/add-order-detail.page.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/orders/{{this.cust_id}}\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Subscription</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\" *ngIf=\"data != null\">\r\n\r\n  <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n    <ion-grid class=\"ion-no-padding\" *ngIf=\"subscriptions != ''\">\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <ion-list class=\"ion-no-margin\">\r\n            <ion-item class=\"ion-margin\" lines=\"none\">\r\n              <!--Content Avatar-->\r\n              <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n                <img [src]=\"data.product_img\">\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding-start\">\r\n                <h5 class=\"text-size-sm text-color-primary font-bold ion-text-wrap capitalize\">\r\n                  {{data.title}}</h5>\r\n                <p class=\"text-size-sm font-regular ion-text-wrap\">‎Per {{data.unit}}</p>\r\n                <p class=\"text-size-sm font-regular ion-text-wrap\">‎Rs. {{data.rate}}</p>\r\n              </ion-label>\r\n\r\n\r\n              <!--  <ion-icon (click)=\"decrementQty()\" name=\"remove-outline\"></ion-icon>\r\n                <ion-text> {{product_qty}} </ion-text>\r\n                <ion-icon (click)=\"incrementQty()\" name=\"add-outline\"></ion-icon> -->\r\n              <!-- <ion-badge class=\"border-radius\" color=\"\" slot=\"start\">\r\n                <ion-icon (click)=\"decrementQty()\" name=\"remove-outline\"></ion-icon>\r\n              </ion-badge>\r\n              <h4 class=\"text-size-sm text-color-primary font-bold\"> {{product_qty}} </h4>\r\n              <ion-badge class=\"border-radius\" color=\"\" slot=\"end\">\r\n                <ion-icon (click)=\"incrementQty()\" name=\"add-outline\"></ion-icon>\r\n              </ion-badge> -->\r\n\r\n              <ion-chip outline>\r\n                <ion-icon (click)=\"decrementQty()\" name=\"remove-outline\"></ion-icon>\r\n                <h5 class=\"text-size-sm text-color-primary font-bold ion-text-wrap capitalize\">{{product_qty}}</h5>\r\n                <ion-icon (click)=\"incrementQty()\" name=\"add-outline\"></ion-icon>\r\n              </ion-chip>\r\n            </ion-item>\r\n          </ion-list>\r\n\r\n          <ion-item class=\"ion-margin\">\r\n            <ion-icon size=\"large\" name=\"calendar-number-outline\" color=\"medium\"></ion-icon>\r\n            <ion-label class=\"ion-margin-start\">\r\n              <h5 class=\"text-size-sm text-color-primary font-bold ion-text-wrap capitalize\">{{dateLabel}}</h5>\r\n            </ion-label>\r\n            <ion-label class=\"text-size-sm text-color-primary\">\r\n              <ion-datetime formControlName=\"start_date\" displayFormat=\"DD-MM-YYYY\"\r\n                placeholder={{schedule_start_date}}>\r\n              </ion-datetime>\r\n            </ion-label>\r\n          </ion-item>\r\n        </ion-col>\r\n          <!-- <ion-item-divider color=\"secondary\"></ion-item-divider> -->\r\n        <ion-col size=\"12\"  *ngIf=\"subscription == 'true'\">\r\n          <ion-item class=\"ion-margin\" lines=\"none\">\r\n            <ion-icon size=\"large\" name=\"repeat-outline\" color=\"medium\"></ion-icon>\r\n            <ion-label class=\"ion-margin-start\">\r\n              <h3>Repeat <ion-text\r\n                  class=\"text-size-sm text-color-primary ion-margin-start ion-text-uppercase text-color-accent\">\r\n                  {{repeat_plan}}</ion-text>\r\n              </h3>\r\n            </ion-label>\r\n\r\n          </ion-item>\r\n\r\n          <ion-item lines=\"none\" class=\"ion-margin\" >\r\n            <ion-chip (click)=\"schedule('daily')\" class=\"schedule_btn\" size=\"small\" color=\"default\">\r\n              Daily</ion-chip>\r\n            <ion-chip (click)=\"schedule('alternate')\" class=\"schedule_btn\" size=\"small\" color=\"default\">\r\n              Alternate</ion-chip>\r\n            <ion-chip (click)=\"schedule('weekends')\" class=\"schedule_btn\" size=\"small\" color=\"default\">\r\n              Weekends</ion-chip>\r\n          </ion-item>\r\n        </ion-col>\r\n      </ion-row>\r\n      <!-- <ion-label class=\"ion-margin\">Choose Days</ion-label> -->\r\n      <ion-row class=\"ion-margin-top\" *ngIf=\"subscription == 'true'\">\r\n        <ion-item *ngFor=\"let item of checkbox\" lines=\"none\">\r\n          <label class=\"coolbox\">\r\n            <input type=\"checkbox\" formControlName=\"{{item.name}}\" (change)=\"getCheckboxValue($event)\"\r\n              name=\"{{item.name}}\" />\r\n            <span>{{item.title}}</span>\r\n          </label>\r\n        </ion-item>\r\n      </ion-row>\r\n\r\n      <!--  <ion-button [routerLink]=\"['/customers/add']\" size=\"small\" shape=\"round\" color=\"secondary\">Confirm</ion-button> -->\r\n    </ion-grid>\r\n    <!-- <ion-footer>\r\n      <ion-toolbar>\r\n        <ion-title class=\"no-margin\"> -->\r\n          <ion-button *ngIf=\"subscription == 'false'\" item-start class=\"default-button border-radius-right capitalize ion-margin-end ion-margin-top\"\r\n            [disabled]=\"!frmFG.valid\" expand=\"block\" size=\"large\" type=\"submit\">\r\n            Buy Once\r\n          </ion-button>\r\n          <ion-button *ngIf=\"subscription == 'true'\" item-end class=\"default-button border-radius-right capitalize ion-margin-end ion-margin-top\" [disabled]=\"!frmFG.valid\"\r\n          expand=\"block\" size=\"large\" type=\"submit\">\r\n            Subscribe\r\n          </ion-button>\r\n        <!-- </ion-title>\r\n      </ion-toolbar>\r\n    </ion-footer> -->\r\n  </form>\r\n</ion-content>\r\n\r\n");

/***/ }),

/***/ "./src/app/pages/orders/add-order-detail/add-order-detail-routing.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/orders/add-order-detail/add-order-detail-routing.module.ts ***!
  \**********************************************************************************/
/*! exports provided: AddOrderDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOrderDetailPageRoutingModule", function() { return AddOrderDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _add_order_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-order-detail.page */ "./src/app/pages/orders/add-order-detail/add-order-detail.page.ts");




const routes = [
    {
        path: '',
        component: _add_order_detail_page__WEBPACK_IMPORTED_MODULE_3__["AddOrderDetailPage"]
    }
];
let AddOrderDetailPageRoutingModule = class AddOrderDetailPageRoutingModule {
};
AddOrderDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddOrderDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/orders/add-order-detail/add-order-detail.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/orders/add-order-detail/add-order-detail.module.ts ***!
  \**************************************************************************/
/*! exports provided: AddOrderDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOrderDetailPageModule", function() { return AddOrderDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _add_order_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-order-detail-routing.module */ "./src/app/pages/orders/add-order-detail/add-order-detail-routing.module.ts");
/* harmony import */ var _add_order_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-order-detail.page */ "./src/app/pages/orders/add-order-detail/add-order-detail.page.ts");








let AddOrderDetailPageModule = class AddOrderDetailPageModule {
};
AddOrderDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _add_order_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddOrderDetailPageRoutingModule"]
        ],
        declarations: [_add_order_detail_page__WEBPACK_IMPORTED_MODULE_6__["AddOrderDetailPage"]]
    })
], AddOrderDetailPageModule);



/***/ }),

/***/ "./src/app/pages/orders/add-order-detail/add-order-detail.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/pages/orders/add-order-detail/add-order-detail.page.scss ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("label.coolbox {\n  background: #1ae5be;\n  padding: 9px;\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n  line-height: 15px;\n  text-align: center;\n  vertical-align: middle;\n  font-weight: bold;\n  font-size: 15px;\n  border-radius: 50%;\n}\n\nlabel.coolbox input {\n  display: none;\n}\n\nlabel.coolbox span {\n  color: white;\n}\n\nlabel.coolbox input:checked + span {\n  color: #0f0f0f;\n}\n\nion-datetime.md.datetime-placeholder.hydrated,\nion-datetime.md.hydrated {\n  margin-left: -17px;\n  margin-top: -8px;\n}\n\n.schedule_btn {\n  border: 1px solid #036f5a;\n  color: #1ae5be;\n  border-radius: 5%;\n  border-color: #1ae5be !important;\n}\n\n.qty_btn {\n  border: 1px solid #1ae5bc34;\n  border-radius: 45%;\n  background-color: #ffffff;\n  color: #1ae5be;\n  padding: 9px 20px;\n  width: 100px;\n}\n\nion-grid {\n  height: 100%;\n  background-color: #ffffff !important;\n}\n\n.capitalize {\n  text-transform: capitalize;\n}\n\n/* ion-badge {\n    --background: var(--cs-background-accent);\n    --color: var(--cs-text-secondary);\n    --padding-bottom: 4px;\n    --padding-end: 4px;\n    --padding-start: 4px;\n    --padding-top: 4px;\n\n} */\n\nion-chip {\n  border-color: #1ae5be !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JkZXJzL2FkZC1vcmRlci1kZXRhaWwvYWRkLW9yZGVyLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFLQTtFQUNJLGFBQUE7QUFGSjs7QUFLQTtFQUNJLFlBQUE7QUFGSjs7QUFLQTtFQUNJLGNBQUE7QUFGSjs7QUFLQTs7RUFFSSxrQkFBQTtFQUNBLGdCQUFBO0FBRko7O0FBS0E7RUFDSSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGdDQUFBO0FBRko7O0FBS0E7RUFDSSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FBRko7O0FBS0E7RUFDSSxZQUFBO0VBQ0Esb0NBQUE7QUFGSjs7QUFLQTtFQUNJLDBCQUFBO0FBRko7O0FBT0E7Ozs7Ozs7O0dBQUE7O0FBVUE7RUFDSSxnQ0FBQTtBQUxKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb3JkZXJzL2FkZC1vcmRlci1kZXRhaWwvYWRkLW9yZGVyLWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJsYWJlbC5jb29sYm94IHtcclxuICAgIGJhY2tncm91bmQ6ICMxYWU1YmU7XHJcbiAgICBwYWRkaW5nOiA5cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcblxyXG4vLyBmb3JtLm5nLXVudG91Y2hlZC5uZy1wcmlzdGluZS5uZy12YWxpZCB7XHJcbi8vICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyB9XHJcbmxhYmVsLmNvb2xib3ggaW5wdXQge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxubGFiZWwuY29vbGJveCBzcGFuIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxubGFiZWwuY29vbGJveCBpbnB1dDpjaGVja2VkK3NwYW4ge1xyXG4gICAgY29sb3I6IHJnYigxNSwgMTUsIDE1KTtcclxufVxyXG5cclxuaW9uLWRhdGV0aW1lLm1kLmRhdGV0aW1lLXBsYWNlaG9sZGVyLmh5ZHJhdGVkLFxyXG5pb24tZGF0ZXRpbWUubWQuaHlkcmF0ZWQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xN3B4O1xyXG4gICAgbWFyZ2luLXRvcDogLThweDtcclxufVxyXG5cclxuLnNjaGVkdWxlX2J0biB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDM2ZjVhO1xyXG4gICAgY29sb3I6ICMxYWU1YmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1JTtcclxuICAgIGJvcmRlci1jb2xvcjogIzFhZTViZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucXR5X2J0biB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMWFlNWJjMzQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0NSU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgY29sb3I6ICMxYWU1YmU7XHJcbiAgICBwYWRkaW5nOiA5cHggMjBweDtcclxuICAgIHdpZHRoOiAxMDBweDtcclxufVxyXG5cclxuaW9uLWdyaWQge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY2FwaXRhbGl6ZSB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG5cclxuXHJcblxyXG4vKiBpb24tYmFkZ2Uge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1jcy1iYWNrZ3JvdW5kLWFjY2VudCk7XHJcbiAgICAtLWNvbG9yOiB2YXIoLS1jcy10ZXh0LXNlY29uZGFyeSk7XHJcbiAgICAtLXBhZGRpbmctYm90dG9tOiA0cHg7XHJcbiAgICAtLXBhZGRpbmctZW5kOiA0cHg7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDRweDtcclxuICAgIC0tcGFkZGluZy10b3A6IDRweDtcclxuXHJcbn0gKi9cclxuXHJcbmlvbi1jaGlwIHtcclxuICAgIGJvcmRlci1jb2xvcjogIzFhZTViZSAhaW1wb3J0YW50O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/orders/add-order-detail/add-order-detail.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/orders/add-order-detail/add-order-detail.page.ts ***!
  \************************************************************************/
/*! exports provided: AddOrderDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOrderDetailPage", function() { return AddOrderDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/schedule.service */ "./src/app/services/schedule.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");








let AddOrderDetailPage = class AddOrderDetailPage {
    constructor(activatedRoute, router, fb, productSer, scheduleSer, notify, datePipe) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.fb = fb;
        this.productSer = productSer;
        this.scheduleSer = scheduleSer;
        this.notify = notify;
        this.datePipe = datePipe;
        this.data = {};
        this.isChecked = true;
        this.weekdays = [];
        this.product_qty = 1;
        this.schedule_start_date = "Tomorrow";
        this.repeat_plan = "daily";
        this.checkbox = [
            { val: 'sun', name: 'Sunday', title: 'S' },
            { val: 'mon', name: 'Monday', title: 'M' },
            { val: 'tues', name: 'Tuesday', title: 'T' },
            { val: 'wed', name: 'Wednesday', title: 'W' },
            { val: 'thu', name: 'Thursday', title: 'T' },
            { val: 'fri', name: 'Friday', title: 'F' },
            { val: 'sat', name: 'Saturday', title: 'S' }
        ];
        this.activatedRoute.params.subscribe((params) => {
            this.cust_id = params['cust_id'];
            this.product_id = params['product_id'];
            this.subscription = params['subscription'];
            if (this.subscription == 'true') {
                this.dateLabel = 'Start date';
            }
            if (this.subscription == 'false') {
                this.dateLabel = 'Date';
            }
            console.log("cust_id", this.subscription);
        });
    }
    ngOnInit() {
        this.formBuilder();
        this.getProduct(this.product_id);
        this.getTomorrowDate();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            start_date: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('Tomorrow'),
            Sunday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true'),
            Monday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true'),
            Tuesday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true'),
            Wednesday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true'),
            Thursday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true'),
            Friday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true'),
            Saturday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('true')
        });
    }
    getTomorrowDate() {
        const today = new Date();
        const tomorrow_date = new Date(today.setDate(today.getDate() + 1));
        this.minDate = this.datePipe.transform(tomorrow_date, 'yyyy-MM-dd');
        return this.minDate;
    }
    getProduct(id) {
        this.productSer.getItem(id)
            .subscribe((res) => {
            this.data = res['product'];
        });
    }
    incrementQty() {
        this.data['rate'] = this.data['rate'] / this.product_qty;
        this.product_qty += 1;
        this.data['rate'] = this.data['rate'] * this.product_qty;
        console.log("data", this.data['rate']);
    }
    decrementQty() {
        if (this.product_qty - 1 < 1) {
            this.product_qty = 1;
        }
        else {
            this.data['rate'] = this.data['rate'] / this.product_qty;
            this.product_qty -= 1;
            this.data['rate'] = this.data['rate'] * this.product_qty;
            console.log("data", this.data['rate']);
        }
    }
    schedule(type) {
        this.repeat_plan = type;
        console.log("this.repeat_plan", this.repeat_plan);
        if (this.repeat_plan == "daily") {
            this.frmFG.patchValue({
                Sunday: true,
                Monday: true,
                Tuesday: true,
                Wednesday: true,
                Thursday: true,
                Friday: true,
                Saturday: true,
            });
        }
        if (this.repeat_plan == "alternate") {
            var first_selected_day = this.frmFG.get("Sunday").value;
            console.log("day", first_selected_day);
            if (first_selected_day == !false) {
                this.frmFG.patchValue({
                    Sunday: false,
                    Monday: true,
                    Tuesday: false,
                    Wednesday: true,
                    Thursday: false,
                    Friday: true,
                    Saturday: false,
                });
            }
            else {
                this.frmFG.patchValue({
                    Sunday: true,
                    Monday: false,
                    Tuesday: true,
                    Wednesday: false,
                    Thursday: true,
                    Friday: false,
                    Saturday: true,
                });
            }
        }
        if (this.repeat_plan == "weekends") {
            this.frmFG.patchValue({
                Sunday: true,
                Monday: false,
                Tuesday: false,
                Wednesday: false,
                Thursday: false,
                Friday: false,
                Saturday: true,
            });
        }
    }
    getCheckboxValue(e) {
        console.log("data", e.target.checked, e.target.value);
    }
    submit() {
        var selectedDays = this.frmFG.value;
        const start_date = this.getTomorrowDate();
        if (selectedDays.start_date == "Tomorrow") {
            selectedDays.start_date = this.datePipe.transform(start_date, 'yyyy-MM-dd');
        }
        if (this.subscription == 'true') {
            for (let item of this.checkbox) {
                if (selectedDays[item.name]) {
                    //this.weekdays.push(item.name)
                    let data = {
                        cust_id: this.cust_id,
                        product_id: this.product_id,
                        unit: this.data['unit'],
                        vendor_id: localStorage.getItem('vendor_id'),
                        weekdays: item.name,
                        quantity: this.product_qty,
                        start_date: selectedDays.start_date
                    };
                    this.scheduleSer.addItem(data)
                        .subscribe((res) => {
                        this.notify.success("Schedule created successfully");
                        console.log("result", res);
                    }, (err) => {
                        this.notify.error("Please try again");
                        console.log("error", err);
                    });
                }
            }
        }
        if (this.subscription == 'false') {
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var d = new Date(selectedDays.start_date);
            var dayName = days[d.getDay()];
            let data = {
                cust_id: this.cust_id,
                product_id: this.product_id,
                unit: this.data['unit'],
                vendor_id: localStorage.getItem('vendor_id'),
                weekdays: dayName,
                quantity: this.product_qty,
                start_date: selectedDays.start_date
            };
            this.scheduleSer.addOneItem(data)
                .subscribe((res) => {
                this.notify.success("Add product successfully");
                console.log("result", res);
            }, (err) => {
                this.notify.error("Please try again");
                console.log("error", err);
            });
        }
        this.router.navigate(['/customers']);
    }
};
AddOrderDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"] },
    { type: _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__["ScheduleService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"] }
];
AddOrderDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-order-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-order-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/add-order-detail/add-order-detail.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-order-detail.page.scss */ "./src/app/pages/orders/add-order-detail/add-order-detail.page.scss")).default]
    })
], AddOrderDetailPage);



/***/ })

}]);
//# sourceMappingURL=add-order-detail-add-order-detail-module.js.map