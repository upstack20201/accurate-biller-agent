(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <!-- Header -->\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>My Profile</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <!-- Content -->\r\n  <ion-content class=\"transparent\">\r\n    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row *ngIf=\"data != null\" class=\"ion-margin-top\">\r\n        <ion-col size=\"12\" class=\"ion-margin-top\">\r\n          <ion-card class=\"ion-margin-top ion-padding avatar-info ion-text-center\r\n        box-shadow border-radius\">\r\n            <ion-thumbnail class=\"border-radius\">\r\n              <img [src]=\"data.image\" alt=\"\">\r\n            </ion-thumbnail>\r\n            <ion-card-content class=\"ion-text-center\">\r\n              <h1 class=\"text-size-xs text-color-primary font-bold ion-text-wrap\">\r\n                {{user_name}}</h1>\r\n              <!--  <p class=\"text-size-sm text-color-primary font-light ion-text-wrap\">\r\n                {{data.email}}</p> -->\r\n              <p class=\"text-size-sm text-color-primary font-light ion-text-wrap\">\r\n                Seller</p>\r\n            </ion-card-content>\r\n          </ion-card>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <!---Form-->\r\n          <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n            <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n                First Name</ion-label>\r\n              <ion-input formControlName=\"firstName\" class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n                [placeholder]=\"\"></ion-input>\r\n              <!-- <i slot=\"end\" class=\"icon icon-pencil icon-font-md icon-color-accent\"></i> -->\r\n\r\n            </ion-item>\r\n            <!-- Last Name -->\r\n            <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Last\r\n                Name</ion-label>\r\n              <ion-input formControlName=\"lastName\" class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n                [placeholder]=\"\"></ion-input>\r\n            </ion-item>\r\n\r\n            <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Email\r\n              </ion-label>\r\n              <ion-input formControlName=\"email\" class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"email\"\r\n                [placeholder]=\"\">\r\n              </ion-input>\r\n            </ion-item>\r\n\r\n            <!-- Contact -->\r\n            <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n                Contact</ion-label>\r\n              <ion-input formControlName=\"mobile\" class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" maxlength=\"10\" [placeholder]=\"\"></ion-input>\r\n            </ion-item>\r\n            <!-- Address Line 1 -->\r\n            <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n                Address</ion-label>\r\n              <ion-input formControlName=\"address\" class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"></ion-input>\r\n            </ion-item>\r\n\r\n            <!-- <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">City\r\n              </ion-label>\r\n              <ion-input formControlName=\"city\" class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n                [placeholder]=\"\"></ion-input>\r\n            </ion-item> -->\r\n            <!-- Zip Code -->\r\n            <ion-item\r\n              class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n              lines=\"none\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n                ZipCode</ion-label>\r\n              <ion-input formControlName=\"zipcode\" class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" maxlength=\"6\"\r\n                [placeholder]=\"\"></ion-input>\r\n            </ion-item>\r\n            <!-- <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n              [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n              {{data.button}}\r\n            </ion-button> -->\r\n            <ion-footer>\r\n              <ion-toolbar>\r\n                <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \"\r\n                  [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n                  Update Profile\r\n                </ion-button>\r\n              </ion-toolbar>\r\n            </ion-footer>\r\n          </form>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-content>\r\n\r\n</ion-app>");

/***/ }),

/***/ "./src/app/pages/profile/profile-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/profile/profile-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function() { return ProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.page */ "./src/app/pages/profile/profile.page.ts");




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/profile/profile.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.module.ts ***!
  \*************************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile-routing.module */ "./src/app/pages/profile/profile-routing.module.ts");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/pages/profile/profile.page.ts");








let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"]
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/pages/profile/profile.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Profile 4\n========================================*/\n:host {\n  --cs-size-thumbnail: 100px;\n}\nion-card {\n  overflow: visible;\n}\nion-card ion-thumbnail {\n  margin: -60px auto 0;\n  width: var(--cs-size-thumbnail, 100px) !important;\n  height: var(--cs-size-thumbnail, 100px) !important;\n}\n.avatar-info {\n  margin-top: 30px;\n}\n/* Style component 2\n========================================================*/\nion-content ion-grid,\nion-content ion-row {\n  height: 100%;\n}\nion-content ion-grid .background-size,\nion-content ion-row .background-size {\n  height: 300px;\n  align-items: flex-end;\n  display: flex;\n}\nion-content ion-grid form,\nion-content ion-row form {\n  /* .default-button {\n     width: 100%;\n     margin: 32px 0 16px 0;\n   }*/\n}\nion-content ion-grid form ion-item,\nion-content ion-row form ion-item {\n  --background: var(--cs-background-secondary) !important;\n  margin: 16px 16px 16px 0;\n  width: calc(100% - 16px);\n}\nion-content ion-grid form ion-item ion-icon,\nion-content ion-row form ion-item ion-icon {\n  position: absolute;\n  right: 10px;\n  bottom: 15px;\n  z-index: 999;\n}\nion-content ion-grid form ion-item ion-label,\nion-content ion-row form ion-item ion-label {\n  transform: none;\n  flex: none !important;\n  min-width: auto !important;\n  width: auto !important;\n  margin: 0 0 0 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTt5Q0FBQTtBQUVBO0VBQ0UsMEJBQUE7QUFDRjtBQUVBO0VBQ0UsaUJBQUE7QUFDRjtBQUNFO0VBQ0Usb0JBQUE7RUFDQSxpREFBQTtFQUNBLGtEQUFBO0FBQ0o7QUFJQTtFQUNFLGdCQUFBO0FBREY7QUFJQTt5REFBQTtBQUtFOztFQUVFLFlBQUE7QUFKSjtBQU1JOztFQUNFLGFBQUE7RUFDQSxxQkFBQTtFQUNBLGFBQUE7QUFITjtBQU1JOztFQXFCQzs7O0tBQUE7QUFwQkw7QUFBTTs7RUFDRSx1REFBQTtFQUNBLHdCQUFBO0VBQ0Esd0JBQUE7QUFHUjtBQURROztFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FBSVY7QUFEUTs7RUFDRSxlQUFBO0VBQ0EscUJBQUE7RUFDQSwwQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUFJViIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBQcm9maWxlIDRcclxuPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXHJcbjpob3N0IHtcclxuICAtLWNzLXNpemUtdGh1bWJuYWlsOiAxMDBweDtcclxufVxyXG5cclxuaW9uLWNhcmQge1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG5cclxuICBpb24tdGh1bWJuYWlsIHtcclxuICAgIG1hcmdpbjogLTYwcHggYXV0byAwO1xyXG4gICAgd2lkdGg6IHZhcigtLWNzLXNpemUtdGh1bWJuYWlsLCAxMDBweCkgIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogdmFyKC0tY3Mtc2l6ZS10aHVtYm5haWwsIDEwMHB4KSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbn1cclxuXHJcbi5hdmF0YXItaW5mbyB7XHJcbiAgbWFyZ2luLXRvcDogMzBweDtcclxufVxyXG5cclxuLyogU3R5bGUgY29tcG9uZW50IDJcclxuPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG5cclxuaW9uLWNvbnRlbnQge1xyXG5cclxuICBpb24tZ3JpZCxcclxuICBpb24tcm93IHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuXHJcbiAgICAuYmFja2dyb3VuZC1zaXplIHtcclxuICAgICAgaGVpZ2h0OiAzMDBweDtcclxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm0ge1xyXG4gICAgICBpb24taXRlbSB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1jcy1iYWNrZ3JvdW5kLXNlY29uZGFyeSkgIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW46IDE2cHggMTZweCAxNnB4IDA7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE2cHgpO1xyXG5cclxuICAgICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICByaWdodDogMTBweDtcclxuICAgICAgICAgIGJvdHRvbTogMTVweDtcclxuICAgICAgICAgIHotaW5kZXg6IDk5OTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICB0cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgICAgICBmbGV4OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBtaW4td2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICAgICAgICAgIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBtYXJnaW46IDAgMCAwIDE2cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgLyogLmRlZmF1bHQtYnV0dG9uIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW46IDMycHggMCAxNnB4IDA7XHJcbiAgICAgIH0qL1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/profile/profile.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/profile/profile.page.ts ***!
  \***********************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/profile-service */ "./src/app/services/profile-service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let ProfilePage = class ProfilePage {
    constructor(fb, profileSer, notify, router, activatedRoute) {
        this.fb = fb;
        this.profileSer = profileSer;
        this.notify = notify;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.data = { image: "assets/imgs/avatar/21.jpg" };
        activatedRoute.params.subscribe(val => {
            this.user_id = localStorage.getItem("vendor_id");
            this.getUser(this.user_id);
        });
    }
    ngOnInit() {
        this.formBuilder();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z]+$")]),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z]+$")]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$"),]),
            mobile: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]{10}")]),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            //city: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]{6}")]),
        });
    }
    getUser(id) {
        this.profileSer.getItem(id)
            .subscribe((res) => {
            console.log("user", res);
            let user = res['user'];
            this.user_name = user.name;
            user.name = user.name.split(" ");
            this.frmFG.patchValue({
                firstName: user.name[0],
                lastName: user.name[1],
                email: user.email,
                mobile: user.mobile,
                address: user.address,
                //city: user.city,
                zipcode: user.zipcode
            });
        });
    }
    submit() {
        var data = this.frmFG.value;
        data.name = this.capitalization(data.firstName) + ' ' + this.capitalization(data.lastName);
        console.log("data", data);
        this.profileSer.updateItem(data, this.user_id)
            .subscribe((res) => {
            if (res) {
                this.getUser(this.user_id);
                this.notify.success('Update profile successfully');
                this.router.navigate(['/profile']);
            }
            else {
                this.notify.error('Please try again');
            }
        }, (err) => {
            this.notify.error('Please try again');
            console.log("error", err);
        });
    }
    capitalization(input) {
        return (!!input) ? input.split(' ').map(function (wrd) { return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase(); }).join(' ') : '';
    }
};
ProfilePage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] }
];
ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile.page.scss */ "./src/app/pages/profile/profile.page.scss")).default]
    })
], ProfilePage);



/***/ }),

/***/ "./src/app/services/profile-service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/profile-service.ts ***!
  \*********************************************/
/*! exports provided: ProfileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileService", function() { return ProfileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




let ProfileService = class ProfileService {
    constructor(http) {
        this.http = http;
        this.base_href = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL;
        this.user = 'users/';
    }
    getItem(id) {
        return this.http.get(`${this.base_href}${this.user}${id}`);
    }
    updateItem(data, id) {
        return this.http.post(`${this.base_href}${this.user}${id}`, this.getData(data, 'update'));
    }
    getData(data, type) {
        const formData = new FormData();
        formData.append("name", data.name),
            formData.append("email", data.email),
            formData.append("mobile", data.mobile),
            formData.append("address", data.address),
            //formData.append("city", data.city),
            formData.append("zipcode", data.zipcode),
            formData.append('city', '2763'),
            formData.append('state', '22'),
            formData.append('country', 'India');
        if (type === 'update') {
            formData.append("method", 'PUT');
        }
        return formData;
    }
};
ProfileService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ProfileService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], ProfileService);



/***/ })

}]);
//# sourceMappingURL=pages-profile-profile-module.js.map