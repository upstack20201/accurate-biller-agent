(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-product-product-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/product.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/product.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <!-- Header -->\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button>\r\n        </ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>Products</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <!-- Content -->\r\n  <ion-content class=\"transparent\">\r\n    <!-- ANIMATION--COMPONENT 2 -->\r\n    <ion-grid class=\"ion-no-padding\">\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <ion-list class=\"ion-no-margin\" className=\"fadeInLeft\" *ngIf=\"data && data.products; else skeleton\">\r\n            <ion-item class=\"default-item box-shadow ion-no-padding animation-normal\" lines=\"none\"\r\n              *ngFor=\"let item of data.products\">\r\n              <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n                <img [src]=\"item.product_img\">\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding-start\">\r\n                <h2 class=\"text-size-sm font-bold text-color-primary ion-text-capitalize ion-text-wrap\">\r\n                  {{item.title}} (1 {{item.unit}})</h2>\r\n                <p class=\"text-size-p font-regular ion-text-wrap\">\r\n                  {{item.quantity}} Pkt</p>\r\n              </ion-label>\r\n              <ion-label class=\"ion-padding-start ion-text-end no-margin-right\" slot=\"end\">\r\n                <ion-badge class=\"border-radius-left\">Rs.{{item.rate}}\r\n                </ion-badge>\r\n                <div class=\"clearfix\"></div>\r\n                <!--  <ion-button class=\"default-button border-radius-left ion-text-capitalize\" color=\"danger\" (click)=remove(item.id)>\r\n                  Remove</ion-button> -->\r\n                <!-- <i slot=\"end\" class=\"icon icon-trash-outline icon-font-md icon-color-danger\"></i> -->\r\n                <ion-icon class=\"icon icon-font-sm font-bold icon-pencil\" color=\"medium\" (click)=\"editProduct(item.id)\">\r\n                </ion-icon>&nbsp;\r\n                <ion-icon class=\"icon icon-font-sm font-bold ion-margin-end\" color=\"medium\" (click)=\"remove(item)\"\r\n                  name=\"trash\"></ion-icon>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-list>\r\n\r\n          <ng-template #skeleton>\r\n            <ion-list class=\"ion-no-margin\" className=\"fadeInLeft\">\r\n              <ion-item class=\"default-item box-shadow ion-no-padding animation-normal\" lines=\"none\"\r\n                *ngFor=\"let item of [1, 2, 3, 4, 5]\">\r\n                <ion-thumbnail class=\"border-radius ion-padding-start\" slot=\"start\">\r\n                  <ion-avatar>\r\n                    <ion-skeleton-text animated></ion-skeleton-text>\r\n                  </ion-avatar>\r\n                </ion-thumbnail>\r\n                <ion-label class=\"ion-padding\" slot=\"start\">\r\n                  <h2>\r\n                    <ion-skeleton-text animated></ion-skeleton-text>\r\n                  </h2>\r\n                  <p>\r\n                    <ion-skeleton-text animated></ion-skeleton-text>\r\n                  </p>\r\n                </ion-label>\r\n                <ion-label class=\"ion-padding\" slot=\"end\">\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                  <div class=\"clearfix\"></div>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-label>\r\n              </ion-item>\r\n            </ion-list>\r\n          </ng-template>\r\n\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n\r\n    <!-- ANIMATION--COMPONENT 2 -->\r\n\r\n    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" icon-only tooltip=\"Add Product\" navTooltip>\r\n      <ion-fab-button (click)=\"addProduct()\">\r\n        <ion-icon name=\"add\"></ion-icon>\r\n      </ion-fab-button>\r\n    </ion-fab>\r\n  </ion-content>\r\n</ion-app>");

/***/ }),

/***/ "./src/app/pages/product/product-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/product/product-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ProductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageRoutingModule", function() { return ProductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _product_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product.page */ "./src/app/pages/product/product.page.ts");




const routes = [
    {
        path: '',
        component: _product_page__WEBPACK_IMPORTED_MODULE_3__["ProductPage"]
    },
    {
        path: 'add',
        loadChildren: () => __webpack_require__.e(/*! import() | add-product-add-product-module */ "add-product-add-product-module").then(__webpack_require__.bind(null, /*! ./add-product/add-product.module */ "./src/app/pages/product/add-product/add-product.module.ts")).then(m => m.AddProductPageModule)
    },
    {
        path: ':product_id/edit',
        loadChildren: () => __webpack_require__.e(/*! import() | edit-product-edit-product-module */ "edit-product-edit-product-module").then(__webpack_require__.bind(null, /*! ./edit-product/edit-product.module */ "./src/app/pages/product/edit-product/edit-product.module.ts")).then(m => m.EditProductPageModule)
    }
];
let ProductPageRoutingModule = class ProductPageRoutingModule {
};
ProductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProductPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/product/product.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/product/product.module.ts ***!
  \*************************************************/
/*! exports provided: ProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageModule", function() { return ProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _product_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-routing.module */ "./src/app/pages/product/product-routing.module.ts");
/* harmony import */ var _product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product.page */ "./src/app/pages/product/product.page.ts");







let ProductPageModule = class ProductPageModule {
};
ProductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _product_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductPageRoutingModule"]
        ],
        declarations: [_product_page__WEBPACK_IMPORTED_MODULE_6__["ProductPage"]]
    })
], ProductPageModule);



/***/ }),

/***/ "./src/app/pages/product/product.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/product/product.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-skeleton-text {\n  line-height: 13px;\n  --border-radius: 20px;\n}\n\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n\n/* ion-item {\n  border: 1px solid #e2dfdf;\n  background: var(--cs-background-secondary) !important;\n  border-radius: 8px;\n\n} */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC9wcm9kdWN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFBO0VBRUEscUJBQUE7QUFBRjs7QUFHQTtFQUNFLGtCQUFBO0FBQUY7O0FBR0E7Ozs7O0dBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcm9kdWN0L3Byb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXNrZWxldG9uLXRleHQge1xyXG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xyXG4gIC8vLS1iYWNrZ3JvdW5kLXJnYjogMjMwLCA0MCwgNTA7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0Omxhc3QtY2hpbGQge1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG5cclxuLyogaW9uLWl0ZW0ge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmRmZGY7XHJcbiAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1zZWNvbmRhcnkpICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG5cclxufSAqLyJdfQ== */");

/***/ }),

/***/ "./src/app/pages/product/product.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/product/product.page.ts ***!
  \***********************************************/
/*! exports provided: ProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPage", function() { return ProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");






let ProductPage = class ProductPage {
    constructor(router, alertCtrl, productSer, notify, activatedRoute) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.productSer = productSer;
        this.notify = notify;
        this.activatedRoute = activatedRoute;
        activatedRoute.params.subscribe(val => {
            this.vendor_id = localStorage.getItem('vendor_id');
            this.getProducts(this.vendor_id);
        });
    }
    ngOnInit() {
        //this.loadData();
    }
    getProducts(id) {
        setTimeout(() => {
            this.productSer.getItems(id).subscribe((res) => {
                this.data = res;
            });
        }, 1000);
    }
    addProduct() {
        this.router.navigate(['/product/add']);
    }
    editProduct(id) {
        this.router.navigate(['/product/' + id + '/edit']);
    }
    remove(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            data.file = data.product_img;
            let alert = yield this.alertCtrl.create({
                header: 'Remove Product',
                subHeader: 'Do you want to remove this product?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Remove',
                        handler: () => {
                            this.productSer.deleteItem(data)
                                .subscribe((res) => {
                                this.notify.success("Product deleted successfully");
                                this.getProducts(this.vendor_id);
                            }, (err) => {
                                this.notify.error("Please try again");
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
ProductPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
ProductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./product.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/product.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./product.page.scss */ "./src/app/pages/product/product.page.scss")).default]
    })
], ProductPage);



/***/ })

}]);
//# sourceMappingURL=pages-product-product-module.js.map