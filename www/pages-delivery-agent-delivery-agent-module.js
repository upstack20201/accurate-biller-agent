(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-agent-delivery-agent-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/delivery-agent.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/delivery-agent.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/product\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Delivery Agent</ion-title>\r\n    <ion-buttons class=\"ion-margin-end\" slot=\"end\">\r\n      <ion-icon class=\"icon-font-md\" (click)=\"addDeliveryAgent()\" name=\"person-add\"></ion-icon>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\" parallax-header>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list class=\"ion-no-margin\" *ngIf=\"data && data.agents; else skeleton\">\r\n          <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of data.agents\">\r\n            <!--Content Avatar-->\r\n            <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n              <img src=\"{{item.profilepic}}\">\r\n            </ion-thumbnail>\r\n            <ion-label (click)=\"changeStatus(item.id)\" class=\"ion-padding-start\">\r\n              <!-- Title and Subtitle -->\r\n              <h3 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                {{item.name}}</h3>\r\n              <p class=\"text-size-sm font-regular ion-text-wrap\">\r\n                {{item.address}}</p>\r\n            </ion-label>\r\n            <!-- <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-end\">\r\n              <ion-item>\r\n                <ion-icon size=\"small\" (click)=\"edit(item.id)\" name=\"create\"></ion-icon>\r\n              </ion-item>\r\n              <ion-item>\r\n                <ion-icon size=\"small\" (click)=\"remove(item)\" name=\"trash\"></ion-icon>\r\n              </ion-item>\r\n\r\n            </ion-buttons> -->\r\n\r\n            <ion-label class=\"ion-text-end no-margin-right\" slot=\"end\">\r\n              <ion-icon class=\"icon icon-font-sm font-bold icon-pencil\" color=\"medium\" (click)=\"edit(item.id)\">\r\n              </ion-icon>&nbsp;\r\n              <ion-icon class=\"icon icon-font-sm font-bold ion-margin-end\" color=\"medium\" (click)=\"remove(item)\"\r\n                name=\"trash\">\r\n              </ion-icon>\r\n            </ion-label>\r\n          </ion-item>\r\n        </ion-list>\r\n\r\n        <ng-template #skeleton>\r\n          <ion-list class=\"ion-no-margin\" className=\"fadeInLeft\">\r\n            <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of [1, 2, 3, 4, 5]\">\r\n              <ion-thumbnail class=\"border-radius ion-padding-start\" slot=\"start\">\r\n                <ion-avatar>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-avatar>\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding\">\r\n                <h2>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </h2>\r\n                <p>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </p>\r\n              </ion-label>\r\n              <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-end\">\r\n                <ion-item>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-item>\r\n                <ion-item>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-item>\r\n\r\n              </ion-buttons>\r\n            </ion-item>\r\n          </ion-list>\r\n        </ng-template>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: DeliveryAgentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryAgentPageRoutingModule", function() { return DeliveryAgentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _delivery_agent_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./delivery-agent.page */ "./src/app/pages/delivery-agent/delivery-agent.page.ts");




const routes = [
    {
        path: '',
        component: _delivery_agent_page__WEBPACK_IMPORTED_MODULE_3__["DeliveryAgentPage"]
    },
    {
        path: 'add',
        loadChildren: () => __webpack_require__.e(/*! import() | add-delivery-agent-add-delivery-agent-module */ "add-delivery-agent-add-delivery-agent-module").then(__webpack_require__.bind(null, /*! ./add-delivery-agent/add-delivery-agent.module */ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.module.ts")).then(m => m.AddDeliveryAgentPageModule)
    },
    {
        path: 'detail',
        loadChildren: () => __webpack_require__.e(/*! import() | delivery-agent-detail-delivery-agent-detail-module */ "delivery-agent-detail-delivery-agent-detail-module").then(__webpack_require__.bind(null, /*! ./delivery-agent-detail/delivery-agent-detail.module */ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.module.ts")).then(m => m.DeliveryAgentDetailPageModule)
    },
    {
        path: ':id/edit',
        loadChildren: () => __webpack_require__.e(/*! import() | edit-delivery-agent-edit-delivery-agent-module */ "edit-delivery-agent-edit-delivery-agent-module").then(__webpack_require__.bind(null, /*! ./edit-delivery-agent/edit-delivery-agent.module */ "./src/app/pages/delivery-agent/edit-delivery-agent/edit-delivery-agent.module.ts")).then(m => m.EditDeliveryAgentPageModule)
    },
    {
        path: ':agent_id/order-status',
        loadChildren: () => Promise.all(/*! import() | order-status-order-status-module */[__webpack_require__.e("common"), __webpack_require__.e("order-status-order-status-module")]).then(__webpack_require__.bind(null, /*! ./order-status/order-status.module */ "./src/app/pages/delivery-agent/order-status/order-status.module.ts")).then(m => m.OrderStatusPageModule)
    }
];
let DeliveryAgentPageRoutingModule = class DeliveryAgentPageRoutingModule {
};
DeliveryAgentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DeliveryAgentPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent.module.ts ***!
  \***************************************************************/
/*! exports provided: DeliveryAgentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryAgentPageModule", function() { return DeliveryAgentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _delivery_agent_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./delivery-agent-routing.module */ "./src/app/pages/delivery-agent/delivery-agent-routing.module.ts");
/* harmony import */ var _delivery_agent_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./delivery-agent.page */ "./src/app/pages/delivery-agent/delivery-agent.page.ts");







let DeliveryAgentPageModule = class DeliveryAgentPageModule {
};
DeliveryAgentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _delivery_agent_routing_module__WEBPACK_IMPORTED_MODULE_5__["DeliveryAgentPageRoutingModule"]
        ],
        declarations: [_delivery_agent_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryAgentPage"]]
    })
], DeliveryAgentPageModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".active ion-toolbar {\n  background: var(--cs-background-primary) !important;\n  color: var(--cs-text-accent) !important;\n}\n.active ion-back-button,\n.active ion-menu-button {\n  --color: var(--cs-text-accent) !important;\n}\n.background-size {\n  height: 400px;\n  position: relative;\n  /* Parallax Title\n     =============================================*/\n}\n.background-size::after {\n  content: \"\";\n  background: var(--cs-gradient-image);\n  position: absolute;\n  height: 100%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.background-size .parallax-title-section {\n  position: absolute;\n  bottom: 16px;\n  left: 16px;\n  right: 16px;\n}\n.icon-vertical {\n  display: inline-block !important;\n}\nion-skeleton-text {\n  line-height: 13px;\n  --border-radius: 20px;\n}\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnktYWdlbnQvZGVsaXZlcnktYWdlbnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbURBQUE7RUFDQSx1Q0FBQTtBQUFKO0FBR0U7O0VBRUUseUNBQUE7QUFESjtBQUtBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBWUE7bURBQUE7QUFaRjtBQUVFO0VBQ0UsV0FBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FBQUo7QUFLRTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBSEo7QUFPQTtFQUNFLGdDQUFBO0FBSkY7QUFPQTtFQUNFLGlCQUFBO0VBRUEscUJBQUE7QUFMRjtBQVFBO0VBQ0Usa0JBQUE7QUFMRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5LWFnZW50L2RlbGl2ZXJ5LWFnZW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hY3RpdmUge1xyXG4gIGlvbi10b29sYmFyIHtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWNzLWJhY2tncm91bmQtcHJpbWFyeSkgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiB2YXIoLS1jcy10ZXh0LWFjY2VudCkgIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIGlvbi1iYWNrLWJ1dHRvbixcclxuICBpb24tbWVudS1idXR0b24ge1xyXG4gICAgLS1jb2xvcjogdmFyKC0tY3MtdGV4dC1hY2NlbnQpICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG4uYmFja2dyb3VuZC1zaXplIHtcclxuICBoZWlnaHQ6IDQwMHB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgJjo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWNzLWdyYWRpZW50LWltYWdlKTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICB9XHJcblxyXG4gIC8qIFBhcmFsbGF4IFRpdGxlXHJcbiAgICAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Ki9cclxuICAucGFyYWxsYXgtdGl0bGUtc2VjdGlvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDE2cHg7XHJcbiAgICBsZWZ0OiAxNnB4O1xyXG4gICAgcmlnaHQ6IDE2cHg7XHJcbiAgfVxyXG59XHJcblxyXG4uaWNvbi12ZXJ0aWNhbCB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1za2VsZXRvbi10ZXh0IHtcclxuICBsaW5lLWhlaWdodDogMTNweDtcclxuICAvLy0tYmFja2dyb3VuZC1yZ2I6IDIzMCwgNDAsIDUwO1xyXG4gIC0tYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG5cclxuLmN1c3RvbS1za2VsZXRvbiBpb24tc2tlbGV0b24tdGV4dDpsYXN0LWNoaWxkIHtcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent.page.ts ***!
  \*************************************************************/
/*! exports provided: DeliveryAgentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryAgentPage", function() { return DeliveryAgentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");






let DeliveryAgentPage = class DeliveryAgentPage {
    constructor(router, alertCtrl, activatedRoute, deliveryAgentSer, notify) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.activatedRoute = activatedRoute;
        this.deliveryAgentSer = deliveryAgentSer;
        this.notify = notify;
        activatedRoute.params.subscribe(val => {
            this.vendor_id = localStorage.getItem("vendor_id");
            this.getAgents(this.vendor_id);
        });
    }
    ngOnInit() { }
    getAgents(id) {
        setTimeout(() => {
            this.deliveryAgentSer.getItems(id)
                .subscribe((res) => {
                this.data = res;
            });
        }, 1000);
    }
    addDeliveryAgent() {
        this.router.navigate(['/delivery-agent/add']);
    }
    onItemClickFunc(item, ev) {
        console.log("item", item);
        console.log("event", ev);
    }
    edit(id) {
        this.router.navigate(['/delivery-agent/' + id + '/edit']);
    }
    changeStatus(agent_id) {
        this.router.navigate(['/delivery-agent/' + agent_id + '/order-status']);
    }
    remove(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Remove delivery agent',
                subHeader: 'Do you want to remove this delivery agent?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Remove',
                        handler: () => {
                            this.deliveryAgentSer.deleteItem(data)
                                .subscribe((res) => {
                                this.notify.success("Agent deleted successfully");
                                this.getAgents(this.vendor_id);
                            }, (err) => {
                                this.notify.error("Please try again");
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
DeliveryAgentPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__["DeliveryAgentService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"] }
];
DeliveryAgentPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-delivery-agent',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./delivery-agent.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/delivery-agent.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./delivery-agent.page.scss */ "./src/app/pages/delivery-agent/delivery-agent.page.scss")).default]
    })
], DeliveryAgentPage);



/***/ })

}]);
//# sourceMappingURL=pages-delivery-agent-delivery-agent-module.js.map