(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["check-schedule-check-schedule-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schedule/check-schedule/check-schedule.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schedule/check-schedule/check-schedule.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/schedule\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Check Schedule</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<!-- EXPANDABLE --COMPONENT 1 -->\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list class=\"ion-no-margin\" *ngIf=\"data && data.agents; else skeleton\">\r\n          <!-- Header List Big Image -->\r\n          <!-- <ion-list-header>\r\n              <ion-label class=\"ion-padding-top ion-padding-bottom\">\r\n                <h1 class=\"text-size-md font-regular text-color-accent ion-no-margin\">\r\n                  {{data.header}}</h1>\r\n              </ion-label>\r\n            </ion-list-header> -->\r\n          <ul class=\"collapsible ion-no-margin ion-no-padding\">\r\n            <li *ngFor=\"let item of data.agents;\">\r\n              <!-- List big image Header -->\r\n              <ion-item class=\"default-item box-shadow ion-no-padding\" lines=\"none\">\r\n                <ion-item (click)=\"sheduleDetails(item.id)\">\r\n                  <ion-thumbnail class=\"border-radius-right\" slot=\"start\">\r\n                    <ion-img src=\"assets/imgs/avatar/avatardefault.png\" [alt]=\"item.name\"></ion-img>\r\n                  </ion-thumbnail>\r\n                  <ion-label class=\"ion-padding-start\">\r\n                    <h3 class=\"text-size-sm font-bold text-color-primary ion-text-wrap\">\r\n                      {{item.name}}</h3>\r\n                    <p class=\"text-size-sm font-regular ion-text-wrap\">\r\n                      {{item.address}}</p>\r\n                  </ion-label>\r\n                </ion-item>\r\n\r\n                <!--  <ion-badge *ngIf=\"item.leave\" color=\"danger\" class=\"border-radius-left\" slot=\"end\">\r\n\r\n                  <p class=\"mar-bottom\">On leave</p>\r\n                </ion-badge> -->\r\n                <!-- </ion-label> -->\r\n              </ion-item>\r\n              <!-- End List big image Header -->\r\n              <!-- LIST OPEN ACCORDIAN BODY  -->\r\n              <div class=\"item-accordion\" [ngClass]=\"{'active': item.show }\" [hidden]=\"!item.show\">\r\n                <ion-item class=\"default-item box-shadow ion-no-padding\" lines=\"none\"\r\n                  *ngFor=\"let item of item.expandItems;\" (click)=\"onItemClickFunc(item, $event)\">\r\n                  <!-- Avatar -->\r\n                  <ion-thumbnail class=\"border-radius-left\" slot=\"end\">\r\n                    <img [src]=\"item.image\" [alt]=\"item.title\" />\r\n                  </ion-thumbnail>\r\n                  <ion-label class=\"ion-padding-start\">\r\n                    <!-- Subtitle Title -->\r\n                    <h2 class=\"text-size-md font-bold text-color-primary ion-text-nowrap\">\r\n                      {{item.title}}</h2>\r\n                    <!-- Subtitle Subtitle -->\r\n                    <h3 class=\"text-size-sm text-color-primary font-regular ion-text-wrap\">\r\n                      {{item.description}}</h3>\r\n                    <p class=\"text-size-sm font-light text-color-primary ion-text-nowrap\">\r\n                      {{item.details}}</p>\r\n                  </ion-label>\r\n                </ion-item>\r\n              </div>\r\n              <!-- End List big image Body -->\r\n            </li>\r\n          </ul>\r\n        </ion-list>\r\n\r\n        <ng-template #skeleton>\r\n          <ion-list class=\"ion-no-margin\" className=\"fadeInLeft\">\r\n            <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of [1, 2, 3, 4, 5]\">\r\n              <ion-thumbnail class=\"border-radius ion-padding-start\" slot=\"start\">\r\n                <ion-avatar>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-avatar>\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding\">\r\n                <h2>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </h2>\r\n                <p>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </p>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-list>\r\n        </ng-template>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/check-schedule-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/check-schedule-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: CheckSchedulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckSchedulePageRoutingModule", function() { return CheckSchedulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _check_schedule_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./check-schedule.page */ "./src/app/pages/schedule/check-schedule/check-schedule.page.ts");




const routes = [
    {
        path: '',
        component: _check_schedule_page__WEBPACK_IMPORTED_MODULE_3__["CheckSchedulePage"]
    }
];
let CheckSchedulePageRoutingModule = class CheckSchedulePageRoutingModule {
};
CheckSchedulePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CheckSchedulePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/check-schedule.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/check-schedule.module.ts ***!
  \************************************************************************/
/*! exports provided: CheckSchedulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckSchedulePageModule", function() { return CheckSchedulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _check_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./check-schedule-routing.module */ "./src/app/pages/schedule/check-schedule/check-schedule-routing.module.ts");
/* harmony import */ var _check_schedule_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./check-schedule.page */ "./src/app/pages/schedule/check-schedule/check-schedule.page.ts");







let CheckSchedulePageModule = class CheckSchedulePageModule {
};
CheckSchedulePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _check_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__["CheckSchedulePageRoutingModule"]
        ],
        declarations: [_check_schedule_page__WEBPACK_IMPORTED_MODULE_6__["CheckSchedulePage"]]
    })
], CheckSchedulePageModule);



/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/check-schedule.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/check-schedule.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-badge.ion-color.ion-color-danger.md.hydrated {\n  height: 30px;\n}\n\np.mar-bottom {\n  margin-top: -8px;\n  letter-spacing: 1px;\n}\n\nion-skeleton-text {\n  line-height: 13px;\n  --border-radius: 20px;\n}\n\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2NoZWR1bGUvY2hlY2stc2NoZWR1bGUvY2hlY2stc2NoZWR1bGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQUNGOztBQUVBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsaUJBQUE7RUFFQSxxQkFBQTtBQUFGOztBQUdBO0VBQ0Usa0JBQUE7QUFBRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NjaGVkdWxlL2NoZWNrLXNjaGVkdWxlL2NoZWNrLXNjaGVkdWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1iYWRnZS5pb24tY29sb3IuaW9uLWNvbG9yLWRhbmdlci5tZC5oeWRyYXRlZCB7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG59XHJcblxyXG5wLm1hci1ib3R0b20ge1xyXG4gIG1hcmdpbi10b3A6IC04cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxufVxyXG5cclxuaW9uLXNrZWxldG9uLXRleHQge1xyXG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xyXG4gIC8vLS1iYWNrZ3JvdW5kLXJnYjogMjMwLCA0MCwgNTA7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0Omxhc3QtY2hpbGQge1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/check-schedule.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/check-schedule.page.ts ***!
  \**********************************************************************/
/*! exports provided: CheckSchedulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckSchedulePage", function() { return CheckSchedulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");




let CheckSchedulePage = class CheckSchedulePage {
    // data = {
    //   'toolbarTitle': 'List big image',
    //     "header": "Catalogue",
    //     "items": [
    //       {
    //         "id": 1,
    //         "title": "Ramesh Vilas Sathe",
    //         "description": "Shaniwar Peth, Pune, Maharashtra 411030",
    //         "image": "assets/imgs/products/boy-user.jpg",
    //       },
    //       {
    //         "id": 2,
    //         "title": "Ganesh Shrikant Ghadi",
    //         "description": " Service Road, Vivek Nagar, Akurdi, Pimpri-Chinchwad, Pune, Maharashtra 411035",
    //         "image": "assets/imgs/products/boy-user.jpg",
    //       },
    //       {
    //         "id": 3,
    //         "title": "Raghav Milind Mane",
    //         "description": "Katraj Dairy, Pune – Satara Road, Opp. Rajiv Gandhi Udyan, Katraj, Pune – 411046",
    //         "image": "assets/imgs/products/boy-user.jpg",
    //       },
    //       {
    //         "id": 4,
    //         "title": "Ganesh Raghav Patil",
    //         "description": "Tech Park One, Tower E, 191 Yerwada, Pune - 411 006",
    //         "image": "assets/imgs/products/boy-user.jpg",
    //       },
    //     ]
    //   }
    constructor(router, deliveryAgentSer) {
        this.router = router;
        this.deliveryAgentSer = deliveryAgentSer;
    }
    ngOnInit() {
        this.vendor_id = localStorage.getItem("vendor_id");
        this.getAgents(this.vendor_id);
    }
    getAgents(id) {
        setTimeout(() => {
            this.deliveryAgentSer.getItems(id)
                .subscribe((res) => {
                this.data = res;
                this.data.agents[1].leave = true;
                console.log("data", res);
            });
        }, 1000);
    }
    sheduleDetails(id) {
        this.router.navigate(['schedule/' + id]);
    }
    sheduleEdit(id) {
        this.router.navigate(['schedule/edit']);
    }
};
CheckSchedulePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_3__["DeliveryAgentService"] }
];
CheckSchedulePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-check-schedule',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./check-schedule.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schedule/check-schedule/check-schedule.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./check-schedule.page.scss */ "./src/app/pages/schedule/check-schedule/check-schedule.page.scss")).default]
    })
], CheckSchedulePage);



/***/ })

}]);
//# sourceMappingURL=check-schedule-check-schedule-module.js.map