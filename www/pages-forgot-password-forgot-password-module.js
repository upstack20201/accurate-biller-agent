(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forgot-password-forgot-password-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/login\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Forgot Password</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row class=\"ion-align-items-center\">\r\n      <ion-col class=\"ion-text-center ion-margin\" offset=\"2\" size=\"8\">\r\n        <h1 class=\"text-size-lg text-color-accent ion-text-wrap ion-margin-top ion-margin-bottom\">\r\n          Accurate Biller</h1>\r\n        <img src=\"assets/icon/lock.png\" />\r\n      </ion-col>\r\n      <ion-col class=\"ion-text-center\" size=\"12\">\r\n\r\n        <!--  <img class=\"ion-margin-bottom\" [src]=\"data.logo\" /> -->\r\n        <h2 class=\"text-size-sm text-color-primary font-bold ion-text-wrap \">\r\n          Forgot Your Password?</h2>\r\n        <h3 class=\"text-size-lg font-bold text-color-primary font-bold ion-text-wrap\">\r\n\r\n        </h3>\r\n      </ion-col>\r\n      <ion-col size=\"12\">\r\n        <form [formGroup]=\"frmFG\" novalidate (ngSubmit)=\"frmSubmit(frmFG.value, frmFG.valid)\">\r\n          <!---Input field username-->\r\n          <ion-item class=\"ion-no-padding border-radius box-shadow input\" lines=\"none\">\r\n            <!--   <ion-label class=\"text-size-md text-color-primary font-regular\" position=\"fixed\">\r\n              USERNAME</ion-label> -->\r\n            <ion-input class=\"text-size-sm ion-text-wrap\" name=\"email\" formControlName=\"email\" required type=\"text\"\r\n              placeholder=\"Enter username\">\r\n            </ion-input>\r\n          </ion-item>\r\n\r\n\r\n\r\n          <div class=\"clearfix\"> </div>\r\n          <ion-row class=\"ion-no-padding\">\r\n            <!-- <ion-col size=\"6\"></ion-col> -->\r\n            <ion-col size=\"12\">\r\n              <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n                expand=\"block\" [disabled]=\"!frmFG.valid\" type=\"submit\">\r\n                Send</ion-button>\r\n\r\n            </ion-col>\r\n          </ion-row>\r\n          <p class=\"text-size-sm text-color-primary font-regular\">\r\n          </p>\r\n        </form>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/forgot-password/forgot-password-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: ForgotPasswordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageRoutingModule", function() { return ForgotPasswordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _forgot_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forgot-password.page */ "./src/app/pages/forgot-password/forgot-password.page.ts");




const routes = [
    {
        path: '',
        component: _forgot_password_page__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordPage"]
    }
];
let ForgotPasswordPageRoutingModule = class ForgotPasswordPageRoutingModule {
};
ForgotPasswordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ForgotPasswordPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/forgot-password/forgot-password.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password.module.ts ***!
  \*****************************************************************/
/*! exports provided: ForgotPasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function() { return ForgotPasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _forgot_password_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forgot-password-routing.module */ "./src/app/pages/forgot-password/forgot-password-routing.module.ts");
/* harmony import */ var _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgot-password.page */ "./src/app/pages/forgot-password/forgot-password.page.ts");








let ForgotPasswordPageModule = class ForgotPasswordPageModule {
};
ForgotPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _forgot_password_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordPageRoutingModule"]
        ],
        declarations: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]]
    })
], ForgotPasswordPageModule);



/***/ }),

/***/ "./src/app/pages/forgot-password/forgot-password.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Style component 2\n========================================================*/\nion-content ion-grid img,\nion-content ion-row img {\n  max-width: 200px;\n  max-height: 100px;\n  margin: 0 auto;\n}\nion-content ion-grid form ion-item,\nion-content ion-row form ion-item {\n  --background: var(--cs-background-secondary) !important;\n  margin: 16px 16px 16px 6px;\n  width: calc(100% - 16px);\n}\nion-content ion-grid form ion-item ion-icon,\nion-content ion-row form ion-item ion-icon {\n  position: absolute;\n  right: 10px;\n  bottom: 15px;\n  z-index: 999;\n}\nion-content ion-grid form ion-item ion-label,\nion-content ion-row form ion-item ion-label {\n  transform: none;\n  flex: none !important;\n  min-width: auto !important;\n  width: auto !important;\n  margin: 0 0 0 16px;\n}\nion-content ion-grid form ion-item ion-input,\nion-content ion-row form ion-item ion-input {\n  color: var(--cs-text-primary);\n}\nion-content ion-grid form .default-button,\nion-content ion-row form .default-button {\n  width: calc(100% - 8px);\n}\nion-content ion-grid form .share-section,\nion-content ion-row form .share-section {\n  position: relative;\n  display: inline-block;\n  padding-top: 50px;\n  width: 100%;\n}\nion-content ion-grid form .share-section::after,\nion-content ion-row form .share-section::after {\n  content: \"or try\";\n  font-size: var(--h6-font-size, 12px);\n  line-height: 27px;\n  color: var(--cs-text-secondary);\n  width: 140px;\n  border-bottom: 1px solid var(--cs-background-secondary);\n  position: absolute;\n  top: 10px;\n  left: calc(50% - 70px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7eURBQUE7QUFVSTs7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQU5OO0FBVU07O0VBQ0UsdURBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0FBUFI7QUFTUTs7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQU5WO0FBU1E7O0VBQ0UsZUFBQTtFQUNBLHFCQUFBO0VBQ0EsMEJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0FBTlY7QUFTUTs7RUFDRSw2QkFBQTtBQU5WO0FBVU07O0VBQ0UsdUJBQUE7QUFQUjtBQVdNOztFQUNFLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUFSUjtBQVVROztFQUNFLGlCQUFBO0VBQ0Esb0NBQUE7RUFDQSxpQkFBQTtFQUNBLCtCQUFBO0VBQ0EsWUFBQTtFQUNBLHVEQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0Esc0JBQUE7QUFQViIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZvcmdvdC1wYXNzd29yZC9mb3Jnb3QtcGFzc3dvcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogU3R5bGUgY29tcG9uZW50IDJcclxuPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG5cclxuaW9uLWNvbnRlbnQge1xyXG5cclxuICBpb24tZ3JpZCxcclxuICBpb24tcm93IHtcclxuXHJcbiAgICAvLyBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgaW1nIHtcclxuICAgICAgbWF4LXdpZHRoOiAyMDBweDtcclxuICAgICAgbWF4LWhlaWdodDogMTAwcHg7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm0ge1xyXG4gICAgICBpb24taXRlbSB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1jcy1iYWNrZ3JvdW5kLXNlY29uZGFyeSkgIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW46IDE2cHggMTZweCAxNnB4IDZweDtcclxuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMTZweCk7XHJcblxyXG4gICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgYm90dG9tOiAxNXB4O1xyXG4gICAgICAgICAgei1pbmRleDogOTk5O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICAgIHRyYW5zZm9ybTogbm9uZTtcclxuICAgICAgICAgIGZsZXg6IG5vbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgIG1pbi13aWR0aDogYXV0byAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICAgICAgICAgIG1hcmdpbjogMCAwIDAgMTZweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlvbi1pbnB1dCB7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0tY3MtdGV4dC1wcmltYXJ5KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5kZWZhdWx0LWJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDhweCk7XHJcblxyXG4gICAgICB9XHJcblxyXG4gICAgICAuc2hhcmUtc2VjdGlvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBwYWRkaW5nLXRvcDogNTBweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgY29udGVudDogXCJvciB0cnlcIjtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogdmFyKC0taDYtZm9udC1zaXplLCAxMnB4KTtcclxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyN3B4O1xyXG4gICAgICAgICAgY29sb3I6IHZhcigtLWNzLXRleHQtc2Vjb25kYXJ5KTtcclxuICAgICAgICAgIHdpZHRoOiAxNDBweDtcclxuICAgICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB2YXIoLS1jcy1iYWNrZ3JvdW5kLXNlY29uZGFyeSk7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICB0b3A6IDEwcHg7XHJcbiAgICAgICAgICBsZWZ0OiBjYWxjKDUwJSAtIDcwcHgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/forgot-password/forgot-password.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password.page.ts ***!
  \***************************************************************/
/*! exports provided: ForgotPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function() { return ForgotPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let ForgotPasswordPage = class ForgotPasswordPage {
    constructor(fb, notify, authSer, router) {
        this.fb = fb;
        this.notify = notify;
        this.authSer = authSer;
        this.router = router;
    }
    ngOnInit() {
        this.formBuilder();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])
        });
    }
    frmSubmit(form, isValid) {
        console.log(form.email);
        this.authSer
            .forgotPsw(form.email)
            .subscribe(result => {
            console.log("result", result);
            if (result) {
                this.notify.success("New password will be sent to your email address");
            }
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 8000);
        }, error => {
            this.notify.error("Please check username");
        });
    }
};
ForgotPasswordPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_3__["NotifyService"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
ForgotPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-forgot-password',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./forgot-password.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_3__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./forgot-password.page.scss */ "./src/app/pages/forgot-password/forgot-password.page.scss")).default]
    })
], ForgotPasswordPage);



/***/ })

}]);
//# sourceMappingURL=pages-forgot-password-forgot-password-module.js.map