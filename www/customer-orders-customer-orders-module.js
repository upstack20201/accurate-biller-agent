(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-orders-customer-orders-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/customers/'+ product_id +'/detail\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Order Details</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\">\r\n\r\n  <ion-grid class=\"ion-no-padding\">\r\n\r\n    <ion-row class=\"ion-padding product-freeze border\">\r\n      <ion-col size=\"12\" class=\"\">\r\n        <ion-list class=\"product-section\">\r\n          <ion-item class=\"\" lines=\"none\">\r\n            <!--Content Avatar-->\r\n            <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n              <img src=\"assets/imgs/products/ghee.jpg\">\r\n            </ion-thumbnail>\r\n            <ion-label class=\"ion-padding-start\">\r\n              <!-- Ttile and Subtitle -->\r\n              <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                <!--  {{item.title}} (1 {{item.unit}}) -->Amul Milk (1 ltr)\r\n              </h6>\r\n              <h6 style=\"display: flex;\" class=\"text-size-sd text-color-primary ion-text-wrap\" color=\"medium\">\r\n                <!-- {{item.quantity}} -->1 Pkt &nbsp;&nbsp;\r\n                <label class=\"day-align\">\r\n                  <p *ngFor=\"let c of checkbox\" [style]=\"item.weekday.includes(c.name) ? textColor : ''\">\r\n                    {{c.title}}\r\n                  </p>\r\n                </label>\r\n              </h6>\r\n              <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                Rs. 64\r\n                <!-- {{item.rate}} -->\r\n              </h6>\r\n              <p>on\r\n                2021-03-28\r\n                <!-- {{item.delivery_date}} -->\r\n              </p>\r\n            </ion-label>\r\n            <!--      <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-end\">\r\n              <ion-item lines=\"none\">\r\n                <ion-icon size=\"small\"  (click)=\"editSchedule(customer_id,item.product_id)\"\r\n                class=\"icon-pencil\" color=\"medium\">\r\n              </ion-icon>\r\n              <ion-icon size=\"small\" (click)=\"remove(customer_id, item.product_id)\" name=\"trash\" color=\"medium\">\r\n              </ion-icon> \r\n              </ion-item>\r\n            </ion-buttons> -->\r\n          </ion-item>\r\n        </ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list *ngFor=\"let item of calender.items\">\r\n          <ion-item>\r\n\r\n            <ion-label class=\"\">\r\n              <h6 class=\"text-size-md text-color-primary font-bold ion-text-wrap\">\r\n                {{item.date}}\r\n              </h6>\r\n              <h6 class=\"text-size-sd \">\r\n                {{item.month}}\r\n              </h6>\r\n              <!--  <h6 class=\"text-size-sd text-color-primary ion-text-wrap\">\r\n                  {{item.title}} (1 {{item.unit}}) {{item.month}}\r\n              </h6> -->\r\n            </ion-label>\r\n\r\n            <ion-label class=\"ion-margin-start\">\r\n              <ion-label class=\"ion-margin-start\">\r\n                <h6 class=\"text-size-xs text-color-primary font-bold ion-text-wrap\">\r\n                  <!--  {{item.title}} (1 {{item.unit}}) -->\r\n                  <!-- {{item.date}}<ion-text\r\n                    class=\"text-size-sd text-color-primary\">&nbsp; {{item.month}} </ion-text> -->&nbsp;{{item.day}}\r\n                </h6>\r\n              </ion-label>\r\n              <ion-radio-group style=\"display: flex;\">\r\n                <ion-item lines=\"none\">\r\n                  <ion-radio slot=\"start\" value=\"delivered\"></ion-radio>\r\n                  <ion-label class=\"text-size-sd text-color-primary\">&nbsp;Delivered\r\n                  </ion-label>\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                  <ion-radio slot=\"start\" value=\"scheduled\"></ion-radio>\r\n                  <ion-label class=\"text-size-sd text-color-primary\">&nbsp;Scheduled\r\n                  </ion-label>\r\n                </ion-item>\r\n                <ion-item lines=\"none\">\r\n                  <ion-radio slot=\"start\" value=\"cancelled\"></ion-radio>\r\n                  <ion-label class=\"text-size-sd text-color-primary\">&nbsp;Cancelled\r\n                  </ion-label>\r\n                </ion-item>\r\n              </ion-radio-group>\r\n            </ion-label>\r\n\r\n          </ion-item>\r\n        </ion-list>\r\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\r\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n          </ion-infinite-scroll-content>\r\n        </ion-infinite-scroll>\r\n      </ion-col>\r\n\r\n    </ion-row>\r\n\r\n\r\n  </ion-grid>\r\n</ion-content>\r\n<ion-footer>\r\n  <ion-toolbar>\r\n\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \" expand=\"block\">\r\n      Submit\r\n    </ion-button>\r\n  </ion-toolbar>\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders-routing.module.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-orders/customer-orders-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: CustomerOrdersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerOrdersPageRoutingModule", function() { return CustomerOrdersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _customer_orders_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-orders.page */ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.ts");




const routes = [
    {
        path: '',
        component: _customer_orders_page__WEBPACK_IMPORTED_MODULE_3__["CustomerOrdersPage"]
    }
];
let CustomerOrdersPageRoutingModule = class CustomerOrdersPageRoutingModule {
};
CustomerOrdersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CustomerOrdersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-orders/customer-orders.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: CustomerOrdersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerOrdersPageModule", function() { return CustomerOrdersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _customer_orders_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customer-orders-routing.module */ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders-routing.module.ts");
/* harmony import */ var _customer_orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customer-orders.page */ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.ts");







let CustomerOrdersPageModule = class CustomerOrdersPageModule {
};
CustomerOrdersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _customer_orders_routing_module__WEBPACK_IMPORTED_MODULE_5__["CustomerOrdersPageRoutingModule"]
        ],
        declarations: [_customer_orders_page__WEBPACK_IMPORTED_MODULE_6__["CustomerOrdersPage"]]
    })
], CustomerOrdersPageModule);



/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".active ion-toolbar {\n  background: var(--cs-background-primary) !important;\n  color: var(--cs-text-accent) !important;\n}\n.active ion-back-button,\n.active ion-menu-button {\n  --color: var(--cs-text-accent) !important;\n}\nion-thumbnail {\n  display: block !important;\n  margin-left: auto !important;\n  margin-right: auto !important;\n}\n.header-image {\n  position: relative;\n  z-index: 999;\n}\n.background-size {\n  height: 140px;\n  position: relative;\n}\n.background-size::after {\n  content: \"\";\n  background: var(--cs-gradient-image);\n  position: absolute;\n  height: 100%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.info-block ion-thumbnail {\n  height: 100px;\n  width: 100px;\n  opacity: 1 !important;\n  z-index: 999;\n  position: relative;\n}\nion-grid {\n  position: relative;\n  z-index: 1;\n}\n.page1 .background-size {\n  height: 250px;\n}\n.page1 .background-size ion-card-content {\n  position: absolute;\n  bottom: 0;\n}\n.product-section {\n  border: 1px solid #cccccc;\n  background: var(--cs-background-secondary) !important;\n  border-radius: 8px;\n}\n.icon-vertical {\n  display: inline-block !important;\n}\n.day-align {\n  display: flex;\n  letter-spacing: 4px;\n  font-family: sans-serif;\n  font-size: smaller;\n  color: #c3cac8;\n}\nion-label {\n  flex: none !important;\n}\nion-radio {\n  width: 12px !important;\n  height: 12px !important;\n}\n.product-freeze {\n  position: sticky;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 9999;\n  background-color: #ffffff;\n  border-bottom: 1px solid #e2e1e1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3VzdG9tZXJzL2N1c3RvbWVyLWRldGFpbC9jdXN0b21lci1vcmRlcnMvY3VzdG9tZXItb3JkZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLG1EQUFBO0VBQ0EsdUNBQUE7QUFBSjtBQUdFOztFQUVFLHlDQUFBO0FBREo7QUFLQTtFQUNFLHlCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQUZGO0FBS0E7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUFGRjtBQUtBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0FBRkY7QUFJRTtFQUNFLFdBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQUZKO0FBT0U7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0FBTEo7QUFTQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtBQU5GO0FBVUU7RUFDRSxhQUFBO0FBUEo7QUFTSTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtBQVBOO0FBWUE7RUFDRSx5QkFBQTtFQUNBLHFEQUFBO0VBQ0Esa0JBQUE7QUFURjtBQVlBO0VBQ0UsZ0NBQUE7QUFURjtBQVlBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFURjtBQVlBO0VBQ0UscUJBQUE7QUFURjtBQVlBO0VBQ0Usc0JBQUE7RUFDQSx1QkFBQTtBQVRGO0FBWUE7RUFDRSxnQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLGFBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0FBVEYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jdXN0b21lcnMvY3VzdG9tZXItZGV0YWlsL2N1c3RvbWVyLW9yZGVycy9jdXN0b21lci1vcmRlcnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjdGl2ZSB7XHJcbiAgaW9uLXRvb2xiYXIge1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHZhcigtLWNzLXRleHQtYWNjZW50KSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgaW9uLWJhY2stYnV0dG9uLFxyXG4gIGlvbi1tZW51LWJ1dHRvbiB7XHJcbiAgICAtLWNvbG9yOiB2YXIoLS1jcy10ZXh0LWFjY2VudCkgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuXHJcbmlvbi10aHVtYm5haWwge1xyXG4gIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG8gIWltcG9ydGFudDtcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxufVxyXG5cclxuLmhlYWRlci1pbWFnZSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHotaW5kZXg6IDk5OTtcclxufVxyXG5cclxuLmJhY2tncm91bmQtc2l6ZSB7XHJcbiAgaGVpZ2h0OiAxNDBweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICY6OmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1jcy1ncmFkaWVudC1pbWFnZSk7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4uaW5mby1ibG9jayB7XHJcbiAgaW9uLXRodW1ibmFpbCB7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xyXG4gICAgLy9tYXJnaW46IC03MHB4IGF1dG8gMDtcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuLnBhZ2UxIHtcclxuICAuYmFja2dyb3VuZC1zaXplIHtcclxuICAgIGhlaWdodDogMjUwcHg7XHJcblxyXG4gICAgaW9uLWNhcmQtY29udGVudCB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnByb2R1Y3Qtc2VjdGlvbiB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcclxuICBiYWNrZ3JvdW5kOiB2YXIoLS1jcy1iYWNrZ3JvdW5kLXNlY29uZGFyeSkgIWltcG9ydGFudDtcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbn1cclxuXHJcbi5pY29uLXZlcnRpY2FsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmRheS1hbGlnbiB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBsZXR0ZXItc3BhY2luZzogNHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogc21hbGxlcjtcclxuICBjb2xvcjogcmdiKDE5NSAyMDIgMjAwKTtcclxufVxyXG5cclxuaW9uLWxhYmVsIHtcclxuICBmbGV4OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1yYWRpbyB7XHJcbiAgd2lkdGg6IDEycHggIWltcG9ydGFudDtcclxuICBoZWlnaHQ6IDEycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnByb2R1Y3QtZnJlZXplIHtcclxuICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gIHRvcDogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIHotaW5kZXg6IDk5OTk7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2UyZTFlMTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: CustomerOrdersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerOrdersPage", function() { return CustomerOrdersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_customer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/customer.service */ "./src/app/services/customer.service.ts");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");
/* harmony import */ var _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/schedule.service */ "./src/app/services/schedule.service.ts");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");








/* import { NotifyService } from '../../../../notifications/notify.service'; */

let CustomerOrdersPage = class CustomerOrdersPage {
    constructor(activatedRoute, router, customerSer, deliveryAgentSer, scheduleSer, productSer, alertCtrl
    /* private notify: NotifyService, */ ) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.customerSer = customerSer;
        this.deliveryAgentSer = deliveryAgentSer;
        this.scheduleSer = scheduleSer;
        this.productSer = productSer;
        this.alertCtrl = alertCtrl;
        this.weekdays = [];
        this.textColor = "color: #1ae5be";
        this.calender = {
            'toolbarTitle': 'List big image',
            "header": "Catalogue",
            "items": [
                {
                    "id": 1,
                    "date": "28",
                    "day": "Sunday",
                    "month": "MAR",
                },
                {
                    "id": 2,
                    "date": "30",
                    "day": "Tuesday",
                    "month": "MAR",
                },
                {
                    "id": 3,
                    "date": "31",
                    "day": "Wednesday",
                    "month": "MAR",
                },
                {
                    "id": 3,
                    "date": "2",
                    "day": "Thursday",
                    "month": "APR",
                },
                {
                    "id": 3,
                    "date": "3",
                    "day": "Friday",
                    "month": "APR",
                },
                {
                    "id": 3,
                    "date": "4",
                    "day": "Saturday",
                    "month": "APR",
                },
            ]
        };
        this.activatedRoute.params.subscribe((params) => {
            this.customer_id = params['id'];
        });
        activatedRoute.params.subscribe(val => {
            this.vendor_id = localStorage.getItem("vendor_id");
            // this.getCustomerSchedule(this.customer_id)
        });
    }
    /*  public checkbox = [
       { val: 'sun', name: 'Sunday', title: 'S', color: '' },
       { val: 'mon', name: 'Monday', title: 'M', color: ''  },
       { val: 'tues', name: 'Tuesday', title: 'T', color: ''  },
       { val: 'wed', name: 'Wednesday', title: 'W', color: ''  },
       { val: 'thu', name: 'Thursday', title: 'T', color: ''  },
       { val: 'fri', name: 'Friday', title: 'F', color: ''  },
       { val: 'sat', name: 'Saturday', title: 'S', color: ''  }
     ]; */
    loadData(event) {
        setTimeout(() => {
            //console.log('Done');
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            /* if (this.data.length == 1000) {
              event.target.disabled = true;
            } */
        }, 500);
    }
    toggleInfiniteScroll() {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
    ngOnInit() {
        this.getCustomer(this.customer_id);
        this.vendor_id = localStorage.getItem("vendor_id");
        // this.getAgent(this.vendor_id);
    }
    getCustomer(id) {
        this.customerSer.getItem(id)
            .subscribe((res) => {
            var customer = res['customer'];
            this.name = customer['name'];
            this.address = customer['address'];
            this.zipcode = customer['zipcode'];
            this.mobile = customer['mobile'];
            this.profilepic = customer['profilepic'];
            this.email = customer['email'];
            this.agent_id = customer['agent_id'];
        });
    }
};
CustomerOrdersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_customer_service__WEBPACK_IMPORTED_MODULE_3__["CustomerService"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__["DeliveryAgentService"] },
    { type: _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__["ScheduleService"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_6__["ProductService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] }
];
CustomerOrdersPage.propDecorators = {
    infiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonInfiniteScroll"],] }]
};
CustomerOrdersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer-orders',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./customer-orders.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./customer-orders.page.scss */ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.page.scss")).default]
    })
], CustomerOrdersPage);



/***/ })

}]);
//# sourceMappingURL=customer-orders-customer-orders-module.js.map