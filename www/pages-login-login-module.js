(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- lOGIN FROMA --COMPONENT 1 -->\r\n<div class=\"ion-page\">\r\n  <!-- Header -->\r\n  <!--  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-title *ngIf=\"data != null\" class=\"ion-text-capitalize\">Accurate Billing</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>  -->\r\n  <!-- Content -->\r\n  <ion-content *ngIf=\"data != null\" class=\"transparent background-size\">\r\n    <!-- [ngStyle]=\"{'background-image': 'url(' + data.background + ')'}\" -->\r\n\r\n    <ion-grid class=\"ion-padding\">\r\n      <ion-row class=\"ion-align-items-center\" *ngIf=\"data != null\">\r\n        <ion-col class=\"ion-text-center\" size=\"12\">\r\n          <h1 class=\"text-size-lg text-color-accent ion-text-wrap ion-margin-top ion-margin-bottom\">\r\n            Accurate Biller</h1>\r\n          <img class=\"ion-margin-bottom\" [src]=\"data.logo\" />\r\n          <h2 class=\"text-size-sm text-color-primary font-bold ion-text-wrap \">\r\n            {{data.subtitle}}</h2>\r\n          <h3 class=\"text-size-lg font-bold text-color-primary font-bold ion-text-wrap\">\r\n            {{data.title}}</h3>\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <!---Form-->\r\n          <form [formGroup]=\"frmFG\" (ngSubmit)=\"onLoginFunc(frmFG.value)\">\r\n            <!---Input field username-->\r\n            <ion-item class=\"ion-no-padding border-radius box-shadow input\" lines=\"none\">\r\n              <!--  <ion-label class=\"text-size-sd text-color-primary font-regular\" position=\"fixed\">\r\n                {{data.labelUsername}}</ion-label> -->\r\n              <ion-input class=\"text-size-sm ion-text-wrap\" required type=\"text\" [placeholder]=\"data.username\"\r\n                formControlName=\"email\" name=\"email\">\r\n              </ion-input>\r\n              <!-- <ion-icon *ngIf=\"!isUsernameValid\" class=\"icon-font-sm error-color\" name=\"remove-circle\"></ion-icon> -->\r\n            </ion-item>\r\n            <!---Input field password-->\r\n            <ion-item class=\"ion-no-padding border-radius box-shadow input\" lines=\"none\">\r\n              <!-- <ion-label class=\"text-size-sd text-color-primary font-regular\" position=\"fixed\">\r\n                {{data.labelPassword}}</ion-label> -->\r\n              <ion-input class=\"text-size-sm ion-text-wrap\" required [type]=\"password\" [placeholder]=\"data.password\"\r\n                formControlName=\"password\" name=\"password\">\r\n              </ion-input>\r\n              <!-- <ion-icon *ngIf=\"!isPasswordValid\" class=\"icon-font-sm error-color\" name=\"remove-circle\"></ion-icon> -->\r\n              <ion-icon item-end [name]=\"passwordIcon\" class=\"icon-font-sm text-color-primary\" (click)='hideShowPassword()'></ion-icon>\r\n            </ion-item>\r\n            <ion-button class=\"text-size-sm clear-button ion-text-capitalize ion-float-end\r\n            ion-margin-end ion-margin-start\" size=\"small\" fill=\"clear\" (click)=\"onForgotPassword()\">\r\n              {{data.forgotPassword}}\r\n            </ion-button>\r\n            <div class=\"clearfix\"> </div>\r\n            <ion-row class=\"ion-no-padding\">\r\n              <ion-col size=\"12\" class=\"ion-padding\">\r\n                <!---Login ion-button-->\r\n                <ion-button class=\"default-button ion-text-capitalize ion-margin-top\r\n                ion-margin-bottom border-radius\" [disabled]=\"!frmFG.valid\" type=\"submit\">\r\n                  <!--   <ion-icon class=\"icon-font-md icon-color-secondary font-bold\" slot=\"end\"\r\n                  name=\"md-arrow-forward\"></ion-icon>  -->\r\n                  {{data.login}}\r\n                </ion-button>\r\n              </ion-col>\r\n              <ion-col size=\"6\" class=\"ion-no-padding\">\r\n                <!---Login ion-button-->\r\n                <!-- <ion-button class=\"default-button ion-text-capitalize ion-margin-top\r\n                ion-margin-bottom border-radius-left ion-float-end\" (click)=\"onLoginFunc()\">\r\n                  {{data.login}}</ion-button> -->\r\n              </ion-col>\r\n            </ion-row>\r\n            <!--   <div class=\"ion-text-center share-section ion-padding-horizontal\">\r\n              <ion-button size=\"large\" fill=\"clear\" class=\"ion-margin-end\">\r\n                <ion-icon class=\"icon-fon-xl icon-color-secondary\" slot=\"icon-only\"\r\n                name=\"logo-instagram\"></ion-icon>\r\n              </ion-button>\r\n              <ion-button size=\"large\" fill=\"clear\">\r\n                <ion-icon class=\"icon-fon-xl icon-color-secondary\" slot=\"icon-only\"\r\n                name=\"logo-pinterest\"></ion-icon>\r\n              </ion-button>\r\n              <ion-button size=\"large\" fill=\"clear\" class=\"ion-margin-start\">\r\n                <ion-icon class=\"icon-fon-xl icon-color-secondary\" slot=\"icon-only\"\r\n                name=\"logo-twitter\"></ion-icon>\r\n              </ion-button>\r\n            </div> -->\r\n            <p class=\"text-size-sm text-color-primary font-regular\">\r\n              {{data.description}}</p>\r\n          </form>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-content>\r\n</div>");

/***/ }),

/***/ "./src/app/pages/login/login-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/pages/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");








let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Style component 2\n========================================================*/\nion-content ion-grid,\nion-content ion-row {\n  height: 100%;\n}\nion-content ion-grid img,\nion-content ion-row img {\n  max-width: 200px;\n  max-height: 100px;\n  margin: 0 auto;\n}\nion-content ion-grid form ion-item,\nion-content ion-row form ion-item {\n  --background: var(--cs-background-secondary) !important;\n  margin: 16px 16px 16px 6px;\n  width: calc(100% - 16px);\n}\nion-content ion-grid form ion-item ion-icon,\nion-content ion-row form ion-item ion-icon {\n  position: absolute;\n  right: 10px;\n  bottom: 15px;\n  z-index: 999;\n}\nion-content ion-grid form ion-item ion-label,\nion-content ion-row form ion-item ion-label {\n  transform: none;\n  flex: none !important;\n  min-width: auto !important;\n  width: auto !important;\n  margin: 0 0 0 16px;\n}\nion-content ion-grid form ion-item ion-input,\nion-content ion-row form ion-item ion-input {\n  color: var(--cs-text-primary);\n}\nion-content ion-grid form .default-button,\nion-content ion-row form .default-button {\n  width: calc(100% - 8px);\n}\nion-content ion-grid form .share-section,\nion-content ion-row form .share-section {\n  position: relative;\n  display: inline-block;\n  padding-top: 50px;\n  width: 100%;\n}\nion-content ion-grid form .share-section::after,\nion-content ion-row form .share-section::after {\n  content: \"or try\";\n  font-size: var(--h6-font-size, 12px);\n  line-height: 27px;\n  color: var(--cs-text-secondary);\n  width: 140px;\n  border-bottom: 1px solid var(--cs-background-secondary);\n  position: absolute;\n  top: 10px;\n  left: calc(50% - 70px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO3lEQUFBO0FBS0k7O0VBRUUsWUFBQTtBQUZOO0FBSU07O0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFEUjtBQUtROztFQUNFLHVEQUFBO0VBQ0EsMEJBQUE7RUFDQSx3QkFBQTtBQUZWO0FBSVU7O0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUFEWjtBQUlVOztFQUNFLGVBQUE7RUFDQSxxQkFBQTtFQUNBLDBCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQURaO0FBR1U7O0VBQ0MsNkJBQUE7QUFBWDtBQUlROztFQUNFLHVCQUFBO0FBRFY7QUFLUTs7RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FBRlY7QUFJVTs7RUFDRSxpQkFBQTtFQUNBLG9DQUFBO0VBQ0EsaUJBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQSx1REFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHNCQUFBO0FBRFoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBTdHlsZSBjb21wb25lbnQgMlxyXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXHJcblxyXG5pb24tY29udGVudCB7XHJcblxyXG4gICAgaW9uLWdyaWQsXHJcbiAgICBpb24tcm93IHtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gIFxyXG4gICAgICBpbWcge1xyXG4gICAgICAgIG1heC13aWR0aDogMjAwcHg7XHJcbiAgICAgICAgbWF4LWhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgZm9ybSB7XHJcbiAgICAgICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1jcy1iYWNrZ3JvdW5kLXNlY29uZGFyeSkgIWltcG9ydGFudDtcclxuICAgICAgICAgIG1hcmdpbjogMTZweCAxNnB4IDE2cHggNnB4O1xyXG4gICAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE2cHgpO1xyXG4gIFxyXG4gICAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBib3R0b206IDE1cHg7XHJcbiAgICAgICAgICAgIHotaW5kZXg6IDk5OTtcclxuICAgICAgICAgIH1cclxuICBcclxuICAgICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogbm9uZTtcclxuICAgICAgICAgICAgZmxleDogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBtaW4td2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICAgICAgICAgICAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICAgICAgICAgICAgbWFyZ2luOiAwIDAgMCAxNnB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaW9uLWlucHV0e1xyXG4gICAgICAgICAgIGNvbG9yOiB2YXIoLS1jcy10ZXh0LXByaW1hcnkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgICAuZGVmYXVsdC1idXR0b24ge1xyXG4gICAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDhweCk7XHJcbiAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgLnNoYXJlLXNlY3Rpb24ge1xyXG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgcGFkZGluZy10b3A6IDUwcHg7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICBcclxuICAgICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJvciB0cnlcIjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiB2YXIoLS1oNi1mb250LXNpemUsIDEycHgpO1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjdweDtcclxuICAgICAgICAgICAgY29sb3I6IHZhcigtLWNzLXRleHQtc2Vjb25kYXJ5KTtcclxuICAgICAgICAgICAgd2lkdGg6IDE0MHB4O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdmFyKC0tY3MtYmFja2dyb3VuZC1zZWNvbmRhcnkpO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogMTBweDtcclxuICAgICAgICAgICAgbGVmdDogY2FsYyg1MCUgLSA3MHB4KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");






let LoginPage = class LoginPage {
    constructor(fb, router, authser, notify) {
        this.fb = fb;
        this.router = router;
        this.authser = authser;
        this.notify = notify;
        this.password = 'password';
        this.passwordIcon = 'eye-off';
        this.data = {
            'headerTitle': 'Login',
            "background": "assets/imgs/background/30.jpg",
            "forgotPassword": "Forgot password?",
            "labelUsername": "USERNAME",
            "labelPassword": "PASSWORD",
            "title": "Gausampada",
            "subtitle": "Welcome To",
            "username": "Enter your username",
            "password": "Enter your password",
            "login": "LOG IN",
            "facebookLogin": "Login with",
            "logo": "assets/imgs/logo/logo.png",
        };
    }
    ngOnInit() {
        this.userName = localStorage.getItem("userName");
        this.psw = localStorage.getItem("password");
        this.formBuilder();
        if (this.userName && this.psw) {
            this.frmFG.get("email").setValue(this.userName);
            this.frmFG.get("password").setValue(this.psw);
        }
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$"),
            ]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
    }
    hideShowPassword() {
        this.password = this.password === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    onForgotPassword() {
        this.router.navigate(['/forgot-password']);
    }
    onLoginFunc(form) {
        var data = this.frmFG.value;
        this.authser.login(data)
            .subscribe((res) => {
            if (!res) {
                this.notify.error("Login failed. Please try again.");
                this.router.navigate(['/login']);
            }
            if (res) {
                this.notify.success("Logged in successfully");
                this.router.navigate(['/product']);
                localStorage.setItem("userName", form.email);
                localStorage.setItem("password", form.password);
            }
            else {
                this.notify.error("Sorry!! These credentials cannot be used");
            }
        }, (err) => {
            this.notify.error('Please check the login credentials');
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")).default]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map