(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-product-edit-product-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/edit-product/edit-product.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/edit-product/edit-product.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/product\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">{{product_title}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n      <ion-row class=\"ion-padding-top ion-padding-bottom\">\r\n\r\n        <ion-col size=\"12\" class=\"ion-align-self-start ion-no-padding\">\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-col size=\"4\" class=\"ion-margin-start\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap\"> Upload Product\r\n              </ion-label>\r\n            </ion-col>\r\n            <ion-col size=\"4\" class=\"ion-margin-start\">\r\n              <!--  <input type=\"file\" (change)=\"loadImageFromDevice($event)\" id=\"file-input\" accept=\"image/png, image/jpeg\"> -->\r\n              <input id=\"fileInput\" name=\"file\" type=\"file\" (change)=\"readUrl($event)\" accept=\"image/*\" />\r\n              <label for=\"fileInput\" icon-only ion-button>\r\n                <ion-icon name=\"image-outline\" size=\"large\"></ion-icon>\r\n              </label>\r\n            </ion-col>\r\n            <ion-col size=\"5\" class=\"\">\r\n              <ion-thumbnail class=\"border-radius\">\r\n                <img [src]=\"product_img\" alt=\"\" id=\"edit-product\">\r\n              </ion-thumbnail>\r\n            </ion-col>\r\n          </ion-item>\r\n        </ion-col>\r\n        <ion-col size=\"12\" class=\"ion-align-self-start ion-no-padding\">\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Product ID</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"\r\n              formControlName=\"SKU\">\r\n            </ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Product Title</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n              [placeholder]=\"\" formControlName=\"title\"></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Price\r\n              in (Rs.)</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" [placeholder]=\"\"\r\n              formControlName=\"rate\"></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap ion-text-wrap ion-margin-start\"\r\n              position=\"fixed\">Unit</ion-label>\r\n            <ion-select formControlName=\"unit\" [interfaceOptions]=\"customActionSheetOptions\" interface=\"action-sheet\"\r\n              class=\"text-size-sm text-color-primary ion-text-wrap ion-padding-start ion-margin-start popvert\">\r\n              <ion-select-option *ngFor=\"let item of units\" [value]=\"item.unit\">{{item.unit}}</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Quantity</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" [placeholder]=\"\"\r\n              formControlName=\"quantity\"></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Description</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"\r\n              formControlName=\"description\"></ion-input>\r\n          </ion-item>\r\n          <!-- <ion-item class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\" lines=\"none\">\r\n         <ion-label class=\"ion-text-wrap ion-margin-start\" position=\"fixed\" >Delivery Agent</ion-label>\r\n         <ion-select\r\n                formControlName=\"delivery_agent\"\r\n                class=\"popvert\"\r\n                [interfaceOptions]=\"customActionSheetOptions\"\r\n                interface=\"action-sheet\"\r\n                class=\"ion-padding-start ion-margin-start\">\r\n                <ion-select-option *ngFor=\"let item of delivery_agent\" [value]=\"item\">{{item}}</ion-select-option>\r\n              </ion-select>\r\n            </ion-item> -->\r\n          <!-- Add Product -->\r\n          <!-- <ion-item class=\"ion-margin-bottom ion-margin-end  ion-no-padding background-secondary \" lines=\"none\">\r\n          <ion-button [routerLink]=\"['/orders']\" class=\"default-button border-radius ion-text-capitalize ion-margin-start ion-margin-top\" expand=\"block\">\r\n            Set Order\r\n         </ion-button>\r\n         </ion-item> -->\r\n          <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n            [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n            Update\r\n          </ion-button>\r\n\r\n        </ion-col>\r\n\r\n      </ion-row>\r\n    </form>\r\n  </ion-grid>\r\n</ion-content>\r\n<!-- <ion-footer>\r\n  <ion-toolbar>\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \"\r\n      [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n      Update\r\n    </ion-button>\r\n\r\n  </ion-toolbar>\r\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/pages/product/edit-product/edit-product-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/product/edit-product/edit-product-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: EditProductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProductPageRoutingModule", function() { return EditProductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _edit_product_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-product.page */ "./src/app/pages/product/edit-product/edit-product.page.ts");




const routes = [
    {
        path: '',
        component: _edit_product_page__WEBPACK_IMPORTED_MODULE_3__["EditProductPage"]
    }
];
let EditProductPageRoutingModule = class EditProductPageRoutingModule {
};
EditProductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditProductPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/product/edit-product/edit-product.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/product/edit-product/edit-product.module.ts ***!
  \*******************************************************************/
/*! exports provided: EditProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProductPageModule", function() { return EditProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_product_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-product-routing.module */ "./src/app/pages/product/edit-product/edit-product-routing.module.ts");
/* harmony import */ var _edit_product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-product.page */ "./src/app/pages/product/edit-product/edit-product.page.ts");







let EditProductPageModule = class EditProductPageModule {
};
EditProductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _edit_product_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProductPageRoutingModule"]
        ],
        declarations: [_edit_product_page__WEBPACK_IMPORTED_MODULE_6__["EditProductPage"]]
    })
], EditProductPageModule);



/***/ }),

/***/ "./src/app/pages/product/edit-product/edit-product.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/product/edit-product/edit-product.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-grid {\n  height: 100%;\n}\n\n#fileInput {\n  position: absolute;\n  opacity: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC9lZGl0LXByb2R1Y3QvZWRpdC1wcm9kdWN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUFDRjs7QUFHQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtBQUFGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJvZHVjdC9lZGl0LXByb2R1Y3QvZWRpdC1wcm9kdWN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1ncmlkIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcblxyXG59XHJcblxyXG4jZmlsZUlucHV0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgb3BhY2l0eTogMDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/product/edit-product/edit-product.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/product/edit-product/edit-product.page.ts ***!
  \*****************************************************************/
/*! exports provided: EditProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProductPage", function() { return EditProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let EditProductPage = class EditProductPage {
    constructor(fb, productSer, notify, router, activatedRoute) {
        this.fb = fb;
        this.productSer = productSer;
        this.notify = notify;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.activatedRoute.params.subscribe((params) => {
            this.product_id = params['product_id'];
            this.getProduct(this.product_id);
        });
    }
    ngOnInit() {
        this.formBuilder();
        this.getUnits();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            file: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            SKU: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            rate: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            unit: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            quantity: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
    }
    getUnits() {
        this.productSer.getUnits()
            .subscribe((res) => {
            this.units = res['units'];
        });
    }
    getProduct(id) {
        this.productSer.getItem(id)
            .subscribe((res) => {
            let product = res['product'];
            this.product_title = product.title;
            this.product_img = product.product_img;
            this.frmFG.get("SKU").setValue(product.SKU);
            this.frmFG.get("title").setValue(product.title);
            this.frmFG.get("rate").setValue(product.rate);
            this.frmFG.get("unit").setValue(product.unit);
            this.frmFG.get("description").setValue(product.description);
            this.frmFG.get("quantity").setValue(product.quantity);
            this.frmFG.get("file").setValue(product.product_img);
        });
    }
    submit() {
        var data = this.frmFG.value;
        data.title = this.capitalization(data.title);
        data.file = this.url1;
        this.productSer.updateItem(this.product_id, data)
            .subscribe((res) => {
            if (res) {
                this.notify.success('Product updated successfully');
                this.router.navigate(['/product']);
            }
            else {
                this.notify.error('Please try again');
            }
        }, (err) => {
            this.notify.error('Please try again');
            console.log("error", err);
        });
    }
    capitalization(input) {
        return (!!input) ? input.split(' ').map(function (wrd) { return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase(); }).join(' ') : '';
    }
    readUrl(event) {
        console.log('readUrl', event.target.files[0]);
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onload = (event) => {
                this.url1 = event.target.result;
                this.product_img = this.url1;
            };
        }
    }
};
EditProductPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] }
];
EditProductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-product.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/edit-product/edit-product.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-product.page.scss */ "./src/app/pages/product/edit-product/edit-product.page.scss")).default]
    })
], EditProductPage);



/***/ })

}]);
//# sourceMappingURL=edit-product-edit-product-module.js.map