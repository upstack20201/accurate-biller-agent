(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-order-detail-edit-order-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/edit-order-detail/edit-order-detail.page.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/edit-order-detail/edit-order-detail.page.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/customers/{{this.cust_id}}/detail\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Edit Subscription</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\" *ngIf=\"data != null\">\r\n\r\n  <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n    <ion-grid class=\"ion-no-padding\" *ngIf=\"data != null\">\r\n      <ion-row>\r\n        <ion-col size=\"12\">\r\n          <ion-list class=\"ion-no-margin\">\r\n            <ion-item class=\"ion-margin\" lines=\"none\">\r\n              <!--Content Avatar-->\r\n              <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n                <img [src]=\"data.product_img\">\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding-start\">\r\n                <h5 class=\"text-size-sm text-color-primary font-bold ion-text-wrap capitalize\">\r\n                  {{data.title}}</h5>\r\n                <p class=\"text-size-sm font-regular ion-text-wrap\">‎Rs. {{data.rate}}</p>\r\n              </ion-label>\r\n\r\n\r\n              <!--  <ion-icon (click)=\"decrementQty()\" name=\"remove-outline\"></ion-icon>\r\n                <ion-text> {{product_qty}} </ion-text>\r\n                <ion-icon (click)=\"incrementQty()\" name=\"add-outline\"></ion-icon> -->\r\n              <!-- <ion-badge class=\"border-radius\" color=\"\" slot=\"start\">\r\n                <ion-icon (click)=\"decrementQty()\" name=\"remove-outline\"></ion-icon>\r\n              </ion-badge>\r\n              <h4 class=\"text-size-sm text-color-primary font-bold\"> {{product_qty}} </h4>\r\n              <ion-badge class=\"border-radius\" color=\"\" slot=\"end\">\r\n                <ion-icon (click)=\"incrementQty()\" name=\"add-outline\"></ion-icon>\r\n              </ion-badge> -->\r\n\r\n              <ion-chip outline>\r\n                <ion-icon (click)=\"decrementQty()\" name=\"remove-outline\"></ion-icon>\r\n                <h5 class=\"text-size-sm text-color-primary font-bold ion-text-wrap capitalize\">{{product_qty}}</h5>\r\n                <ion-icon (click)=\"incrementQty()\" name=\"add-outline\"></ion-icon>\r\n              </ion-chip>\r\n            </ion-item>\r\n          </ion-list>\r\n\r\n          <ion-item class=\"ion-margin\">\r\n            <ion-icon size=\"large\" name=\"calendar-number-outline\" color=\"medium\"></ion-icon>\r\n            <ion-label class=\"ion-margin-start\">\r\n              <h5 class=\"text-size-sm text-color-primary font-bold ion-text-wrap capitalize\">Start date</h5>\r\n            </ion-label>\r\n            <ion-label class=\"text-size-sm text-color-primary\">\r\n              <ion-datetime formControlName=\"start_date\" displayFormat=\"DD-MM-YYYY\" min=\"1940\"\r\n                placeholder={{schedule_start_date}}>\r\n              </ion-datetime>\r\n            </ion-label>\r\n          </ion-item>\r\n\r\n          <!-- <ion-item-divider color=\"secondary\"></ion-item-divider> -->\r\n          <ion-item class=\"ion-margin\" lines=\"none\">\r\n            <ion-icon size=\"large\" name=\"repeat-outline\" color=\"medium\"></ion-icon>\r\n            <ion-label class=\"ion-margin-start\">\r\n              <h3>Repeat <ion-text\r\n                  class=\"text-size-sm text-color-primary ion-margin-start ion-text-uppercase text-color-accent\">\r\n                  {{repeat_plan}}</ion-text>\r\n              </h3>\r\n            </ion-label>\r\n\r\n          </ion-item>\r\n\r\n          <ion-item lines=\"none\" class=\"ion-margin\">\r\n            <ion-chip (click)=\"schedule('daily')\" class=\"schedule_btn\" size=\"small\" color=\"default\">\r\n              Daily</ion-chip>\r\n            <ion-chip (click)=\"schedule('alternate')\" class=\"schedule_btn\" size=\"small\" color=\"default\">\r\n              Alternate</ion-chip>\r\n            <ion-chip (click)=\"schedule('weekends')\" class=\"schedule_btn\" size=\"small\" color=\"default\">\r\n              Weekends</ion-chip>\r\n          </ion-item>\r\n        </ion-col>\r\n      </ion-row>\r\n      <!-- <ion-label class=\"ion-margin\">Choose Days</ion-label> -->\r\n      <ion-row class=\"ion-margin-top\">\r\n        <ion-item *ngFor=\"let item of checkbox\" lines=\"none\">\r\n          <label class=\"coolbox\">\r\n            <input type=\"checkbox\" formControlName=\"{{item.name}}\" (change)=\"getCheckboxValue($event)\"\r\n              name=\"{{item.name}}\" />\r\n            <span>{{item.title}}</span>\r\n          </label>\r\n        </ion-item>\r\n      </ion-row>\r\n\r\n\r\n      <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n        [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n        Confirm\r\n      </ion-button>\r\n      <!--  <ion-button [routerLink]=\"['/customers/add']\" size=\"small\" shape=\"round\" color=\"secondary\">Confirm</ion-button> -->\r\n    </ion-grid>\r\n  </form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/orders/edit-order-detail/edit-order-detail-routing.module.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/orders/edit-order-detail/edit-order-detail-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: EditOrderDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditOrderDetailPageRoutingModule", function() { return EditOrderDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _edit_order_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-order-detail.page */ "./src/app/pages/orders/edit-order-detail/edit-order-detail.page.ts");




const routes = [
    {
        path: '',
        component: _edit_order_detail_page__WEBPACK_IMPORTED_MODULE_3__["EditOrderDetailPage"]
    }
];
let EditOrderDetailPageRoutingModule = class EditOrderDetailPageRoutingModule {
};
EditOrderDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditOrderDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/orders/edit-order-detail/edit-order-detail.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/pages/orders/edit-order-detail/edit-order-detail.module.ts ***!
  \****************************************************************************/
/*! exports provided: EditOrderDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditOrderDetailPageModule", function() { return EditOrderDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_order_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-order-detail-routing.module */ "./src/app/pages/orders/edit-order-detail/edit-order-detail-routing.module.ts");
/* harmony import */ var _edit_order_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-order-detail.page */ "./src/app/pages/orders/edit-order-detail/edit-order-detail.page.ts");








let EditOrderDetailPageModule = class EditOrderDetailPageModule {
};
EditOrderDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _edit_order_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditOrderDetailPageRoutingModule"]
        ],
        declarations: [_edit_order_detail_page__WEBPACK_IMPORTED_MODULE_6__["EditOrderDetailPage"]]
    })
], EditOrderDetailPageModule);



/***/ }),

/***/ "./src/app/pages/orders/edit-order-detail/edit-order-detail.page.scss":
/*!****************************************************************************!*\
  !*** ./src/app/pages/orders/edit-order-detail/edit-order-detail.page.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("label.coolbox {\n  background: #1ae5be;\n  padding: 9px;\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n  line-height: 15px;\n  text-align: center;\n  vertical-align: middle;\n  font-weight: bold;\n  font-size: 15px;\n  border-radius: 50%;\n}\n\nlabel.coolbox input {\n  display: none;\n}\n\nlabel.coolbox span {\n  color: white;\n}\n\nlabel.coolbox input:checked + span {\n  color: #0f0f0f;\n}\n\nion-datetime.md.datetime-placeholder.hydrated,\nion-datetime.md.hydrated {\n  margin-left: -17px;\n  margin-top: -8px;\n}\n\n.schedule_btn {\n  border: 1px solid #036f5a;\n  color: #1ae5be;\n  border-radius: 5%;\n  border-color: #1ae5be !important;\n}\n\n.qty_btn {\n  border: 1px solid #1ae5bc34;\n  border-radius: 45%;\n  background-color: #ffffff;\n  color: #1ae5be;\n  padding: 9px 20px;\n  width: 100px;\n}\n\nion-grid {\n  height: 100%;\n  background-color: #ffffff !important;\n}\n\n.capitalize {\n  text-transform: capitalize;\n}\n\n/* ion-badge {\n    --background: var(--cs-background-accent);\n    --color: var(--cs-text-secondary);\n    --padding-bottom: 4px;\n    --padding-end: 4px;\n    --padding-start: 4px;\n    --padding-top: 4px;\n\n} */\n\nion-chip {\n  border-color: #1ae5be !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JkZXJzL2VkaXQtb3JkZXItZGV0YWlsL2VkaXQtb3JkZXItZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUtBO0VBQ0ksYUFBQTtBQUZKOztBQUtBO0VBQ0ksWUFBQTtBQUZKOztBQUtBO0VBQ0ksY0FBQTtBQUZKOztBQUtBOztFQUVJLGtCQUFBO0VBQ0EsZ0JBQUE7QUFGSjs7QUFLQTtFQUNJLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0NBQUE7QUFGSjs7QUFLQTtFQUNJLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUFGSjs7QUFLQTtFQUNJLFlBQUE7RUFDQSxvQ0FBQTtBQUZKOztBQUtBO0VBQ0ksMEJBQUE7QUFGSjs7QUFPQTs7Ozs7Ozs7R0FBQTs7QUFVQTtFQUNJLGdDQUFBO0FBTEoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vcmRlcnMvZWRpdC1vcmRlci1kZXRhaWwvZWRpdC1vcmRlci1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibGFiZWwuY29vbGJveCB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMWFlNWJlO1xyXG4gICAgcGFkZGluZzogOXB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBsaW5lLWhlaWdodDogMTVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG5cclxuLy8gZm9ybS5uZy11bnRvdWNoZWQubmctcHJpc3RpbmUubmctdmFsaWQge1xyXG4vLyAgICAgZGlzcGxheTogZmxleDtcclxuLy8gfVxyXG5sYWJlbC5jb29sYm94IGlucHV0IHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbmxhYmVsLmNvb2xib3ggc3BhbiB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbmxhYmVsLmNvb2xib3ggaW5wdXQ6Y2hlY2tlZCtzcGFuIHtcclxuICAgIGNvbG9yOiByZ2IoMTUsIDE1LCAxNSk7XHJcbn1cclxuXHJcbmlvbi1kYXRldGltZS5tZC5kYXRldGltZS1wbGFjZWhvbGRlci5oeWRyYXRlZCxcclxuaW9uLWRhdGV0aW1lLm1kLmh5ZHJhdGVkIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTdweDtcclxuICAgIG1hcmdpbi10b3A6IC04cHg7XHJcbn1cclxuXHJcbi5zY2hlZHVsZV9idG4ge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzAzNmY1YTtcclxuICAgIGNvbG9yOiAjMWFlNWJlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXItY29sb3I6ICMxYWU1YmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnF0eV9idG4ge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzFhZTViYzM0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNDUlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIGNvbG9yOiAjMWFlNWJlO1xyXG4gICAgcGFkZGluZzogOXB4IDIwcHg7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNhcGl0YWxpemUge1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuXHJcblxyXG5cclxuLyogaW9uLWJhZGdlIHtcclxuICAgIC0tYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1hY2NlbnQpO1xyXG4gICAgLS1jb2xvcjogdmFyKC0tY3MtdGV4dC1zZWNvbmRhcnkpO1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogNHB4O1xyXG4gICAgLS1wYWRkaW5nLWVuZDogNHB4O1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiA0cHg7XHJcbiAgICAtLXBhZGRpbmctdG9wOiA0cHg7XHJcblxyXG59ICovXHJcblxyXG5pb24tY2hpcCB7XHJcbiAgICBib3JkZXItY29sb3I6ICMxYWU1YmUgIWltcG9ydGFudDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/orders/edit-order-detail/edit-order-detail.page.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/orders/edit-order-detail/edit-order-detail.page.ts ***!
  \**************************************************************************/
/*! exports provided: EditOrderDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditOrderDetailPage", function() { return EditOrderDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/schedule.service */ "./src/app/services/schedule.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");








let EditOrderDetailPage = class EditOrderDetailPage {
    constructor(activatedRoute, router, fb, productSer, scheduleSer, notify, datePipe) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.fb = fb;
        this.productSer = productSer;
        this.scheduleSer = scheduleSer;
        this.notify = notify;
        this.datePipe = datePipe;
        this.data = {};
        this.isChecked = true;
        this.weekdays = [];
        this.product_qty = 1;
        this.schedule_start_date = "";
        this.start_date = new Date();
        this.repeat_plan = "";
        this.checkbox = [
            { val: 'sun', name: 'Sunday', title: 'S' },
            { val: 'mon', name: 'Monday', title: 'M' },
            { val: 'tues', name: 'Tuesday', title: 'T' },
            { val: 'wed', name: 'Wednesday', title: 'W' },
            { val: 'thu', name: 'Thursday', title: 'T' },
            { val: 'fri', name: 'Friday', title: 'F' },
            { val: 'sat', name: 'Saturday', title: 'S' }
        ];
        this.activatedRoute.params.subscribe((params) => {
            this.cust_id = params['cust_id'];
            this.product_id = params['product_id'];
        });
        this.vendor_id = localStorage.getItem("vendor_id");
    }
    ngOnInit() {
        this.formBuilder();
        this.getProdutSchedule(this.cust_id, this.product_id);
        console.log("ids", this.cust_id, this.product_id, this.vendor_id);
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            start_date: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Sunday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Monday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Tuesday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Wednesday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Thursday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Friday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Saturday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
    }
    getProdutSchedule(cust_id, product_id) {
        let details = {
            cust_id: cust_id,
            product_id: product_id,
            vendor_id: this.vendor_id
        };
        this.scheduleSer.getProductSchedule(details)
            .subscribe((res) => {
            console.log("result", res);
            this.data = res['customerSchedulers'][0];
            let schedule_detail = this.data['productschedules'];
            for (let item of schedule_detail) {
                this.product_qty = item.quantity;
                this.frmFG.get(item.weekday).setValue(true);
                this.frmFG.get("start_date").setValue(item.start_date);
            }
        });
    }
    incrementQty() {
        this.data['rate'] = this.data['rate'] / this.product_qty;
        this.product_qty += 1;
        this.data['rate'] = this.data['rate'] * this.product_qty;
        console.log("data", this.data['rate']);
    }
    decrementQty() {
        if (this.product_qty - 1 < 1) {
            this.product_qty = 1;
        }
        else {
            this.data['rate'] = this.data['rate'] / this.product_qty;
            this.product_qty -= 1;
            this.data['rate'] = this.data['rate'] * this.product_qty;
            console.log("data", this.data['rate']);
        }
    }
    schedule(type) {
        this.repeat_plan = type;
        console.log("this.repeat_plan", this.repeat_plan);
        if (this.repeat_plan == "daily") {
            this.frmFG.patchValue({
                Sunday: true,
                Monday: true,
                Tuesday: true,
                Wednesday: true,
                Thursday: true,
                Friday: true,
                Saturday: true,
            });
        }
        if (this.repeat_plan == "alternate") {
            var first_selected_day = this.frmFG.get("Sunday").value;
            console.log("day", first_selected_day);
            if (first_selected_day == !false) {
                this.frmFG.patchValue({
                    Sunday: false,
                    Monday: true,
                    Tuesday: false,
                    Wednesday: true,
                    Thursday: false,
                    Friday: true,
                    Saturday: false,
                });
            }
            else {
                this.frmFG.patchValue({
                    Sunday: true,
                    Monday: false,
                    Tuesday: true,
                    Wednesday: false,
                    Thursday: true,
                    Friday: false,
                    Saturday: true,
                });
            }
        }
        if (this.repeat_plan == "weekends") {
            this.frmFG.patchValue({
                Sunday: true,
                Monday: false,
                Tuesday: false,
                Wednesday: false,
                Thursday: false,
                Friday: false,
                Saturday: true,
            });
        }
    }
    submit() {
        var selectedDays = this.frmFG.value;
        console.log("date", selectedDays);
        let details = {
            cust_id: this.cust_id,
            product_id: this.product_id,
            vendor_id: this.vendor_id
        };
        this.scheduleSer.deleteItem(details)
            .subscribe((res) => {
            var selectedDays = this.frmFG.value;
            console.log("date", selectedDays);
            if (selectedDays.start_date == "Tomorrow") {
                selectedDays.start_date = this.datePipe.transform(this.start_date, 'yyyy-MM-dd');
            }
            for (let item of this.checkbox) {
                if (selectedDays[item.name]) {
                    let data = {
                        cust_id: this.cust_id,
                        product_id: this.product_id,
                        unit: this.data['unit'],
                        vendor_id: localStorage.getItem('vendor_id'),
                        weekdays: item.name,
                        quantity: this.product_qty,
                        start_date: selectedDays.start_date.substring(0, 10)
                    };
                    this.scheduleSer.addItem(data)
                        .subscribe((res) => {
                        this.notify.success("Schedule created successfully");
                        console.log("result", res);
                    }, (err) => {
                        this.notify.error("Please try again");
                        console.log("error", err);
                    });
                }
            }
            this.router.navigate(['/customers']);
        }, (err) => {
            this.notify.error("Please try again");
        });
    }
};
EditOrderDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_4__["ProductService"] },
    { type: _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__["ScheduleService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"] }
];
EditOrderDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-order-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-order-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/edit-order-detail/edit-order-detail.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-order-detail.page.scss */ "./src/app/pages/orders/edit-order-detail/edit-order-detail.page.scss")).default]
    })
], EditOrderDetailPage);



/***/ })

}]);
//# sourceMappingURL=edit-order-detail-edit-order-detail-module.js.map