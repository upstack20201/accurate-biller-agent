(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-delivery-agent-add-delivery-agent-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.html ***!
  \****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/delivery-agent\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Add Delivery Agent</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<!-- FORMS --COMPONENT 3 -->\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n    <ion-row class=\"ion-padding-top ion-padding-bottom\">\r\n      <ion-col (change)=\"readUrl($event)\" class=\"ion-align-self-start ion-no-padding ion-margin-bottom\">\r\n        <input id=\"fileInput\" formControlName=\"file\" type=\"file\" accept=\"image/*\" />\r\n          <ion-label for=\"fileInput\" size=\"12\" class=\"text-size-sm ion-text-capitalize ion-text-center\">\r\n            <img class=\"ion-margin-bottom\" [src]=\"url1\" height=\"70\">\r\n            <p>Upload Photo</p> \r\n          </ion-label>\r\n      </ion-col>\r\n      <!-- Full Name -->\r\n      <ion-col size=\"12\" class=\"ion-align-self-start ion-no-padding\">\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">First Name\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n              [placeholder]=\"\" formControlName=\"firstName\"></ion-input>\r\n          </ion-item>\r\n          <!-- Last Name -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Last Name\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n              [placeholder]=\"\" formControlName=\"lastName\"></ion-input>\r\n          </ion-item>\r\n          <!-- Email -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Email\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"email\" [placeholder]=\"\"\r\n              formControlName=\"email\"></ion-input>\r\n          </ion-item>\r\n          <!-- Mobile -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Contact\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" maxlength=\"10\"\r\n              [placeholder]=\"\" formControlName=\"mobile\"></ion-input>\r\n          </ion-item>\r\n          <!-- Address Line 1 -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Address\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"\r\n              formControlName=\"address\"></ion-input>\r\n          </ion-item>\r\n          <!-- Area -->\r\n          <!-- <ion-item class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\" lines=\"none\">\r\n          <ion-label class=\"required ion-margin-start\">Area</ion-label>  \r\n          <ion-input type=\"text\" [placeholder]=\"\" formControlName=\"area\"></ion-input>\r\n         </ion-item> -->\r\n          <!-- Zip Code -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Zipcode\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" maxlength=\"6\"\r\n              [placeholder]=\"\" formControlName=\"zipcode\"></ion-input>\r\n          </ion-item>\r\n            <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n            [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n            Save\r\n          </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </form>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n<!-- <ion-footer>\r\n  <ion-toolbar>\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \"\r\n      [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n      Save\r\n    </ion-button>\r\n\r\n  </ion-toolbar>\r\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent-routing.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent-routing.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: AddDeliveryAgentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDeliveryAgentPageRoutingModule", function() { return AddDeliveryAgentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _add_delivery_agent_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-delivery-agent.page */ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.ts");




const routes = [
    {
        path: '',
        component: _add_delivery_agent_page__WEBPACK_IMPORTED_MODULE_3__["AddDeliveryAgentPage"]
    }
];
let AddDeliveryAgentPageRoutingModule = class AddDeliveryAgentPageRoutingModule {
};
AddDeliveryAgentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddDeliveryAgentPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.module.ts ***!
  \**************************************************************************************/
/*! exports provided: AddDeliveryAgentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDeliveryAgentPageModule", function() { return AddDeliveryAgentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _add_delivery_agent_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-delivery-agent-routing.module */ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent-routing.module.ts");
/* harmony import */ var _add_delivery_agent_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-delivery-agent.page */ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.ts");








let AddDeliveryAgentPageModule = class AddDeliveryAgentPageModule {
};
AddDeliveryAgentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _add_delivery_agent_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddDeliveryAgentPageRoutingModule"]
        ],
        declarations: [_add_delivery_agent_page__WEBPACK_IMPORTED_MODULE_6__["AddDeliveryAgentPage"]]
    })
], AddDeliveryAgentPageModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.scss ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#fileInput {\n  position: absolute;\n  opacity: 0;\n  height: 93px !important;\n}\n\nimg {\n  display: block !important;\n  margin-left: auto !important;\n  margin-right: auto !important;\n  border-radius: 20%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnktYWdlbnQvYWRkLWRlbGl2ZXJ5LWFnZW50L2FkZC1kZWxpdmVyeS1hZ2VudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUVFO0VBQ0EseUJBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5LWFnZW50L2FkZC1kZWxpdmVyeS1hZ2VudC9hZGQtZGVsaXZlcnktYWdlbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2ZpbGVJbnB1dCB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgaGVpZ2h0OiA5M3B4ICFpbXBvcnRhbnRcclxuICB9XHJcblxyXG4gIGltZyB7XHJcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcclxuICBtYXJnaW4tbGVmdDogYXV0byAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1yaWdodDogYXV0byAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwJTtcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.ts":
/*!************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.ts ***!
  \************************************************************************************/
/*! exports provided: AddDeliveryAgentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDeliveryAgentPage", function() { return AddDeliveryAgentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let AddDeliveryAgentPage = class AddDeliveryAgentPage {
    constructor(fb, deliveryAgentSer, notify, router) {
        this.fb = fb;
        this.deliveryAgentSer = deliveryAgentSer;
        this.notify = notify;
        this.router = router;
        this.url1 = 'assets/imgs/avatar/avatardefault.png';
    }
    ngOnInit() {
        this.formBuilder();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            file: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z]+$")]),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z]+$")]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$")]),
            mobile: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]{10}")]),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]{6}")]),
        });
    }
    submit() {
        var data = this.frmFG.value;
        console.log("data", data);
        data.name = this.capitalization(data.firstName) + ' ' + this.capitalization(data.lastName);
        data.vendor_id = localStorage.getItem("vendor_id");
        data.file = this.url1;
        this.deliveryAgentSer.addItem(data)
            .subscribe((res) => {
            this.notify.success('Agent created successfully');
            this.router.navigate(['/delivery-agent']);
        }, (err) => {
            this.notify.error('Please try again');
            console.log("error", err);
        });
    }
    capitalization(input) {
        return (!!input) ? input.split(' ').map(function (wrd) { return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase(); }).join(' ') : '';
    }
    readUrl(event) {
        console.log('readUrl');
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onload = (event) => {
                this.url1 = event.target.result;
                console.log("image", this.url1);
            };
        }
    }
};
AddDeliveryAgentPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_3__["DeliveryAgentService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
AddDeliveryAgentPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-delivery-agent',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-delivery-agent.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-delivery-agent.page.scss */ "./src/app/pages/delivery-agent/add-delivery-agent/add-delivery-agent.page.scss")).default]
    })
], AddDeliveryAgentPage);



/***/ })

}]);
//# sourceMappingURL=add-delivery-agent-add-delivery-agent-module.js.map