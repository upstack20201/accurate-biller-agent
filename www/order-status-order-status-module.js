(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-status-order-status-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/order-status/order-status.page.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/order-status/order-status.page.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/delivery-agent\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">\r\n      <h6 class=\"text-size-xs font-bold ion-text-wrap\">\r\n        {{today | date:'dd'}} {{today | date:'LLLL'}}</h6>\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\">\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list *ngFor=\"let item of data\">\r\n          <ion-row>\r\n            <h4 class=\"text-size-sm text-color-primary ion-padding-start font-bold ion-text-wrap\">\r\n              {{item.name}}\r\n            </h4>\r\n            <ion-item>\r\n              <ion-label class=\"\">\r\n                <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n                  <img src={{item.product_img}}>\r\n                </ion-thumbnail>\r\n              </ion-label>\r\n              <ion-label class=\"ion-padding-start\">\r\n                <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                  {{item.title}}\r\n                </h6>\r\n                <h6 style=\"display: flex;\" class=\"text-size-sd text-color-primary ion-text-wrap\" color=\"medium\">\r\n                  (1 {{item.unit}})\r\n                </h6>\r\n                <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                  Rs. {{item.totalcost}}\r\n                </h6>\r\n              </ion-label>\r\n              <form [formGroup]=\"frmFG\">\r\n                <ion-label class=\"ion-margin-end\">\r\n                  <ion-radio-group formControlName=\"status\" (click)=\"onStatusChange(item)\" style=\"display: flex;\">\r\n                    <ion-item lines=\"none\">\r\n                      <ion-radio slot=\"start\" value=\"delivered\"></ion-radio>\r\n                      <ion-label class=\"text-size-sd text-color-primary\">&nbsp;Delivered\r\n                      </ion-label>\r\n                    </ion-item>\r\n                    <ion-item lines=\"none\">\r\n                      <ion-radio slot=\"start\" value=\"cancelled\"></ion-radio>\r\n                      <ion-label class=\"text-size-sd text-color-primary\">&nbsp;Cancelled\r\n                      </ion-label>\r\n                    </ion-item>\r\n                  </ion-radio-group>\r\n                </ion-label>\r\n              </form>\r\n            </ion-item>\r\n          </ion-row>\r\n        </ion-list>\r\n        <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\r\n          <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n          </ion-infinite-scroll-content>\r\n        </ion-infinite-scroll>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n<!-- <ion-footer>\r\n  <ion-toolbar>\r\n\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \" expand=\"block\">\r\n      Submit\r\n    </ion-button>\r\n  </ion-toolbar>\r\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/pages/delivery-agent/order-status/order-status-routing.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/order-status/order-status-routing.module.ts ***!
  \**********************************************************************************/
/*! exports provided: OrderStatusPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderStatusPageRoutingModule", function() { return OrderStatusPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _order_status_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-status.page */ "./src/app/pages/delivery-agent/order-status/order-status.page.ts");




const routes = [
    {
        path: '',
        component: _order_status_page__WEBPACK_IMPORTED_MODULE_3__["OrderStatusPage"]
    }
];
let OrderStatusPageRoutingModule = class OrderStatusPageRoutingModule {
};
OrderStatusPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderStatusPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/order-status/order-status.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/order-status/order-status.module.ts ***!
  \**************************************************************************/
/*! exports provided: OrderStatusPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderStatusPageModule", function() { return OrderStatusPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _order_status_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-status-routing.module */ "./src/app/pages/delivery-agent/order-status/order-status-routing.module.ts");
/* harmony import */ var _order_status_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-status.page */ "./src/app/pages/delivery-agent/order-status/order-status.page.ts");








let OrderStatusPageModule = class OrderStatusPageModule {
};
OrderStatusPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _order_status_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderStatusPageRoutingModule"]
        ],
        declarations: [_order_status_page__WEBPACK_IMPORTED_MODULE_6__["OrderStatusPage"]]
    })
], OrderStatusPageModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/order-status/order-status.page.scss":
/*!**************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/order-status/order-status.page.scss ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".active ion-toolbar {\n  background: var(--cs-background-primary) !important;\n  color: var(--cs-text-accent) !important;\n}\n.active ion-back-button,\n.active ion-menu-button {\n  --color: var(--cs-text-accent) !important;\n}\nion-thumbnail {\n  display: block !important;\n  margin-left: auto !important;\n  margin-right: auto !important;\n}\n.header-image {\n  position: relative;\n  z-index: 999;\n}\n.background-size {\n  height: 140px;\n  position: relative;\n}\n.background-size::after {\n  content: \"\";\n  background: var(--cs-gradient-image);\n  position: absolute;\n  height: 100%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.info-block ion-thumbnail {\n  height: 100px;\n  width: 100px;\n  opacity: 1 !important;\n  z-index: 999;\n  position: relative;\n}\nion-grid {\n  position: relative;\n  z-index: 1;\n}\n.page1 .background-size {\n  height: 250px;\n}\n.page1 .background-size ion-card-content {\n  position: absolute;\n  bottom: 0;\n}\n.product-section {\n  border: 1px solid #cccccc;\n  background: var(--cs-background-secondary) !important;\n  border-radius: 8px;\n}\n.icon-vertical {\n  display: inline-block !important;\n}\n.day-align {\n  display: flex;\n  letter-spacing: 4px;\n  font-family: sans-serif;\n  font-size: smaller;\n  color: #c3cac8;\n}\nion-label {\n  flex: none !important;\n}\nion-radio {\n  width: 12px !important;\n  height: 12px !important;\n}\n.product-freeze {\n  position: sticky;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 9999;\n  background-color: #ffffff;\n  border-bottom: 1px solid #e2e1e1;\n}\nh4.text-size-sm.text-color-primary.ion-padding-start.font-bold.ion-text-wrap {\n  margin-bottom: -4px;\n  margin-top: 2px;\n  color: #1ae5be !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGVsaXZlcnktYWdlbnQvb3JkZXItc3RhdHVzL29yZGVyLXN0YXR1cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSxtREFBQTtFQUNBLHVDQUFBO0FBQU47QUFHSTs7RUFFRSx5Q0FBQTtBQUROO0FBS0U7RUFDRSx5QkFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUFGSjtBQUtFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0FBRko7QUFLRTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtBQUZKO0FBSUk7RUFDRSxXQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7QUFGTjtBQU9JO0VBQ0UsYUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQUxOO0FBU0U7RUFDRSxrQkFBQTtFQUNBLFVBQUE7QUFOSjtBQVVJO0VBQ0UsYUFBQTtBQVBOO0FBU007RUFDRSxrQkFBQTtFQUNBLFNBQUE7QUFQUjtBQVlFO0VBQ0UseUJBQUE7RUFDQSxxREFBQTtFQUNBLGtCQUFBO0FBVEo7QUFZRTtFQUNFLGdDQUFBO0FBVEo7QUFZRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBVEo7QUFZRTtFQUNFLHFCQUFBO0FBVEo7QUFZRTtFQUNFLHNCQUFBO0VBQ0EsdUJBQUE7QUFUSjtBQVlFO0VBQ0UsZ0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxnQ0FBQTtBQVRKO0FBWUU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBQVRKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGVsaXZlcnktYWdlbnQvb3JkZXItc3RhdHVzL29yZGVyLXN0YXR1cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWN0aXZlIHtcclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgICBjb2xvcjogdmFyKC0tY3MtdGV4dC1hY2NlbnQpICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgXHJcbiAgICBpb24tYmFjay1idXR0b24sXHJcbiAgICBpb24tbWVudS1idXR0b24ge1xyXG4gICAgICAtLWNvbG9yOiB2YXIoLS1jcy10ZXh0LWFjY2VudCkgIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgaW9uLXRodW1ibmFpbCB7XHJcbiAgICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG8gIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1yaWdodDogYXV0byAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAuaGVhZGVyLWltYWdlIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICB9XHJcbiAgXHJcbiAgLmJhY2tncm91bmQtc2l6ZSB7XHJcbiAgICBoZWlnaHQ6IDE0MHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIFxyXG4gICAgJjo6YWZ0ZXIge1xyXG4gICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1jcy1ncmFkaWVudC1pbWFnZSk7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICBib3R0b206IDA7XHJcbiAgICAgIGxlZnQ6IDA7XHJcbiAgICAgIHJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICAuaW5mby1ibG9jayB7XHJcbiAgICBpb24tdGh1bWJuYWlsIHtcclxuICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICBvcGFjaXR5OiAxICFpbXBvcnRhbnQ7XHJcbiAgICAgIC8vbWFyZ2luOiAtNzBweCBhdXRvIDA7XHJcbiAgICAgIHotaW5kZXg6IDk5OTtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICBpb24tZ3JpZCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gIH1cclxuICBcclxuICAucGFnZTEge1xyXG4gICAgLmJhY2tncm91bmQtc2l6ZSB7XHJcbiAgICAgIGhlaWdodDogMjUwcHg7XHJcbiAgXHJcbiAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgLnByb2R1Y3Qtc2VjdGlvbiB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjY2NjO1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1zZWNvbmRhcnkpICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5pY29uLXZlcnRpY2FsIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAuZGF5LWFsaWduIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogNHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IHNtYWxsZXI7XHJcbiAgICBjb2xvcjogcmdiKDE5NSAyMDIgMjAwKTtcclxuICB9XHJcbiAgXHJcbiAgaW9uLWxhYmVsIHtcclxuICAgIGZsZXg6IG5vbmUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgaW9uLXJhZGlvIHtcclxuICAgIHdpZHRoOiAxMnB4ICFpbXBvcnRhbnQ7XHJcbiAgICBoZWlnaHQ6IDEycHggIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgLnByb2R1Y3QtZnJlZXplIHtcclxuICAgIHBvc2l0aW9uOiBzdGlja3k7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICB6LWluZGV4OiA5OTk5O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTJlMWUxO1xyXG4gIH1cclxuICBcclxuICBoNC50ZXh0LXNpemUtc20udGV4dC1jb2xvci1wcmltYXJ5Lmlvbi1wYWRkaW5nLXN0YXJ0LmZvbnQtYm9sZC5pb24tdGV4dC13cmFwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IC00cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICBjb2xvcjogIzFhZTViZSAhaW1wb3J0YW50O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/delivery-agent/order-status/order-status.page.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/order-status/order-status.page.ts ***!
  \************************************************************************/
/*! exports provided: OrderStatusPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderStatusPage", function() { return OrderStatusPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");
/* harmony import */ var _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/schedule.service */ "./src/app/services/schedule.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");










let OrderStatusPage = class OrderStatusPage {
    constructor(activatedRoute, fb, datePipe, deliveryAgentSer, scheduleSer, alertCtrl, notify) {
        this.activatedRoute = activatedRoute;
        this.fb = fb;
        this.datePipe = datePipe;
        this.deliveryAgentSer = deliveryAgentSer;
        this.scheduleSer = scheduleSer;
        this.alertCtrl = alertCtrl;
        this.notify = notify;
        this.today = new Date();
        this.activatedRoute.params.subscribe((params) => {
            this.agent_id = params['agent_id'];
        });
        activatedRoute.params.subscribe(val => {
            this.vendor_id = localStorage.getItem("vendor_id");
            this.getOrderList();
        });
    }
    loadData(event) {
        setTimeout(() => {
            //console.log('Done');
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            /* if (this.data.length == 1000) {
              event.target.disabled = true;
            } */
        }, 500);
    }
    toggleInfiniteScroll() {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
    ngOnInit() {
        this.formBuilder();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
    }
    getOrderList() {
        var order_date = this.datePipe.transform(this.today, 'yyyy-MM-dd');
        var detail = {
            vendor_id: this.vendor_id,
            agent_id: this.agent_id,
            status: 'scheduled',
            order_date: order_date
        };
        this.deliveryAgentSer.getAgentwiseCustomer(detail)
            .subscribe((res) => {
            this.data = res['agentCustomers'];
        });
    }
    onStatusChange(data) {
        const status = this.frmFG.get("status").value;
        let details = {
            quantity: data.quantity,
            status: status,
            delivered_by: this.agent_id
        };
        this.scheduleSer.changeOrderStatus(data.id, details)
            .subscribe((res) => {
            this.notify.success("Update status successfully");
            this.frmFG.get("status").setValue('');
            this.getOrderList();
        }, (err) => {
            this.notify.error("Please try again");
        });
    }
};
OrderStatusPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__["DeliveryAgentService"] },
    { type: _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__["ScheduleService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_8__["NotifyService"] }
];
OrderStatusPage.propDecorators = {
    infiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInfiniteScroll"],] }]
};
OrderStatusPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-status',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./order-status.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/order-status/order-status.page.html")).default,
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"], _notifications_notify_service__WEBPACK_IMPORTED_MODULE_8__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./order-status.page.scss */ "./src/app/pages/delivery-agent/order-status/order-status.page.scss")).default]
    })
], OrderStatusPage);



/***/ })

}]);
//# sourceMappingURL=order-status-order-status-module.js.map