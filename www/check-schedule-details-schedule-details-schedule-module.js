(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["check-schedule-details-schedule-details-schedule-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- EXPANDABLE --COMPONENT 1 -->\r\n<!-- Header -->\r\n<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/schedule/check-schedule\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Details Schedule</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content *ngIf=\"data != null\">\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row *ngIf=\"data != null\">\r\n      <ion-col size=\"12\">\r\n        <ion-list class=\"ion-no-margin\">\r\n          <!-- Header List Big Image -->\r\n          <!-- <ion-list-header>\r\n              <ion-label class=\"ion-padding-top ion-padding-bottom\">\r\n                <h1 class=\"text-size-md font-regular text-color-accent ion-no-margin\">\r\n                  {{data.header}}</h1>\r\n              </ion-label>\r\n            </ion-list-header> -->\r\n          <ul *ngIf=\"!onLeave\" class=\"collapsible ion-no-margin ion-no-padding\">\r\n            <li *ngFor=\"let group of data.items;\">\r\n              <!-- List big image Header -->\r\n              <div class=\"collapsible-header ion-no-padding\" (click)=\"toggleGroup(group)\">\r\n                <ion-item class=\"default-item box-shadow ion-no-padding\" lines=\"none\">\r\n                  <ion-thumbnail class=\"border-radius-right\" slot=\"start\">\r\n                    <ion-img [src]=\"group.image\" [alt]=\"group.title\"></ion-img>\r\n                  </ion-thumbnail>\r\n                  <ion-label class=\"ion-padding-start\">\r\n                    <h3 class=\"text-size-sm font-bold text-color-primary ion-text-wrap\">\r\n                      {{group.title}}</h3>\r\n                    <p class=\"text-size-sm font-regular ion-text-wrap\">\r\n                      {{group.description}}</p>\r\n                  </ion-label>\r\n                </ion-item>\r\n              </div><!-- End List big image Header -->\r\n              <!-- LIST OPEN ACCORDIAN BODY  -->\r\n              <div class=\"item-accordion\" [ngClass]=\"{'active': group.show }\" [hidden]=\"!group.show\">\r\n                <ion-item class=\"default-item box-shadow ion-no-padding\" lines=\"none\"\r\n                  *ngFor=\"let item of group.expandItems;\" (click)=\"onItemClickFunc(item, $event)\">\r\n                  <!-- Avatar -->\r\n                  <ion-thumbnail class=\"border-radius-left\" slot=\"end\">\r\n                    <img [src]=\"item.image\" [alt]=\"item.title\" />\r\n                  </ion-thumbnail>\r\n                  <ion-label class=\"ion-padding-start\">\r\n                    <!-- Subtitle Title -->\r\n                    <h3 class=\"text-size-sm font-bold text-color-primary ion-text-nowrap\">\r\n                      {{item.title}}</h3>\r\n                    <!-- Subtitle Subtitle -->\r\n                    <h3 class=\"text-size-sm text-color-primary font-regular ion-text-wrap\">\r\n                      {{item.description}}</h3>\r\n                    <p class=\"text-size-sm font-light ion-text-nowrap\">\r\n                      {{item.details}}</p>\r\n                  </ion-label>\r\n                </ion-item>\r\n              </div>\r\n              <!-- End List big image Body -->\r\n            </li>\r\n          </ul>\r\n\r\n          <ul *ngIf=\"onLeave\" class=\"collapsible ion-no-margin ion-no-padding\">\r\n            <li *ngFor=\"let group of data.items;\">\r\n              <!-- List big image Header -->\r\n              <div class=\"collapsible-header ion-no-padding\" (click)=\"toggleGroup(group)\">\r\n                <ion-item class=\"default-item box-shadow ion-no-padding\" lines=\"none\">\r\n                  <ion-thumbnail class=\"border-radius-right\" slot=\"start\">\r\n                    <ion-img [src]=\"group.image\" [alt]=\"group.title\"></ion-img>\r\n                  </ion-thumbnail>\r\n                  <ion-label class=\"ion-padding-start\">\r\n                    <h3 class=\"text-size-sm font-bold text-color-primary ion-text-wrap\">\r\n                      {{group.title}}</h3>\r\n                    <p class=\"text-size-sm font-regular ion-text-wrap\">\r\n                      {{group.description}}</p>\r\n                  </ion-label>\r\n                  <ion-checkbox class=\"ion-margin-end\" type=\"checkbox\" slot=\"end\"></ion-checkbox>\r\n                </ion-item>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule-routing.module.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/details-schedule/details-schedule-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: DetailsSchedulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsSchedulePageRoutingModule", function() { return DetailsSchedulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _details_schedule_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./details-schedule.page */ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.ts");




const routes = [
    {
        path: '',
        component: _details_schedule_page__WEBPACK_IMPORTED_MODULE_3__["DetailsSchedulePage"]
    }
];
let DetailsSchedulePageRoutingModule = class DetailsSchedulePageRoutingModule {
};
DetailsSchedulePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetailsSchedulePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: DetailsSchedulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsSchedulePageModule", function() { return DetailsSchedulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _details_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./details-schedule-routing.module */ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule-routing.module.ts");
/* harmony import */ var _details_schedule_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./details-schedule.page */ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.ts");







let DetailsSchedulePageModule = class DetailsSchedulePageModule {
};
DetailsSchedulePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _details_schedule_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetailsSchedulePageRoutingModule"]
        ],
        declarations: [_details_schedule_page__WEBPACK_IMPORTED_MODULE_6__["DetailsSchedulePage"]]
    })
], DetailsSchedulePageModule);



/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Style component 1\n========================================================*/\n.collapsible-header {\n  padding: 4px 8px 4px 0;\n}\n.collapsible-header .default-item {\n  width: 100% !important;\n  margin: 0 !important;\n}\n/* Settings Accordion \"OPEN\" */\n.item-accordion {\n  transition: all 400ms;\n  max-height: 0;\n  display: block !important;\n  overflow: hidden;\n}\n.item-accordion.active {\n  transition: all 400ms;\n  overflow: visible;\n  max-height: 999px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2NoZWR1bGUvY2hlY2stc2NoZWR1bGUvZGV0YWlscy1zY2hlZHVsZS9kZXRhaWxzLXNjaGVkdWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTt5REFBQTtBQUVBO0VBQ0ksc0JBQUE7QUFDSjtBQUNJO0VBQ0Usc0JBQUE7RUFDQSxvQkFBQTtBQUNOO0FBRUUsOEJBQUE7QUFDQTtFQUtFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7QUFDSjtBQUNJO0VBS0UscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FBQ04iLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zY2hlZHVsZS9jaGVjay1zY2hlZHVsZS9kZXRhaWxzLXNjaGVkdWxlL2RldGFpbHMtc2NoZWR1bGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogU3R5bGUgY29tcG9uZW50IDFcclxuPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG4uY29sbGFwc2libGUtaGVhZGVyIHtcclxuICAgIHBhZGRpbmc6IDRweCA4cHggNHB4IDA7XHJcbiAgXHJcbiAgICAuZGVmYXVsdC1pdGVtIHtcclxuICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8qIFNldHRpbmdzIEFjY29yZGlvbiBcIk9QRU5cIiAqL1xyXG4gIC5pdGVtLWFjY29yZGlvbiB7XHJcbiAgICAtbW96LXRyYW5zaXRpb246IGFsbCA0MDBtcztcclxuICAgIC1tcy10cmFuc2l0aW9uOiBhbGwgNDAwbXM7XHJcbiAgICAtby10cmFuc2l0aW9uOiBhbGwgNDAwbXM7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCA0MDBtcztcclxuICAgIHRyYW5zaXRpb246IGFsbCA0MDBtcztcclxuICAgIG1heC1oZWlnaHQ6IDA7XHJcbiAgICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBcclxuICAgICYuYWN0aXZlIHtcclxuICAgICAgLW1vei10cmFuc2l0aW9uOiBhbGwgNDAwbXM7XHJcbiAgICAgIC1tcy10cmFuc2l0aW9uOiBhbGwgNDAwbXM7XHJcbiAgICAgIC1vLXRyYW5zaXRpb246IGFsbCA0MDBtcztcclxuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgNDAwbXM7XHJcbiAgICAgIHRyYW5zaXRpb246IGFsbCA0MDBtcztcclxuICAgICAgb3ZlcmZsb3c6IHZpc2libGU7XHJcbiAgICAgIG1heC1oZWlnaHQ6IDk5OXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAiXX0= */");

/***/ }),

/***/ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: DetailsSchedulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsSchedulePage", function() { return DetailsSchedulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let DetailsSchedulePage = class DetailsSchedulePage {
    constructor() {
        this.onLeave = true;
        this.data = {
            'toolbarTitle': 'List big image',
            "header": "Catalogue",
            "items": [
                {
                    "id": 1,
                    "title": "Black dualshock",
                    "description": "katraj",
                    "image": "assets/imgs/products/boy-user.jpg",
                    "expandItems": [
                        {
                            "id": 1,
                            "title": "Buffalo Milk",
                            "description": "500 ML",
                            "image": "assets/imgs/products/milk.jpg",
                            "details": "Rs. 36"
                        },
                        {
                            "id": 2,
                            "title": "Curd",
                            "description": "400 GM",
                            "image": "assets/imgs/avatar/6.jpg",
                            "details": "Rs. 30"
                        }
                    ]
                },
                {
                    "id": 2,
                    "title": "Keyboard",
                    "description": "deccan",
                    "image": "assets/imgs/products/girl-user.png",
                    "expandItems": [
                        {
                            "id": 1,
                            "title": "Buffalo Milk",
                            "description": "500 ML",
                            "image": "assets/imgs/avatar/3.jpg",
                            "details": "Rs. 36"
                        },
                        {
                            "id": 2,
                            "title": "Taaza Paneer",
                            "description": "200 GM",
                            "image": "assets/imgs/avatar/7.jpg",
                            "details": "Rs. 79"
                        },
                    ]
                },
                {
                    "id": 3,
                    "title": "Watch Black",
                    "description": "karve nagar",
                    "image": "assets/imgs/products/boy-user.jpg",
                    "expandItems": [
                        {
                            "id": 1,
                            "title": "Cow Milk",
                            "description": "500 ML",
                            "image": "assets/imgs/avatar/2.jpg",
                            "details": "Rs. 30"
                        },
                    ]
                },
            ]
        };
    }
    ngOnInit() {
    }
    toggleGroup(group) {
        if (event) {
            event.stopPropagation();
        }
        group.show = !group.show;
    }
};
DetailsSchedulePage.ctorParameters = () => [];
DetailsSchedulePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-details-schedule',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./details-schedule.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./details-schedule.page.scss */ "./src/app/pages/schedule/check-schedule/details-schedule/details-schedule.page.scss")).default]
    })
], DetailsSchedulePage);



/***/ })

}]);
//# sourceMappingURL=check-schedule-details-schedule-details-schedule-module.js.map