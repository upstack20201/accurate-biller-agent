(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bill-bill-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bill/bill.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bill/bill.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/product\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Bills</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\" parallax-header>\r\n  <!--Content-->\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list class=\"ion-no-margin\" *ngIf=\"data && data.customers; else skeleton\">\r\n          <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of data.customers\">\r\n            <!--Content Avatar-->\r\n            <!--    <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n              <img src=\"{{item.profilepic}}\">\r\n            </ion-thumbnail> -->\r\n            <ion-label class=\"ion-padding-start\" (click)=\"customerDetail(item.id)\">\r\n              <!-- Ttile and Subtitle -->\r\n              <h3 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                {{item.name}}</h3>\r\n              <p class=\"text-size-sm font-regular ion-text-wrap\">\r\n                <!--  {{item.address}} --> Total - Rs. 2000\r\n              </p>\r\n            </ion-label>\r\n            <!-- <ion-button class=\"button-icon ion-margin-end\" slot=\"end\">\r\n                <ion-icon class=\"icon-font-md\" name=\"create\"></ion-icon>\r\n                <ion-icon class=\"icon-font-md\" name=\"create\"></ion-icon>\r\n              </ion-button> -->\r\n            <ion-label class=\"ion-text-end ion-margin\" matTooltip=\"View Bill\">\r\n              <ion-icon class=\"icon icon-color-accent icon-font-sm font-bold icon-receipt\" (click)=\"edit(item.id)\">\r\n              </ion-icon>&nbsp;\r\n              <!--  <ion-icon class=\"icon icon-font-sm font-bold ion-margin-end\" color=\"medium\" (click)=\"remove(item)\"\r\n                name=\"trash\">\r\n              </ion-icon> -->\r\n            </ion-label>\r\n\r\n          </ion-item>\r\n        </ion-list>\r\n\r\n        <ng-template #skeleton>\r\n          <ion-list class=\"ion-no-margin\" className=\"fadeInLeft\">\r\n            <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of [1, 2, 3, 4, 5]\">\r\n              <ion-thumbnail class=\"border-radius ion-padding-start\" slot=\"start\">\r\n                <ion-avatar>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-avatar>\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding\">\r\n                <h2>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </h2>\r\n                <p>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </p>\r\n              </ion-label>\r\n              <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-end\">\r\n                <ion-item>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-item>\r\n                <ion-item>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-item>\r\n\r\n              </ion-buttons>\r\n            </ion-item>\r\n          </ion-list>\r\n        </ng-template>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/bill/bill-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/bill/bill-routing.module.ts ***!
  \***************************************************/
/*! exports provided: BillPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillPageRoutingModule", function() { return BillPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _bill_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bill.page */ "./src/app/pages/bill/bill.page.ts");




const routes = [
    {
        path: '',
        component: _bill_page__WEBPACK_IMPORTED_MODULE_3__["BillPage"]
    }
];
let BillPageRoutingModule = class BillPageRoutingModule {
};
BillPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BillPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/bill/bill.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/bill/bill.module.ts ***!
  \*******************************************/
/*! exports provided: BillPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillPageModule", function() { return BillPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _bill_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bill-routing.module */ "./src/app/pages/bill/bill-routing.module.ts");
/* harmony import */ var _bill_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bill.page */ "./src/app/pages/bill/bill.page.ts");







let BillPageModule = class BillPageModule {
};
BillPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _bill_routing_module__WEBPACK_IMPORTED_MODULE_5__["BillPageRoutingModule"]
        ],
        declarations: [_bill_page__WEBPACK_IMPORTED_MODULE_6__["BillPage"]]
    })
], BillPageModule);



/***/ }),

/***/ "./src/app/pages/bill/bill.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/bill/bill.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".active ion-toolbar {\n  background: var(--cs-background-primary) !important;\n  color: var(--cs-text-accent) !important;\n}\n.active ion-back-button,\n.active ion-menu-button {\n  --color: var(--cs-text-accent) !important;\n}\n.background-size {\n  height: 400px;\n  position: relative;\n  /* Parallax Title\n     =============================================*/\n}\n.background-size::after {\n  content: \"\";\n  background: var(--cs-gradient-image);\n  position: absolute;\n  height: 100%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.background-size .parallax-title-section {\n  position: absolute;\n  bottom: 16px;\n  left: 16px;\n  right: 16px;\n}\n.icon-vertical {\n  display: inline-block !important;\n}\nion-skeleton-text {\n  line-height: 13px;\n  --border-radius: 20px;\n}\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmlsbC9iaWxsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLG1EQUFBO0VBQ0EsdUNBQUE7QUFBSjtBQUdFOztFQUVFLHlDQUFBO0FBREo7QUFLQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQVlBO21EQUFBO0FBWkY7QUFFRTtFQUNFLFdBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQUFKO0FBS0U7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQUhKO0FBT0E7RUFDRSxnQ0FBQTtBQUpGO0FBT0E7RUFDRSxpQkFBQTtFQUVBLHFCQUFBO0FBTEY7QUFRQTtFQUNFLGtCQUFBO0FBTEYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9iaWxsL2JpbGwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjdGl2ZSB7XHJcbiAgaW9uLXRvb2xiYXIge1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHZhcigtLWNzLXRleHQtYWNjZW50KSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgaW9uLWJhY2stYnV0dG9uLFxyXG4gIGlvbi1tZW51LWJ1dHRvbiB7XHJcbiAgICAtLWNvbG9yOiB2YXIoLS1jcy10ZXh0LWFjY2VudCkgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kLXNpemUge1xyXG4gIGhlaWdodDogNDAwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAmOjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0tY3MtZ3JhZGllbnQtaW1hZ2UpO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gIH1cclxuXHJcbiAgLyogUGFyYWxsYXggVGl0bGVcclxuICAgICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG4gIC5wYXJhbGxheC10aXRsZS1zZWN0aW9uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogMTZweDtcclxuICAgIGxlZnQ6IDE2cHg7XHJcbiAgICByaWdodDogMTZweDtcclxuICB9XHJcbn1cclxuXHJcbi5pY29uLXZlcnRpY2FsIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLXNrZWxldG9uLXRleHQge1xyXG4gIGxpbmUtaGVpZ2h0OiAxM3B4O1xyXG4gIC8vLS1iYWNrZ3JvdW5kLXJnYjogMjMwLCA0MCwgNTA7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG4uY3VzdG9tLXNrZWxldG9uIGlvbi1za2VsZXRvbi10ZXh0Omxhc3QtY2hpbGQge1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/bill/bill.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/bill/bill.page.ts ***!
  \*****************************************/
/*! exports provided: BillPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BillPage", function() { return BillPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_customer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/customer.service */ "./src/app/services/customer.service.ts");





//import { NotifyService} from '../../notifications/notify.service';
let BillPage = class BillPage {
    constructor(router, alertCtrl, customerSer, 
    //private notify: NotifyService,
    activatedRoute) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.customerSer = customerSer;
        this.activatedRoute = activatedRoute;
        activatedRoute.params.subscribe(val => {
            this.vendor_id = localStorage.getItem("vendor_id");
            this.getCustomers(this.vendor_id);
        });
    }
    ngOnInit() { }
    getCustomers(id) {
        setTimeout(() => {
            this.customerSer.getItems(id)
                .subscribe((res) => {
                this.data = res;
                console.log("data", this.data);
            });
        }, 1000);
    }
    addCustomer() { this.router.navigate(['/customers/add']); }
    onItemClickFunc(item, ev) {
        console.log("item", item);
        console.log("event", ev);
    }
    customerDetail(id) {
        this.router.navigate(['/customers/' + id + '/detail']);
    }
    edit(id) {
        this.router.navigate(['/customers/' + id + '/edit']);
    }
    remove(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log("data", data);
            let alert = yield this.alertCtrl.create({
                header: 'Remove customer',
                subHeader: 'Do you want to remove this customer?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Remove',
                        handler: () => {
                            this.customerSer.deleteItem(data)
                                .subscribe((res) => {
                                //this.notify.success("Customer deleted successfully")
                                this.getCustomers(this.vendor_id);
                            }, (err) => {
                                // this.notify.error("Please try again")
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
BillPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _services_customer_service__WEBPACK_IMPORTED_MODULE_4__["CustomerService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
BillPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bill',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./bill.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bill/bill.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./bill.page.scss */ "./src/app/pages/bill/bill.page.scss")).default]
    })
], BillPage);



/***/ })

}]);
//# sourceMappingURL=pages-bill-bill-module.js.map