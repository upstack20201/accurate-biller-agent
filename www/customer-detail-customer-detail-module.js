(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-detail-customer-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customer-detail/customer-detail.page.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customer-detail/customer-detail.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/customers\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">{{name}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\">\r\n\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row>\r\n      <ion-col class=\"\">\r\n        <ion-label size=\"12\" class=\"ion-margin-top ion-padding avatar-info ion-text-center\r\n          border-radius \">\r\n\r\n          <ion-thumbnail class=\"border-radius\">\r\n            <img src=\"{{profilepic}}\" alt=\"\">\r\n          </ion-thumbnail>\r\n\r\n          <p class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n            {{name}}</p>\r\n\r\n        </ion-label>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"ion-padding\">\r\n      <ion-col size=\"12\" class=\"\">\r\n        <ion-item class=\"ion-no-padding\">\r\n          <ion-icon size=\"large\" name=\"person-circle-outline\" color=\"medium\"></ion-icon>\r\n\r\n          <ion-label class=\"ion-padding-start\">\r\n            <h4 class=\"text-size-sm text-color-primary font-bold \">\r\n              Profile Details\r\n            </h4>\r\n            <p>{{name}}, <ion-icon name=\"call\"></ion-icon>&nbsp;{{mobile}}</p>\r\n            <p>{{email}}, </p>\r\n            <p>{{address}}</p>\r\n          </ion-label>\r\n\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"ion-padding\">\r\n      <ion-col size=\"12\" class=\"\">\r\n        <ion-item class=\"ion-no-padding\">\r\n          <ion-icon size=\"large\" name=\"bicycle-outline\" color=\"medium\"></ion-icon>\r\n          <ion-label class=\"ion-padding-start\">\r\n            <p class=\"text-size-sm text-color-primary font-bold \">\r\n              Agent\r\n            </p>\r\n            <p>{{agent}}</p>\r\n          </ion-label>\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row class=\"ion-padding\">\r\n      <!-- <ion-col size=\"2\" class=\"ion-margin-top\">\r\n        <ion-icon size=\"large\" name=\"repeat-outline\"></ion-icon>\r\n      </ion-col> -->\r\n      <ion-col size=\"12\" *ngIf=\"subscriptions == ''\">\r\n        <p class=\"text-size-sm text-color-primary font-bold \">\r\n          No active subscriptions yet\r\n        </p>\r\n      </ion-col>\r\n      <ion-col size=\"12\" *ngIf=\"subscriptions != ''\">\r\n        <ion-item class=\"ion-no-padding\" lines=\"none\">\r\n          <ion-icon size=\" large\" name=\"refresh-outline\" color=\"medium\">\r\n          </ion-icon>\r\n          <ion-label class=\"ion-padding-start\">\r\n            <ion-text class=\"text-size-sm text-color-primary font-bold\">Active subscription</ion-text>\r\n          </ion-label>\r\n        </ion-item>\r\n\r\n        <ion-list class=\"ion-margin-bottom\" *ngFor=\"let item of subscriptions\">\r\n          <ion-item class=\"\" lines=\"none\">\r\n            <!--Content Avatar-->\r\n            <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n              <img [src]=\"item.product_img\">\r\n            </ion-thumbnail>\r\n            <ion-label class=\"ion-padding-start\" >\r\n              <!-- Ttile and Subtitle -->\r\n              <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                {{item.title}} (1 {{item.unit}})</h6>\r\n              <h6 style=\"display: flex;\" class=\"text-size-sd text-color-primary ion-text-wrap\" color=\"medium\">\r\n                {{item.quantity}} Pkt\r\n\r\n              </h6>\r\n              <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                Rs. {{item.rate}}</h6>\r\n              <label class=\"day-align\" *ngIf=\"item.onetime == 'false'\">\r\n                <p *ngFor=\"let c of checkbox\" [style]=\"item.weekday.includes(c.name) ? textColor : ''\">{{c.title}}</p>\r\n              </label>\r\n              <p *ngIf=\"item.onetime == 'true'\">on {{item.delivery_date}}</p>\r\n            </ion-label>\r\n            <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-end\">\r\n              <ion-item lines=\"none\">\r\n                <ion-icon size=\"small\" *ngIf=\"item.onetime == 'false'\"\r\n                  (click)=\"editSchedule(customer_id,item.product_id)\" class=\"icon-pencil\" color=\"medium\">\r\n                </ion-icon>\r\n                <ion-icon size=\"small\" (click)=\"remove(customer_id, item.product_id)\" name=\"trash\" color=\"medium\">\r\n                </ion-icon>\r\n              </ion-item>\r\n            </ion-buttons>\r\n          </ion-item>\r\n        </ion-list>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-toolbar>\r\n\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \"\r\n      (click)=\"subscribe(customer_id)\" expand=\"block\">\r\n      Subscribe\r\n    </ion-button>\r\n  </ion-toolbar>\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-detail-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-detail-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: CustomerDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDetailPageRoutingModule", function() { return CustomerDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _customer_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-detail.page */ "./src/app/pages/customers/customer-detail/customer-detail.page.ts");




const routes = [
    {
        path: '',
        component: _customer_detail_page__WEBPACK_IMPORTED_MODULE_3__["CustomerDetailPage"]
    },
    {
        path: ':product_id/customer-orders',
        loadChildren: () => __webpack_require__.e(/*! import() | customer-orders-customer-orders-module */ "customer-orders-customer-orders-module").then(__webpack_require__.bind(null, /*! ./customer-orders/customer-orders.module */ "./src/app/pages/customers/customer-detail/customer-orders/customer-orders.module.ts")).then(m => m.CustomerOrdersPageModule)
    }
];
let CustomerDetailPageRoutingModule = class CustomerDetailPageRoutingModule {
};
CustomerDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CustomerDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-detail.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-detail.module.ts ***!
  \***************************************************************************/
/*! exports provided: CustomerDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDetailPageModule", function() { return CustomerDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _customer_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customer-detail-routing.module */ "./src/app/pages/customers/customer-detail/customer-detail-routing.module.ts");
/* harmony import */ var _customer_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customer-detail.page */ "./src/app/pages/customers/customer-detail/customer-detail.page.ts");







let CustomerDetailPageModule = class CustomerDetailPageModule {
};
CustomerDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _customer_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["CustomerDetailPageRoutingModule"]
        ],
        declarations: [_customer_detail_page__WEBPACK_IMPORTED_MODULE_6__["CustomerDetailPage"]]
    })
], CustomerDetailPageModule);



/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-detail.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-detail.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".active ion-toolbar {\n  background: var(--cs-background-primary) !important;\n  color: var(--cs-text-accent) !important;\n}\n.active ion-back-button,\n.active ion-menu-button {\n  --color: var(--cs-text-accent) !important;\n}\nion-thumbnail {\n  display: block !important;\n  margin-left: auto !important;\n  margin-right: auto !important;\n}\n.header-image {\n  position: relative;\n  z-index: 999;\n}\n.background-size {\n  height: 140px;\n  position: relative;\n}\n.background-size::after {\n  content: \"\";\n  background: var(--cs-gradient-image);\n  position: absolute;\n  height: 100%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.info-block ion-thumbnail {\n  height: 100px;\n  width: 100px;\n  opacity: 1 !important;\n  z-index: 999;\n  position: relative;\n}\nion-grid {\n  position: relative;\n  z-index: 1;\n}\n.page1 .background-size {\n  height: 250px;\n}\n.page1 .background-size ion-card-content {\n  position: absolute;\n  bottom: 0;\n}\nion-list {\n  border: 1px solid #cccccc;\n  background: var(--cs-background-secondary) !important;\n  border-radius: 8px;\n}\n.icon-vertical {\n  display: inline-block !important;\n}\n.day-align {\n  display: flex;\n  letter-spacing: 4px;\n  font-family: sans-serif;\n  font-size: smaller;\n  color: #c3cac8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3VzdG9tZXJzL2N1c3RvbWVyLWRldGFpbC9jdXN0b21lci1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsbURBQUE7RUFDQSx1Q0FBQTtBQUFKO0FBR0U7O0VBRUUseUNBQUE7QUFESjtBQUtBO0VBQ0UseUJBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FBRkY7QUFLQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtBQUZGO0FBS0E7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7QUFGRjtBQUlFO0VBQ0UsV0FBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FBRko7QUFPRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFFQSxZQUFBO0VBQ0Esa0JBQUE7QUFMSjtBQVNBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0FBTkY7QUFVRTtFQUNFLGFBQUE7QUFQSjtBQVNJO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0FBUE47QUFZQTtFQUNFLHlCQUFBO0VBQ0EscURBQUE7RUFDQSxrQkFBQTtBQVRGO0FBWUE7RUFDRSxnQ0FBQTtBQVRGO0FBWUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQVRGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY3VzdG9tZXJzL2N1c3RvbWVyLWRldGFpbC9jdXN0b21lci1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjdGl2ZSB7XHJcbiAgaW9uLXRvb2xiYXIge1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHZhcigtLWNzLXRleHQtYWNjZW50KSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgaW9uLWJhY2stYnV0dG9uLFxyXG4gIGlvbi1tZW51LWJ1dHRvbiB7XHJcbiAgICAtLWNvbG9yOiB2YXIoLS1jcy10ZXh0LWFjY2VudCkgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuXHJcbmlvbi10aHVtYm5haWwge1xyXG4gIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG8gIWltcG9ydGFudDtcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxufVxyXG5cclxuLmhlYWRlci1pbWFnZSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHotaW5kZXg6IDk5OTtcclxufVxyXG5cclxuLmJhY2tncm91bmQtc2l6ZSB7XHJcbiAgaGVpZ2h0OiAxNDBweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICY6OmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1jcy1ncmFkaWVudC1pbWFnZSk7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4uaW5mby1ibG9jayB7XHJcbiAgaW9uLXRodW1ibmFpbCB7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xyXG4gICAgLy9tYXJnaW46IC03MHB4IGF1dG8gMDtcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogMTtcclxufVxyXG5cclxuLnBhZ2UxIHtcclxuICAuYmFja2dyb3VuZC1zaXplIHtcclxuICAgIGhlaWdodDogMjUwcHg7XHJcblxyXG4gICAgaW9uLWNhcmQtY29udGVudCB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuaW9uLWxpc3Qge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2NjY2M7XHJcbiAgYmFja2dyb3VuZDogdmFyKC0tY3MtYmFja2dyb3VuZC1zZWNvbmRhcnkpICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG59XHJcblxyXG4uaWNvbi12ZXJ0aWNhbCB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5kYXktYWxpZ24ge1xyXG4gIGRpc3BsYXk6IGZsZXg7IFxyXG4gIGxldHRlci1zcGFjaW5nOiAgNHB4OyBcclxuICBmb250LWZhbWlseTogc2Fucy1zZXJpZjtcclxuICBmb250LXNpemU6c21hbGxlcjtcclxuICBjb2xvcjogIHJnYigxOTUgMjAyIDIwMCk7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/customers/customer-detail/customer-detail.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/customers/customer-detail/customer-detail.page.ts ***!
  \*************************************************************************/
/*! exports provided: CustomerDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDetailPage", function() { return CustomerDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_customer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/customer.service */ "./src/app/services/customer.service.ts");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");
/* harmony import */ var _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/schedule.service */ "./src/app/services/schedule.service.ts");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");









let CustomerDetailPage = class CustomerDetailPage {
    constructor(activatedRoute, router, customerSer, deliveryAgentSer, scheduleSer, productSer, alertCtrl, notify) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.customerSer = customerSer;
        this.deliveryAgentSer = deliveryAgentSer;
        this.scheduleSer = scheduleSer;
        this.productSer = productSer;
        this.alertCtrl = alertCtrl;
        this.notify = notify;
        this.subscriptions = [];
        this.weekdays = [];
        this.textColor = "color: #1ae5be";
        this.checkbox = [
            { val: 'sun', name: 'Sunday', title: 'S', color: '' },
            { val: 'mon', name: 'Monday', title: 'M', color: '' },
            { val: 'tues', name: 'Tuesday', title: 'T', color: '' },
            { val: 'wed', name: 'Wednesday', title: 'W', color: '' },
            { val: 'thu', name: 'Thursday', title: 'T', color: '' },
            { val: 'fri', name: 'Friday', title: 'F', color: '' },
            { val: 'sat', name: 'Saturday', title: 'S', color: '' }
        ];
        this.activatedRoute.params.subscribe((params) => {
            this.customer_id = params['id'];
        });
        this.vendor_id = localStorage.getItem("vendor_id");
        activatedRoute.params.subscribe(val => {
            this.getCustomerSchedule(this.customer_id);
        });
    }
    ngOnInit() {
        this.getCustomer(this.customer_id);
        this.getAgent(this.vendor_id);
    }
    getCustomer(id) {
        this.customerSer.getItem(id)
            .subscribe((res) => {
            var customer = res['customer'];
            this.name = customer['name'];
            this.address = customer['address'];
            this.zipcode = customer['zipcode'];
            this.mobile = customer['mobile'];
            this.profilepic = customer['profilepic'];
            this.email = customer['email'];
            this.agent_id = customer['agent_id'];
        });
    }
    getAgent(id) {
        this.deliveryAgentSer.getItems(id)
            .subscribe((res) => {
            var data = res['agents'];
            for (let i = 0; i < data.length; i++) {
                var value = data[i].id;
                if (value == this.agent_id) {
                    this.agent = data[i].name;
                }
            }
        });
    }
    getCustomerSchedule(cust_id) {
        this.subscriptions = [];
        this.scheduleSer.getItem(cust_id)
            .subscribe((res) => {
            for (let item of res['customerSchedulers']) {
                this.productSer.getItem(item.product_id)
                    .subscribe((res) => {
                    this.weekdays = [];
                    for (let schedule of item.productschedules) {
                        this.weekdays.push(schedule.weekday);
                        var quantity = schedule.quantity;
                    }
                    let schedule_detail = {
                        weekday: this.weekdays,
                        title: res['product'].title,
                        product_img: res['product'].product_img,
                        rate: res['product'].rate,
                        unit: res['product'].unit,
                        schedule_id: item.id,
                        product_id: res['product'].id,
                        quantity: quantity,
                        onetime: item.onetime,
                        delivery_date: item.start_date
                    };
                    this.subscriptions.push(schedule_detail);
                });
            } //console.log("this.subscription", this.subscriptions)
        });
    }
    editSchedule(cust_id, product_id) {
        this.router.navigate(['orders/' + cust_id + '/' + product_id]);
    }
    customerOrderDetail(cust_id, product_id) {
        this.router.navigate(['customers/' + cust_id + '/detail/' + product_id + '/customer-orders']);
    }
    remove(cust_id, product_id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Remove subscription',
                subHeader: 'Do you want to remove the product?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            // console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Remove',
                        handler: () => {
                            let details = {
                                cust_id: cust_id,
                                product_id: product_id,
                                vendor_id: this.vendor_id
                            };
                            //console.log("details", details)
                            this.scheduleSer.deleteItem(details)
                                .subscribe((res) => {
                                this.notify.success("Subscription deleted successfully");
                                this.getCustomerSchedule(cust_id);
                            }, (err) => {
                                this.notify.error("Please try again");
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    subscribe(cust_id) {
        this.router.navigate(['/orders/' + cust_id]);
    }
};
CustomerDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_customer_service__WEBPACK_IMPORTED_MODULE_3__["CustomerService"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__["DeliveryAgentService"] },
    { type: _services_schedule_service__WEBPACK_IMPORTED_MODULE_5__["ScheduleService"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_6__["ProductService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_8__["NotifyService"] }
];
CustomerDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./customer-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customer-detail/customer-detail.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_8__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./customer-detail.page.scss */ "./src/app/pages/customers/customer-detail/customer-detail.page.scss")).default]
    })
], CustomerDetailPage);



/***/ })

}]);
//# sourceMappingURL=customer-detail-customer-detail-module.js.map