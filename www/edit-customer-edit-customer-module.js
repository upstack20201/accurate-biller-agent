(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-customer-edit-customer-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/edit-customer/edit-customer.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/edit-customer/edit-customer.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/customers\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">{{customer_name}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<!-- FORMS --COMPONENT 3 -->\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n    <ion-row class=\"ion-padding-top ion-padding-bottom\">\r\n      <ion-col (change)=\"readUrl($event)\" class=\"ion-align-self-start ion-no-padding ion-margin-bottom\">\r\n        <input id=\"fileInput\" name=\"file\" type=\"file\" accept=\"image/*\" />\r\n          <ion-label for=\"fileInput\" size=\"12\" class=\"text-size-sm ion-text-capitalize ion-text-center\">\r\n            <img class=\"ion-margin-bottom\" [src]=\"customer_img\" height=\"70\">\r\n            <p>Edit Photo</p> \r\n          </ion-label>\r\n      </ion-col>\r\n      <!-- Full Name -->\r\n      <ion-col size=\"12\" class=\"ion-align-self-start ion-no-padding\">\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">First Name\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n              [placeholder]=\"\" formControlName=\"firstName\"></ion-input>\r\n          </ion-item>\r\n          <!-- Last Name -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Last Name\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n              [placeholder]=\"\" formControlName=\"lastName\"></ion-input>\r\n          </ion-item>\r\n          <!-- Email -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Email\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"email\" [placeholder]=\"\"\r\n              formControlName=\"email\"></ion-input>\r\n          </ion-item>\r\n          <!-- Mobile -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Contact\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" maxlength=\"10\"\r\n              [placeholder]=\"\" formControlName=\"mobile\"></ion-input>\r\n          </ion-item>\r\n          <!-- Address Line 1 -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Address\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"\r\n              formControlName=\"address\"></ion-input>\r\n          </ion-item>\r\n          <!-- Address Line 2 -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap ion-text-wrap ion-margin-start\"\r\n              position=\"fixed\">Delivery Agent</ion-label>\r\n            <ion-select formControlName=\"delivery_agent\" [interfaceOptions]=\"customActionSheetOptions\"\r\n              interface=\"action-sheet\"\r\n              class=\"text-size-sm text-color-primary ion-text-wrap ion-padding-start ion-margin-start popvert\">\r\n              <ion-select-option *ngFor=\"let item of delivery_agent\" [value]=\"item.id\">{{item.name}}</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n          <!-- Zip Code -->\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Zipcode\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" maxlength=\"6\"\r\n              [placeholder]=\"\" formControlName=\"zipcode\"></ion-input>\r\n          </ion-item>\r\n           <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n            [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n            Update\r\n          </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </form>\r\n  </ion-grid>\r\n</ion-content>\r\n<!-- <ion-footer>\r\n  <ion-toolbar>\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \"\r\n      [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n      Update\r\n    </ion-button>\r\n\r\n  </ion-toolbar>\r\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/pages/customers/edit-customer/edit-customer-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/customers/edit-customer/edit-customer-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: EditCustomerPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCustomerPageRoutingModule", function() { return EditCustomerPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _edit_customer_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-customer.page */ "./src/app/pages/customers/edit-customer/edit-customer.page.ts");




const routes = [
    {
        path: '',
        component: _edit_customer_page__WEBPACK_IMPORTED_MODULE_3__["EditCustomerPage"]
    }
];
let EditCustomerPageRoutingModule = class EditCustomerPageRoutingModule {
};
EditCustomerPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditCustomerPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/customers/edit-customer/edit-customer.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/customers/edit-customer/edit-customer.module.ts ***!
  \***********************************************************************/
/*! exports provided: EditCustomerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCustomerPageModule", function() { return EditCustomerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_customer_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-customer-routing.module */ "./src/app/pages/customers/edit-customer/edit-customer-routing.module.ts");
/* harmony import */ var _edit_customer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-customer.page */ "./src/app/pages/customers/edit-customer/edit-customer.page.ts");








let EditCustomerPageModule = class EditCustomerPageModule {
};
EditCustomerPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _edit_customer_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditCustomerPageRoutingModule"]
        ],
        declarations: [_edit_customer_page__WEBPACK_IMPORTED_MODULE_6__["EditCustomerPage"]]
    })
], EditCustomerPageModule);



/***/ }),

/***/ "./src/app/pages/customers/edit-customer/edit-customer.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/customers/edit-customer/edit-customer.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".capitalize {\n  text-transform: capitalize;\n}\n\n#fileInput {\n  position: absolute;\n  opacity: 0;\n  height: 93px !important;\n}\n\nimg {\n  display: block !important;\n  margin-left: auto !important;\n  margin-right: auto !important;\n  border-radius: 20%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3VzdG9tZXJzL2VkaXQtY3VzdG9tZXIvZWRpdC1jdXN0b21lci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwwQkFBQTtBQUNKOztBQUNFO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsdUJBQUE7QUFFSjs7QUFDRTtFQUNBLHlCQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLGtCQUFBO0FBRUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jdXN0b21lcnMvZWRpdC1jdXN0b21lci9lZGl0LWN1c3RvbWVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXBpdGFsaXplIHtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIH1cclxuICAjZmlsZUlucHV0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICBoZWlnaHQ6IDkzcHggIWltcG9ydGFudFxyXG4gIH1cclxuXHJcbiAgaW1nIHtcclxuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMjAlO1xyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/pages/customers/edit-customer/edit-customer.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/customers/edit-customer/edit-customer.page.ts ***!
  \*********************************************************************/
/*! exports provided: EditCustomerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCustomerPage", function() { return EditCustomerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_customer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/customer.service */ "./src/app/services/customer.service.ts");
/* harmony import */ var _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/delivery-agent.service */ "./src/app/services/delivery-agent.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







let EditCustomerPage = class EditCustomerPage {
    constructor(fb, customerSer, deliveryAgentSer, notify, router, activatedRoute) {
        this.fb = fb;
        this.customerSer = customerSer;
        this.deliveryAgentSer = deliveryAgentSer;
        this.notify = notify;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.vendor_id = localStorage.getItem("vendor_id");
        this.getAgents(this.vendor_id);
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            this.id = params['id'];
        });
        this.getCustomer(this.id);
        this.formBuilder();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            file: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z]+$")]),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[a-zA-Z]+$")]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$"),]),
            mobile: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]{10}")]),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^[0-9]{6}")]),
            delivery_agent: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    }
    getAgents(id) {
        this.deliveryAgentSer.getItems(id)
            .subscribe((res) => {
            console.log("agents", res['agents']);
            this.delivery_agent = res['agents'];
        });
    }
    getCustomer(id) {
        this.customerSer.getItem(id)
            .subscribe((res) => {
            let customer = res['customer'];
            this.customer_name = customer.name;
            customer.name = customer.name.split(" ");
            this.customer_img = customer.profilepic;
            this.frmFG.get("file").setValue(customer.profilepic);
            this.frmFG.get("firstName").setValue(customer.name[0]);
            this.frmFG.get("lastName").setValue(customer.name[1]);
            this.frmFG.get("email").setValue(customer.email);
            this.frmFG.get("mobile").setValue(customer.mobile);
            this.frmFG.get("address").setValue(customer.address);
            this.frmFG.get("zipcode").setValue(customer.zipcode);
            this.frmFG.get("delivery_agent").setValue(customer.agent_id);
        });
    }
    submit() {
        var data = this.frmFG.value;
        console.log("data", data);
        data.name = this.capitalization(data.firstName) + ' ' + this.capitalization(data.lastName);
        data.file = this.url1;
        this.customerSer.updateItem(this.id, data)
            .subscribe((res) => {
            this.notify.success('Edit customer successfully');
            this.router.navigate(['/customers']);
        }, (err) => {
            console.log("error", err);
        });
    }
    capitalization(input) {
        return (!!input) ? input.split(' ').map(function (wrd) { return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase(); }).join(' ') : '';
    }
    readUrl(event) {
        console.log('readUrl', event.target.files[0]);
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onload = (event) => {
                this.url1 = event.target.result;
                this.customer_img = this.url1;
            };
        }
    }
};
EditCustomerPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_customer_service__WEBPACK_IMPORTED_MODULE_3__["CustomerService"] },
    { type: _services_delivery_agent_service__WEBPACK_IMPORTED_MODULE_4__["DeliveryAgentService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
];
EditCustomerPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-customer',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-customer.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/edit-customer/edit-customer.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-customer.page.scss */ "./src/app/pages/customers/edit-customer/edit-customer.page.scss")).default]
    })
], EditCustomerPage);



/***/ })

}]);
//# sourceMappingURL=edit-customer-edit-customer-module.js.map