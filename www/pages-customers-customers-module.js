(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-customers-customers-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customers.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customers.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/product\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Customers</ion-title>\r\n    <ion-buttons class=\"ion-margin-end\" slot=\"end\">\r\n      <ion-icon class=\"icon-font-md\" (click)=\"addCustomer()\" name=\"person-add\"></ion-icon>\r\n      <!-- <ion-icon name=\"add-circle-outline\" size=\"large\" (click)=\"addCustomer()\"></ion-icon> -->\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<!--- Theme Parallax Frends -->\r\n<ion-content scrollEvents=\"true\" parallax-header>\r\n  <!--Content-->\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list class=\"ion-no-margin\" *ngIf=\"data && data.customers; else skeleton\">\r\n          <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of data.customers\">\r\n            <!--Content Avatar-->\r\n            <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n              <img src=\"{{item.profilepic}}\">\r\n            </ion-thumbnail>\r\n            <ion-label class=\"ion-padding-start\" (click)=\"customerDetail(item.id)\">\r\n              <!-- Ttile and Subtitle -->\r\n              <h3 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                {{item.name}}</h3>\r\n              <p class=\"text-size-sm font-regular ion-text-wrap\">\r\n                {{item.address}}</p>\r\n            </ion-label>\r\n            <!-- <ion-button class=\"button-icon ion-margin-end\" slot=\"end\">\r\n                <ion-icon class=\"icon-font-md\" name=\"create\"></ion-icon>\r\n                <ion-icon class=\"icon-font-md\" name=\"create\"></ion-icon>\r\n              </ion-button> -->\r\n            <ion-label class=\"ion-text-end no-margin-right\" slot=\"end\">\r\n              <ion-icon class=\"icon icon-font-sm font-bold icon-pencil\" color=\"medium\" (click)=\"edit(item.id)\">\r\n              </ion-icon>&nbsp;\r\n              <ion-icon class=\"icon icon-font-sm font-bold ion-margin-end\" color=\"medium\" (click)=\"remove(item)\"\r\n                name=\"trash\">\r\n              </ion-icon>\r\n            </ion-label>\r\n            <!--  <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-right\">\r\n              <ion-item lines=\"none\">\r\n                <ion-icon size=\"small\" (click)=\"edit(item.id)\" class=\"icon-pencil\" color=\"medium\"></ion-icon>\r\n\r\n                <ion-icon size=\"small\" (click)=\"remove(item)\" name=\"trash\" color=\"medium\"></ion-icon>\r\n              </ion-item>\r\n             \r\n            </ion-buttons> -->\r\n          </ion-item>\r\n        </ion-list>\r\n\r\n        <ng-template #skeleton>\r\n          <ion-list class=\"ion-no-margin\" className=\"fadeInLeft\">\r\n            <ion-item class=\"default-item ion-no-padding box-shadow\" lines=\"none\" *ngFor=\"let item of [1, 2, 3, 4, 5]\">\r\n              <ion-thumbnail class=\"border-radius ion-padding-start\" slot=\"start\">\r\n                <ion-avatar>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-avatar>\r\n              </ion-thumbnail>\r\n              <ion-label class=\"ion-padding\">\r\n                <h2>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </h2>\r\n                <p>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </p>\r\n              </ion-label>\r\n              <ion-buttons slot=\"end\" class=\"icon-vertical ion-margin-end\">\r\n                <ion-item>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-item>\r\n                <ion-item>\r\n                  <ion-skeleton-text animated></ion-skeleton-text>\r\n                </ion-item>\r\n\r\n              </ion-buttons>\r\n            </ion-item>\r\n          </ion-list>\r\n        </ng-template>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/customers/customers-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/customers/customers-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: CustomersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPageRoutingModule", function() { return CustomersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _customers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customers.page */ "./src/app/pages/customers/customers.page.ts");




const routes = [
    {
        path: '',
        component: _customers_page__WEBPACK_IMPORTED_MODULE_3__["CustomersPage"]
    },
    {
        path: 'add',
        loadChildren: () => Promise.all(/*! import() | add-customer-add-customer-module */[__webpack_require__.e("common"), __webpack_require__.e("add-customer-add-customer-module")]).then(__webpack_require__.bind(null, /*! ./add-customer/add-customer.module */ "./src/app/pages/customers/add-customer/add-customer.module.ts")).then(m => m.AddCustomerPageModule)
    },
    {
        path: ':id/detail',
        loadChildren: () => Promise.all(/*! import() | customer-detail-customer-detail-module */[__webpack_require__.e("common"), __webpack_require__.e("customer-detail-customer-detail-module")]).then(__webpack_require__.bind(null, /*! ./customer-detail/customer-detail.module */ "./src/app/pages/customers/customer-detail/customer-detail.module.ts")).then(m => m.CustomerDetailPageModule)
    },
    {
        path: ':id/edit',
        loadChildren: () => Promise.all(/*! import() | edit-customer-edit-customer-module */[__webpack_require__.e("common"), __webpack_require__.e("edit-customer-edit-customer-module")]).then(__webpack_require__.bind(null, /*! ./edit-customer/edit-customer.module */ "./src/app/pages/customers/edit-customer/edit-customer.module.ts")).then(m => m.EditCustomerPageModule)
    }
];
let CustomersPageRoutingModule = class CustomersPageRoutingModule {
};
CustomersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CustomersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/customers/customers.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/customers/customers.module.ts ***!
  \*****************************************************/
/*! exports provided: CustomersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPageModule", function() { return CustomersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _customers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customers-routing.module */ "./src/app/pages/customers/customers-routing.module.ts");
/* harmony import */ var _customers_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customers.page */ "./src/app/pages/customers/customers.page.ts");







let CustomersPageModule = class CustomersPageModule {
};
CustomersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _customers_routing_module__WEBPACK_IMPORTED_MODULE_5__["CustomersPageRoutingModule"]
        ],
        declarations: [_customers_page__WEBPACK_IMPORTED_MODULE_6__["CustomersPage"]]
    })
], CustomersPageModule);



/***/ }),

/***/ "./src/app/pages/customers/customers.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/customers/customers.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".active ion-toolbar {\n  background: var(--cs-background-primary) !important;\n  color: var(--cs-text-accent) !important;\n}\n.active ion-back-button,\n.active ion-menu-button {\n  --color: var(--cs-text-accent) !important;\n}\n.background-size {\n  height: 400px;\n  position: relative;\n  /* Parallax Title\n     =============================================*/\n}\n.background-size::after {\n  content: \"\";\n  background: var(--cs-gradient-image);\n  position: absolute;\n  height: 100%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.background-size .parallax-title-section {\n  position: absolute;\n  bottom: 16px;\n  left: 16px;\n  right: 16px;\n}\n.icon-vertical {\n  display: inline-block !important;\n}\nion-skeleton-text {\n  line-height: 13px;\n  --border-radius: 20px;\n}\n.custom-skeleton ion-skeleton-text:last-child {\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY3VzdG9tZXJzL2N1c3RvbWVycy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxtREFBQTtFQUNBLHVDQUFBO0FBQUo7QUFHRTs7RUFFRSx5Q0FBQTtBQURKO0FBS0E7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFZQTttREFBQTtBQVpGO0FBRUU7RUFDRSxXQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7QUFBSjtBQUtFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFISjtBQU9BO0VBQ0UsZ0NBQUE7QUFKRjtBQU9BO0VBQ0UsaUJBQUE7RUFFQSxxQkFBQTtBQUxGO0FBUUE7RUFDRSxrQkFBQTtBQUxGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY3VzdG9tZXJzL2N1c3RvbWVycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWN0aXZlIHtcclxuICBpb24tdG9vbGJhciB7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1jcy1iYWNrZ3JvdW5kLXByaW1hcnkpICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogdmFyKC0tY3MtdGV4dC1hY2NlbnQpICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICBpb24tYmFjay1idXR0b24sXHJcbiAgaW9uLW1lbnUtYnV0dG9uIHtcclxuICAgIC0tY29sb3I6IHZhcigtLWNzLXRleHQtYWNjZW50KSAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG5cclxuLmJhY2tncm91bmQtc2l6ZSB7XHJcbiAgaGVpZ2h0OiA0MDBweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICY6OmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1jcy1ncmFkaWVudC1pbWFnZSk7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgfVxyXG5cclxuICAvKiBQYXJhbGxheCBUaXRsZVxyXG4gICAgID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXHJcbiAgLnBhcmFsbGF4LXRpdGxlLXNlY3Rpb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAxNnB4O1xyXG4gICAgbGVmdDogMTZweDtcclxuICAgIHJpZ2h0OiAxNnB4O1xyXG4gIH1cclxufVxyXG5cclxuLmljb24tdmVydGljYWwge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24tc2tlbGV0b24tdGV4dCB7XHJcbiAgbGluZS1oZWlnaHQ6IDEzcHg7XHJcbiAgLy8tLWJhY2tncm91bmQtcmdiOiAyMzAsIDQwLCA1MDtcclxuICAtLWJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbi5jdXN0b20tc2tlbGV0b24gaW9uLXNrZWxldG9uLXRleHQ6bGFzdC1jaGlsZCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/customers/customers.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/customers/customers.page.ts ***!
  \***************************************************/
/*! exports provided: CustomersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPage", function() { return CustomersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_customer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/customer.service */ "./src/app/services/customer.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");






let CustomersPage = class CustomersPage {
    constructor(router, alertCtrl, customerSer, notify, activatedRoute) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.customerSer = customerSer;
        this.notify = notify;
        this.activatedRoute = activatedRoute;
        activatedRoute.params.subscribe(val => {
            this.vendor_id = localStorage.getItem("vendor_id");
            this.getCustomers(this.vendor_id);
        });
    }
    ngOnInit() { }
    getCustomers(id) {
        setTimeout(() => {
            this.customerSer.getItems(id)
                .subscribe((res) => {
                this.data = res;
                console.log("data", this.data);
            });
        }, 1000);
    }
    addCustomer() { this.router.navigate(['/customers/add']); }
    onItemClickFunc(item, ev) {
        console.log("item", item);
        console.log("event", ev);
    }
    customerDetail(id) {
        this.router.navigate(['/customers/' + id + '/detail']);
    }
    edit(id) {
        this.router.navigate(['/customers/' + id + '/edit']);
    }
    remove(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log("data", data);
            let alert = yield this.alertCtrl.create({
                header: 'Remove customer',
                subHeader: 'Do you want to remove this customer?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Remove',
                        handler: () => {
                            this.customerSer.deleteItem(data)
                                .subscribe((res) => {
                                this.notify.success("Customer deleted successfully");
                                this.getCustomers(this.vendor_id);
                            }, (err) => {
                                this.notify.error("Please try again");
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
CustomersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _services_customer_service__WEBPACK_IMPORTED_MODULE_4__["CustomerService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
CustomersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customers',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./customers.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/customers/customers.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_5__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./customers.page.scss */ "./src/app/pages/customers/customers.page.scss")).default]
    })
], CustomersPage);



/***/ })

}]);
//# sourceMappingURL=pages-customers-customers-module.js.map