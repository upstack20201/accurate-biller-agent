(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-orders-orders-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/orders.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/orders.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <!-- Header -->\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button defaultHref=\"/customers\"></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>Products</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <!-- Content -->\r\n  <ion-content>\r\n    <!-- ANIMATION--COMPONENT 2 -->\r\n    <ion-grid class=\"ion-no-padding\" *ngIf=\"data && data.products\">\r\n      <ion-row class=\"ion-padding\">\r\n        <ion-col size=\"12\">\r\n\r\n          <ion-list class=\"ion-margin-bottom\" *ngFor=\"let item of data.products\">\r\n            <ion-item class=\"\" lines=\"none\">\r\n              <ion-col size=\"4\">\r\n                <ion-label class=\"\">\r\n                  <ion-thumbnail class=\"border-radius\" slot=\"start\">\r\n                    <img [src]=\"item.product_img\">\r\n                  </ion-thumbnail>\r\n                </ion-label>\r\n              </ion-col>\r\n              <ion-col size=\"8\">\r\n                <ion-label class=\"\">\r\n                  <!-- Ttile and Subtitle -->\r\n                  <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                    {{item.title}} (1 {{item.unit}})</h6>\r\n                  <h6 class=\"text-size-sd text-color-primary ion-text-wrap\" color=\"medium\">\r\n                    {{item.quantity}} Pkt</h6>\r\n                  <h6 class=\"text-size-sm text-color-primary font-bold ion-text-wrap\">\r\n                    Rs. {{item.rate}}</h6>\r\n\r\n                  <ion-buttons class=\"\">\r\n                    <ion-text *ngIf=\"!item.subscribed\">\r\n                      <ion-chip outline (click)=\"subscribeProduct(item.id, true)\" class=\"schedule_btn\" size=\"small\"\r\n                        color=\"default\">\r\n                        Subscribe</ion-chip>\r\n                    </ion-text>\r\n                    <ion-text>\r\n                      <p class=\"text-size-sd font-bold ion-text-wrap\"  *ngIf=\"item.subscribed && item.onetime == 'false'\">\r\n                        Subscription</p>\r\n                      <p class=\"text-size-sd font-bold ion-text-wrap\" *ngIf=\"item.subscribed && item.onetime == 'true'\">\r\n                        Delivered on {{item.delivery_date}}</p>\r\n                    </ion-text>\r\n                    <ion-text *ngIf=\"item.onetime == 'false' || !item.subscribed\">\r\n                      <ion-chip outline (click)=\"subscribeProduct(item.id, false)\" class=\"schedule_btn\" size=\"small\"\r\n                        color=\"default\">\r\n                        Buy Once</ion-chip>\r\n                    </ion-text>\r\n\r\n                  </ion-buttons>\r\n\r\n                  <!--  <ion-text *ngIf=\"subscriptions != ''\">\r\n                    <ion-button class=\"ion-text-capitalize btn-color border-radius\" (click)=\"subscribeProduct(item.id)\"\r\n                      fill=\"outline\">\r\n                      Buy Once\r\n                    </ion-button>\r\n                  </ion-text>&nbsp;\r\n                  <ion-text>\r\n                    <ion-button (click)=\"subscribeProduct(item.id)\" class=\"ion-text-capitalize btn-color border-radius\"\r\n                      fill=\"outline\">\r\n                      Subscribe\r\n                    </ion-button>\r\n                  </ion-text>\r\n                  <ion-text *ngIf=\"subscriptions == ''\">\r\n                    <p class=\"text-size-sd font-bold ion-text-wrap\">\r\n                      Subscription</p>\r\n                  </ion-text>\r\n -->\r\n\r\n                </ion-label>\r\n              </ion-col>\r\n            </ion-item>\r\n          </ion-list>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </ion-content>\r\n</ion-app>");

/***/ }),

/***/ "./src/app/pages/orders/orders-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/orders/orders-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: OrdersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageRoutingModule", function() { return OrdersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./orders.page */ "./src/app/pages/orders/orders.page.ts");




const routes = [
    {
        path: ':cust_id',
        component: _orders_page__WEBPACK_IMPORTED_MODULE_3__["OrdersPage"]
    },
    {
        path: ':cust_id/:product_id/:subscription',
        loadChildren: () => Promise.all(/*! import() | add-order-detail-add-order-detail-module */[__webpack_require__.e("common"), __webpack_require__.e("add-order-detail-add-order-detail-module")]).then(__webpack_require__.bind(null, /*! ./add-order-detail/add-order-detail.module */ "./src/app/pages/orders/add-order-detail/add-order-detail.module.ts")).then(m => m.AddOrderDetailPageModule)
    },
    {
        path: ':cust_id/:product_id',
        loadChildren: () => Promise.all(/*! import() | edit-order-detail-edit-order-detail-module */[__webpack_require__.e("common"), __webpack_require__.e("edit-order-detail-edit-order-detail-module")]).then(__webpack_require__.bind(null, /*! ./edit-order-detail/edit-order-detail.module */ "./src/app/pages/orders/edit-order-detail/edit-order-detail.module.ts")).then(m => m.EditOrderDetailPageModule)
    }
];
let OrdersPageRoutingModule = class OrdersPageRoutingModule {
};
OrdersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrdersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/orders/orders.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/orders/orders.module.ts ***!
  \***********************************************/
/*! exports provided: OrdersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageModule", function() { return OrdersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _orders_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./orders-routing.module */ "./src/app/pages/orders/orders-routing.module.ts");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orders.page */ "./src/app/pages/orders/orders.page.ts");







let OrdersPageModule = class OrdersPageModule {
};
OrdersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _orders_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrdersPageRoutingModule"]
        ],
        declarations: [_orders_page__WEBPACK_IMPORTED_MODULE_6__["OrdersPage"]]
    })
], OrdersPageModule);



/***/ }),

/***/ "./src/app/pages/orders/orders.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/orders/orders.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-list {\n  border: 1px solid #cccccc;\n  background: var(--cs-background-secondary) !important;\n  border-radius: 8px;\n  padding: 5px !important;\n}\n\nion-grid {\n  position: relative;\n  z-index: 1;\n}\n\n.schedule_btn {\n  color: #1ae5be;\n}\n\n.icon-vertical {\n  display: inline-block !important;\n}\n\nion-chip {\n  border-color: #1ae5be !important;\n}\n\n.btn-color {\n  border-color: #1ae5be !important;\n  color: var(--cs-icon-color-accent) !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JkZXJzL29yZGVycy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtFQUNBLHFEQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUlBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0FBREo7O0FBSUE7RUFFSSxjQUFBO0FBRko7O0FBT0E7RUFDSSxnQ0FBQTtBQUpKOztBQU9BO0VBQ0ksZ0NBQUE7QUFKSjs7QUFPQTtFQUNJLGdDQUFBO0VBQ0EsNkNBQUE7QUFKSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29yZGVycy9vcmRlcnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWxpc3Qge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcclxuICAgIGJhY2tncm91bmQ6IHZhcigtLWNzLWJhY2tncm91bmQtc2Vjb25kYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgcGFkZGluZzogNXB4ICFpbXBvcnRhbnQ7XHJcblxyXG59XHJcblxyXG5cclxuaW9uLWdyaWQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMTtcclxufVxyXG5cclxuLnNjaGVkdWxlX2J0biB7XHJcbiAgICAvL2JvcmRlcjogMXB4IHNvbGlkICMwMzZmNWE7XHJcbiAgICBjb2xvcjogIzFhZTViZTtcclxuICAgIC8vYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICAvL2JvcmRlci1jb2xvcjogIzFhZTViZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uaWNvbi12ZXJ0aWNhbCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWNoaXAge1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjMWFlNWJlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5idG4tY29sb3Ige1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjMWFlNWJlICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogdmFyKC0tY3MtaWNvbi1jb2xvci1hY2NlbnQsICkgIWltcG9ydGFudDtcclxuXHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/orders/orders.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/orders/orders.page.ts ***!
  \*********************************************/
/*! exports provided: OrdersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPage", function() { return OrdersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _services_schedule_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/schedule.service */ "./src/app/services/schedule.service.ts");





let OrdersPage = class OrdersPage {
    constructor(router, productSer, ActivatedRoute, scheduleSer) {
        this.router = router;
        this.productSer = productSer;
        this.ActivatedRoute = ActivatedRoute;
        this.scheduleSer = scheduleSer;
        this.subscribe_products = [];
        this.product_qty = 1;
        this.ActivatedRoute.params.subscribe((params) => {
            this.cust_id = params['cust_id'];
        });
    }
    ngOnInit() {
        this.vendor_id = localStorage.getItem('vendor_id');
        this.getCustomerSchedule(this.cust_id);
    }
    getCustomerSchedule(cust_id) {
        this.scheduleSer.getItem(cust_id)
            .subscribe((res) => {
            console.log("result", res);
            for (let item of res['customerSchedulers'])
                this.subscribe_products.push(item);
        });
        this.productSer.getItems(this.vendor_id)
            .subscribe((res) => {
            this.data = res;
            let products = res['products'];
            for (let product of products) {
                for (let item of this.subscribe_products) {
                    if (item.product_id === product.id) {
                        product.subscribed = "true",
                            product.product_quantity = item['productschedules'][0].quantity,
                            product.onetime = item.onetime,
                            product.delivery_date = item.start_date;
                    }
                }
            }
            console.log("products", products);
        });
    }
    subscribeProduct(product_id, subscription) {
        this.router.navigate(['/orders/' + this.cust_id + '/' + product_id + '/' + subscription]);
    }
    editSchedule(cust_id, product_id) {
        this.router.navigate(['orders/edit/' + cust_id + '/' + product_id]);
    }
    incrementQty() {
        this.data['rate'] = this.data['rate'] / this.product_qty;
        this.product_qty += 1;
        this.data['rate'] = this.data['rate'] * this.product_qty;
        console.log("data", this.data['rate']);
    }
    decrementQty() {
        if (this.product_qty - 1 < 1) {
            this.product_qty = 1;
        }
        else {
            this.data['rate'] = this.data['rate'] / this.product_qty;
            this.product_qty -= 1;
            this.data['rate'] = this.data['rate'] * this.product_qty;
            console.log("data", this.data['rate']);
        }
    }
};
OrdersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_schedule_service__WEBPACK_IMPORTED_MODULE_4__["ScheduleService"] }
];
OrdersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-orders',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./orders.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/orders/orders.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./orders.page.scss */ "./src/app/pages/orders/orders.page.scss")).default]
    })
], OrdersPage);



/***/ })

}]);
//# sourceMappingURL=pages-orders-orders-module.js.map