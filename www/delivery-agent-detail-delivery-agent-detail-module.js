(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["delivery-agent-detail-delivery-agent-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar class=\"transparent\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/delivery-agent\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">{{data.name}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollEvents=\"true\" parallax-header *ngIf=\"data != null\">\r\n  <div class=\"header-image background-secondary\">\r\n    <!-- <div class=\"ion-padding background-size\" [ngStyle]=\"{'background-image': 'url(' + data.headerImage + ')'}\">\r\n    </div> -->\r\n    <div class=\"info-block ion-padding\">\r\n      <ion-thumbnail class=\"border-radius\">\r\n        <img [src]=\"data.avatar\">\r\n      </ion-thumbnail>\r\n      <h1 class=\"text-size-md text-color-primary font-bold\" *ngIf=\"data != null\">{{data.name}}</h1>\r\n    </div>\r\n  </div>\r\n\r\n  <ion-grid class=\"ion-no-padding\" *ngIf=\"data != null\">\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-list class=\"ion-margin-start\">\r\n         <ion-label class=\"ion-padding-start\">\r\n            <!-- Ttile and Subtitle -->\r\n            <h3 class=\"text-size-md text-color-primary font-bold ion-text-wrap\">\r\n              Address</h3>\r\n            <p class=\"text-size-sm text-color-primary font-regular ion-text-wrap\">\r\n              {{data.address}}</p>\r\n         </ion-label>\r\n         <ion-label class=\"ion-padding-start\">\r\n          <!-- Ttile and Subtitle -->\r\n          <h3 class=\"text-size-md text-color-primary font-bold ion-text-wrap\">\r\n            Area</h3>\r\n          <p class=\"text-size-sm text-color-primary font-regular ion-text-wrap\">\r\n            {{data.area}}</p>\r\n       </ion-label>\r\n       <ion-label class=\"ion-padding-start\">\r\n        <!-- Ttile and Subtitle -->\r\n        <h3 class=\"text-size-md text-color-primary font-bold ion-text-wrap\">\r\n          City</h3>\r\n        <p class=\"text-size-sm text-color-primary font-regular ion-text-wrap\">\r\n          Pune</p>\r\n     </ion-label>\r\n       <ion-label class=\"ion-padding-start\">\r\n        <!-- Ttile and Subtitle -->\r\n        <h3 class=\"text-size-md text-color-primary font-bold ion-text-wrap\">\r\n          Zipcode</h3>\r\n        <p class=\"text-size-sm text-color-primary font-regular ion-text-wrap\">\r\n          12345</p>\r\n     </ion-label>\r\n        </ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail-routing.module.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail-routing.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: DeliveryAgentDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryAgentDetailPageRoutingModule", function() { return DeliveryAgentDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _delivery_agent_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./delivery-agent-detail.page */ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.ts");




const routes = [
    {
        path: '',
        component: _delivery_agent_detail_page__WEBPACK_IMPORTED_MODULE_3__["DeliveryAgentDetailPage"]
    }
];
let DeliveryAgentDetailPageRoutingModule = class DeliveryAgentDetailPageRoutingModule {
};
DeliveryAgentDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DeliveryAgentDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.module.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.module.ts ***!
  \********************************************************************************************/
/*! exports provided: DeliveryAgentDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryAgentDetailPageModule", function() { return DeliveryAgentDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _delivery_agent_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./delivery-agent-detail-routing.module */ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail-routing.module.ts");
/* harmony import */ var _delivery_agent_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./delivery-agent-detail.page */ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.ts");







let DeliveryAgentDetailPageModule = class DeliveryAgentDetailPageModule {
};
DeliveryAgentDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _delivery_agent_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["DeliveryAgentDetailPageRoutingModule"]
        ],
        declarations: [_delivery_agent_detail_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryAgentDetailPage"]]
    })
], DeliveryAgentDetailPageModule);



/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5LWFnZW50L2RlbGl2ZXJ5LWFnZW50LWRldGFpbC9kZWxpdmVyeS1hZ2VudC1kZXRhaWwucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.ts ***!
  \******************************************************************************************/
/*! exports provided: DeliveryAgentDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeliveryAgentDetailPage", function() { return DeliveryAgentDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let DeliveryAgentDetailPage = class DeliveryAgentDetailPage {
    constructor() {
        this.data = {
            'toolbarTitle': 'Profile',
            "headerImage": "assets/imgs/background/15.jpg",
            "avatar": "assets/imgs/products/boy-user.jpg",
            "name": "Ramesh Vilas Sathe",
            "address": "Shaniwar Peth, Pune, Maharashtra 411030",
            "area": "Shaniwar Peth, Pune"
        };
    }
    ngOnInit() {
    }
};
DeliveryAgentDetailPage.ctorParameters = () => [];
DeliveryAgentDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-delivery-agent-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./delivery-agent-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./delivery-agent-detail.page.scss */ "./src/app/pages/delivery-agent/delivery-agent-detail/delivery-agent-detail.page.scss")).default]
    })
], DeliveryAgentDetailPage);



/***/ })

}]);
//# sourceMappingURL=delivery-agent-detail-delivery-agent-detail-module.js.map