(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-product-add-product-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/add-product/add-product.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/add-product/add-product.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/product\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-capitalize\">Add Product</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <form [formGroup]=\"frmFG\" (ngSubmit)=\"submit(frmFG.value)\">\r\n      <ion-row class=\"ion-padding-top ion-padding-bottom\">\r\n\r\n        <ion-col size=\"12\" class=\"ion-align-self-start ion-no-padding\">\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-col size=\"4\" class=\"ion-margin-start\">\r\n              <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap\"> Upload\r\n                Product </ion-label>\r\n            </ion-col>\r\n            <ion-col size=\"4\" class=\"ion-margin-start\">\r\n              <!--  <input type=\"file\" (change)=\"loadImageFromDevice($event)\" id=\"file-input\" accept=\"image/png, image/jpeg\"> -->\r\n              <input id=\"fileInput\" formControlName=\"file\" type=\"file\" (change)=\"readUrl($event)\" accept=\"image/*\" />\r\n              <label for=\"fileInput\" icon-only ion-button>\r\n                <ion-icon name=\"image-outline\" size=\"large\"></ion-icon>\r\n              </label>\r\n            </ion-col>\r\n            <ion-col size=\"4\" class=\"image-upload\">\r\n              <img *ngIf=\"!url1==true\">\r\n              <img [src]=\"url1\" height=\"70\">\r\n            </ion-col>\r\n\r\n          </ion-item>\r\n        </ion-col>\r\n        <ion-col size=\"12\" class=\"ion-align-self-start ion-no-padding\">\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Product ID\r\n            </ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"\r\n              formControlName=\"SKU\">\r\n            </ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Product Title</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-capitalize ion-text-wrap\" type=\"text\"\r\n              [placeholder]=\"\" formControlName=\"title\"></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">Price\r\n              in (Rs.)</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" [placeholder]=\"\"\r\n              formControlName=\"rate\"></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap ion-text-wrap ion-margin-start\"\r\n              position=\"fixed\">Unit</ion-label>\r\n            <ion-select formControlName=\"unit\" [interfaceOptions]=\"customActionSheetOptions\" interface=\"action-sheet\"\r\n              class=\"text-size-sm text-color-primary ion-text-wrap ion-padding-start ion-margin-start popvert\">\r\n              <ion-select-option *ngFor=\"let item of units\" [value]=\"item.unit\">{{item.unit}}</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Quantity</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"number\" [placeholder]=\"\"\r\n              formControlName=\"quantity\"></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item\r\n            class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\"\r\n            lines=\"none\">\r\n            <ion-label class=\"text-size-sm ion-text-capitalize ion-text-wrap required ion-margin-start\">\r\n              Description</ion-label>\r\n            <ion-input class=\"text-size-sm text-color-primary ion-text-wrap\" type=\"text\" [placeholder]=\"\"\r\n              formControlName=\"description\"></ion-input>\r\n          </ion-item>\r\n          <!-- <ion-item class=\"ion-margin-bottom ion-margin-end box-shadow ion-no-padding background-secondary border-radius-right input\" lines=\"none\">\r\n         <ion-label class=\"ion-text-wrap ion-margin-start\" position=\"fixed\" >Delivery Agent</ion-label>\r\n         <ion-select\r\n                formControlName=\"delivery_agent\"\r\n                class=\"popvert\"\r\n                [interfaceOptions]=\"customActionSheetOptions\"\r\n                interface=\"action-sheet\"\r\n                class=\"ion-padding-start ion-margin-start\">\r\n                <ion-select-option *ngFor=\"let item of delivery_agent\" [value]=\"item\">{{item}}</ion-select-option>\r\n              </ion-select>\r\n            </ion-item> -->\r\n          <!-- Add Product -->\r\n          <!-- <ion-item class=\"ion-margin-bottom ion-margin-end  ion-no-padding background-secondary \" lines=\"none\">\r\n          <ion-button [routerLink]=\"['/orders']\" class=\"default-button border-radius ion-text-capitalize ion-margin-start ion-margin-top\" expand=\"block\">\r\n            Set Order\r\n         </ion-button>\r\n         </ion-item> -->\r\n          <ion-button class=\"default-button border-radius-right ion-text-capitalize ion-margin-end ion-margin-top\"\r\n            [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n            Save\r\n          </ion-button>\r\n        </ion-col>\r\n\r\n      </ion-row>\r\n    </form>\r\n  </ion-grid>\r\n</ion-content>\r\n<!-- <ion-footer>\r\n  <ion-toolbar>\r\n    <ion-button item-end class=\"default-button border-radius-right ion-text-capitalize ion-margin-end \"\r\n      [disabled]=\"!frmFG.valid\" expand=\"block\" type=\"submit\">\r\n      Save\r\n    </ion-button>\r\n\r\n  </ion-toolbar>\r\n</ion-footer> -->");

/***/ }),

/***/ "./src/app/pages/product/add-product/add-product-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/product/add-product/add-product-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: AddProductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddProductPageRoutingModule", function() { return AddProductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _add_product_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-product.page */ "./src/app/pages/product/add-product/add-product.page.ts");




const routes = [
    {
        path: '',
        component: _add_product_page__WEBPACK_IMPORTED_MODULE_3__["AddProductPage"]
    }
];
let AddProductPageRoutingModule = class AddProductPageRoutingModule {
};
AddProductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddProductPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/product/add-product/add-product.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/product/add-product/add-product.module.ts ***!
  \*****************************************************************/
/*! exports provided: AddProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddProductPageModule", function() { return AddProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _add_product_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-product-routing.module */ "./src/app/pages/product/add-product/add-product-routing.module.ts");
/* harmony import */ var _add_product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-product.page */ "./src/app/pages/product/add-product/add-product.page.ts");







let AddProductPageModule = class AddProductPageModule {
};
AddProductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _add_product_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddProductPageRoutingModule"]
        ],
        declarations: [_add_product_page__WEBPACK_IMPORTED_MODULE_6__["AddProductPage"]]
    })
], AddProductPageModule);



/***/ }),

/***/ "./src/app/pages/product/add-product/add-product.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/product/add-product/add-product.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-grid {\n  height: 100%;\n}\n\n#fileInput {\n  position: absolute;\n  opacity: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC9hZGQtcHJvZHVjdC9hZGQtcHJvZHVjdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FBQ0Y7O0FBR0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7QUFBRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb2R1Y3QvYWRkLXByb2R1Y3QvYWRkLXByb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWdyaWQge1xyXG4gIGhlaWdodDogMTAwJTtcclxuXHJcbn1cclxuXHJcbiNmaWxlSW5wdXQge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvcGFjaXR5OiAwO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/product/add-product/add-product.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/product/add-product/add-product.page.ts ***!
  \***************************************************************/
/*! exports provided: AddProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddProductPage", function() { return AddProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../notifications/notify.service */ "./src/app/notifications/notify.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






let AddProductPage = class AddProductPage {
    constructor(fb, productSer, notify, router) {
        this.fb = fb;
        this.productSer = productSer;
        this.notify = notify;
        this.router = router;
        //instanceOfFileReader.readAsDataURL(blob);
        this.data = { 'image': 'assets/imgs/products/Cow milk.png' };
    }
    ngOnInit() {
        this.formBuilder();
        this.getUnits();
    }
    formBuilder() {
        this.frmFG = this.fb.group({
            file: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            SKU: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            rate: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            unit: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            quantity: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
    }
    getUnits() {
        this.productSer.getUnits()
            .subscribe((res) => {
            this.units = res['units'];
        });
    }
    submit() {
        var data = this.frmFG.value;
        data.vendor_id = localStorage.getItem("vendor_id");
        data.title = this.capitalization(data.title);
        console.log("image", this.url1);
        data.file = this.url1;
        this.productSer.addItem(data)
            .subscribe((res) => {
            console.log("data", res);
            if (res) {
                this.notify.success('Add product successfully');
                this.router.navigate(['/product']);
            }
            else {
                this.notify.error('Please try again');
            }
        }, (err) => {
            this.notify.error('Please try again');
            console.log("error", err);
        });
    }
    /*
    loadImageFromDevice(event) {
    
      const file = event.target.files[0];
    
      const reader = new FileReader();
    
      reader.readAsArrayBuffer(file);
    
      reader.onload = () => {
    
        // get the blob of the image:
        let blob: Blob = new Blob([new Uint8Array((reader.result as ArrayBuffer))]);
    
        // create blobURL, such that we could use it in an image element:
        let blobURL: string = URL.createObjectURL(blob);
    
      };
    
      reader.onerror = (error) => {
    
        //handle errors
    
      };
    };
     */
    capitalization(input) {
        return (!!input) ? input.split(' ').map(function (wrd) { return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase(); }).join(' ') : '';
    }
    readUrl(event) {
        console.log('readUrl');
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onload = (event) => {
                this.url1 = event.target.result;
                console.log("image", this.url1);
            };
        }
    }
};
AddProductPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _services_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"] },
    { type: _notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
AddProductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-product.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product/add-product/add-product.page.html")).default,
        providers: [_notifications_notify_service__WEBPACK_IMPORTED_MODULE_4__["NotifyService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-product.page.scss */ "./src/app/pages/product/add-product/add-product.page.scss")).default]
    })
], AddProductPage);



/***/ })

}]);
//# sourceMappingURL=add-product-add-product-module.js.map