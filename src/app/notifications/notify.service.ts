import { Injectable} from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable()
export class NotifyService {
    constructor(public toastCtrl: ToastController) { }

    async info(message: string, title?: string, options?: any) {
        const toast = await this.toastCtrl.create({
            header: title,
            message: message,
            duration: 2000,
            color: 'secondary'
          });
          toast.present();
    }

    async success(message: string, title?: string, options?: any) {
        const toast = await this.toastCtrl.create({
            header: title,
            message: message,
            duration: 2000,
            color: 'primary'
          });
          toast.present();
    }

    async warn(message: string, title?: string, options?: any) {
        const toast = await this.toastCtrl.create({
            header: title,
            message: message,
            duration: 2000,
            color: 'warning'
          });
          toast.present();
    }

    async error(message: string, title?: string, options?: any) {
        const toast = await this.toastCtrl.create({
            header: title,
            message: message,
            duration: 2000,
            color: 'danger'
          });
          toast.present();
    }
}