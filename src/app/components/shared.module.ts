import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { AgmCoreModule } from '@agm/core';


import { AnimateItemsDirective } from '../directives/animate-items.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgmCoreModule.forRoot({ apiKey: '' })
  ],
  declarations: [
    AnimateItemsDirective],
  exports: [ ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
