import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [];
  user = {
    'role':'',
    'image': '',
    'name': '',
  }
  isEnabledRTL: boolean = false;

  constructor(
    private platform: Platform,
    private navController: NavController
  ) {
    this.isEnabledRTL = localStorage.getItem('isEnabledRTL') == "true";
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.setRTL();
    });
  }

  setRTL() {
    document.getElementsByTagName('html')[0]
            .setAttribute('dir', this.isEnabledRTL  ? 'rtl': 'ltr');
  }

  openPage(page) {
    this.navController.navigateRoot([page.url], {});
  }
}
