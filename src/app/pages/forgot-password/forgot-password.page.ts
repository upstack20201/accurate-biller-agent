import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { NotifyService } from '../../notifications/notify.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
  providers: [NotifyService]
})
export class ForgotPasswordPage implements OnInit {
  frmFG: FormGroup;
  constructor(private fb: FormBuilder,
    private notify: NotifyService,
    private authSer: AuthService,
    private router: Router,) { }

  ngOnInit() {
    this.formBuilder();
  }

  formBuilder() {
    this.frmFG = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }

  frmSubmit(form: any, isValid: boolean) {
    console.log(form.email);
    this.authSer
      .forgotPsw(form.email)
      .subscribe(
        result => {
          console.log("result", result);

          if (result) {
            this.notify.success("New password will be sent to your email address");
          }
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 8000);
        },
        error => {
          this.notify.error("Please check username");

        }
      );
  }
}
