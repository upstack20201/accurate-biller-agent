import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ProfileService } from '../../services/profile-service';
import { NotifyService } from '../../notifications/notify.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
  providers: [NotifyService]
})
export class ProfilePage implements OnInit {
  frmFG: FormGroup;
  public user_id;
  public user_name;
  public url1;
  public profile_img;
  data = { image: "assets/imgs/avatar/21.jpg" };
  constructor(private fb: FormBuilder,
    private profileSer: ProfileService,
    private notify: NotifyService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { 
      activatedRoute.params.subscribe(val => {
        this.user_id = localStorage.getItem("vendor_id");
        this.getUser(this.user_id);
      });
    }

  ngOnInit() {
    this.formBuilder();
  }

  formBuilder() {
    this.frmFG = this.fb.group({
      file: new FormControl(''),
      firstName: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      lastName: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      email: new FormControl('', [Validators.required,
      Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$"),]),
      mobile: new FormControl('', [Validators.required, Validators.pattern("^[0-9]{10}")]),
      address: new FormControl('', Validators.required),
      //city: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      zipcode: new FormControl('', [Validators.required, Validators.pattern("^[0-9]{6}")]),
    })
  }

  getUser(id) {
    this.profileSer.getItem(id)
      .subscribe((res) => {
        console.log("user", res)
        let user = res['user']
        this.user_name = user.name;
        user.name = user.name.split(" ")
        this.url1 = user.profilepic;
        this.frmFG.get("file").setValue( user.profilepic);
        this.frmFG.get("firstName").setValue( user.name[0]);
        this.frmFG.get("lastName").setValue( user.name[1]);
        this.frmFG.get("email").setValue( user.email);
        this.frmFG.get("mobile").setValue( user.mobile);
        this.frmFG.get("address").setValue( user.address);
        this.frmFG.get("zipcode").setValue( user.zipcode );
      })
  }

  submit() {
    var data = this.frmFG.value;
    data.name = this.capitalization(data.firstName) + ' ' + this.capitalization(data.lastName);
    data.file = this.url1;
    console.log("data", data)
    this.profileSer.updateItem(data, this.user_id)
      .subscribe((res) => {
        if (res) {
          this.getUser(this.user_id)
          this.notify.success('Update profile successfully')
          this.router.navigate(['/profile']);
        } else {
          this.notify.error('Please try again')
        }
      },
        (err) => {
          this.notify.error('Please try again')
          console.log("error", err)
        })
  }

  readUrl(event: any) {
    console.log('readUrl', event.target.files[0]);
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url1 = event.target.result;
        this.profile_img = this.url1;
      }
    }
  }

  capitalization(input){
    return (!!input) ? input.split(' ').map(function(wrd){return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase();}).join(' ') : '';
    }  
}
