
import { Component,OnInit  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { MenuService } from '../../services/menu-service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit  {
  public appPages = [];
  user = {
    'role':'',
    'image': '',
    'name': '',
  }
  isEnabledRTL: boolean = false;

  constructor(private platform: Platform,
              private http: HttpClient,
              private menuService: MenuService,
              private navController: NavController) {
    this.isEnabledRTL = localStorage.getItem('isEnabledRTL') == "true";
    //console.log(JSON.stringify(exportService.export()));
    this.getMenu();
    this.initializeApp();
    this.user.name = localStorage.getItem("name")
    this.user.role = localStorage.getItem("role")
  }
     ngOnInit() {
      this.getMenu();
  }   
 
  getMenu(){
    var role = localStorage.getItem("role")
    console.log("role", role)
    this.appPages = this.menuService.getAllThemes("Agent");

  }

  initializeApp() {
    this.platform.ready().then(() => {
      /* this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#000000'); */
      //this.splashScreen.hide();
      this.setRTL();
    });
  }

  setRTL() {
    document.getElementsByTagName('html')[0]
            .setAttribute('dir', this.isEnabledRTL  ? 'rtl': 'ltr');
  }

  openPage(page) {
    this.navController.navigateRoot([page.url], {});
  }
}

