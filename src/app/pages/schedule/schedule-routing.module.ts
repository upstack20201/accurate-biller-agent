import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SchedulePage } from './schedule.page';

const routes: Routes = [
  {
    path: '',
    component: SchedulePage
  },
  {
    path: 'check-schedule',
    loadChildren: () => import('./check-schedule/check-schedule.module').then( m => m.CheckSchedulePageModule)
  },
  {
    path: 'edit',
    loadChildren: () => import('./check-schedule/edit-schedule/edit-schedule.module').then( m => m.EditSchedulePageModule)
  },
  {
    path: ':id',
    loadChildren: () => import('./check-schedule/details-schedule/details-schedule.module').then( m => m.DetailsSchedulePageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SchedulePageRoutingModule {}
