import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsSchedulePage } from './details-schedule.page';

describe('DetailsSchedulePage', () => {
  let component: DetailsSchedulePage;
  let fixture: ComponentFixture<DetailsSchedulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsSchedulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsSchedulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
