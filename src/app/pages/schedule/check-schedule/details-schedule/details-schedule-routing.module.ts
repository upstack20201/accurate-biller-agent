import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsSchedulePage } from './details-schedule.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsSchedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsSchedulePageRoutingModule {}
