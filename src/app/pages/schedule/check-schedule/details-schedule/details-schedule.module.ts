import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsSchedulePageRoutingModule } from './details-schedule-routing.module';

import { DetailsSchedulePage } from './details-schedule.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsSchedulePageRoutingModule
  ],
  declarations: [DetailsSchedulePage]
})
export class DetailsSchedulePageModule {}
