import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details-schedule',
  templateUrl: './details-schedule.page.html',
  styleUrls: ['./details-schedule.page.scss'],
})
export class DetailsSchedulePage implements OnInit {
  onLeave: boolean = true;
  data = {
    'toolbarTitle': 'List big image',
    "header": "Catalogue",
    "items": [
      {
        "id": 1,
        "title": "Black dualshock",
        "description": "katraj",
        "image": "assets/imgs/products/boy-user.jpg",
        "expandItems": [
          {
            "id": 1,
            "title": "Buffalo Milk",
            "description": "500 ML",
            "image": "assets/imgs/products/milk.jpg",
            "details": "Rs. 36"
          },
          {
            "id": 2,
            "title": "Curd",
            "description": "400 GM",
            "image": "assets/imgs/avatar/6.jpg",
            "details": "Rs. 30"
          }
        ]
      },
      {
        "id": 2,
        "title": "Keyboard",
        "description": "deccan",
        "image": "assets/imgs/products/girl-user.png",
        "expandItems": [
          {
            "id": 1,
            "title": "Buffalo Milk",
            "description": "500 ML",
            "image": "assets/imgs/avatar/3.jpg",
            "details": "Rs. 36"
          },
          {
            "id": 2,
            "title": "Taaza Paneer",
            "description": "200 GM",
            "image": "assets/imgs/avatar/7.jpg",
            "details": "Rs. 79"
          },
        ]
      },
      {
        "id": 3,
        "title": "Watch Black",
        "description": "karve nagar",
        "image": "assets/imgs/products/boy-user.jpg",
        "expandItems": [
          {
            "id": 1,
            "title": "Cow Milk",
            "description": "500 ML",
            "image": "assets/imgs/avatar/2.jpg",
            "details": "Rs. 30"
          },
        ]
      },
    ]
  };

  constructor() { }

  ngOnInit() {
  }

  toggleGroup(group: any) {
    if (event) {
      event.stopPropagation();
    }
    group.show = !group.show;
  }

}
