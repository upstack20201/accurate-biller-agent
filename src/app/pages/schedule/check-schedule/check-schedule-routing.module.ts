import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckSchedulePage } from './check-schedule.page';

const routes: Routes = [
  {
    path: '',
    component: CheckSchedulePage
  },
  {
    path: ':agent_id/order-status',
    loadChildren: () => import('./order-status/order-status.module').then( m => m.OrderStatusPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckSchedulePageRoutingModule {}
