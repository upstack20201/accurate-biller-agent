import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
// import { DeliveryAgentService } from '../../../services/delivery-agent.service';

@Component({
  selector: 'app-check-schedule',
  templateUrl: './check-schedule.page.html',
  styleUrls: ['./check-schedule.page.scss'],
})
export class CheckSchedulePage implements OnInit {
  data;
  vendor_id;
  // data = {
  //   'toolbarTitle': 'List big image',
  //     "header": "Catalogue",
  //     "items": [
  //       {
  //         "id": 1,
  //         "title": "Ramesh Vilas Sathe",
  //         "description": "Shaniwar Peth, Pune, Maharashtra 411030",
  //         "image": "assets/imgs/products/boy-user.jpg",
  //       },
  //       {
  //         "id": 2,
  //         "title": "Ganesh Shrikant Ghadi",
  //         "description": " Service Road, Vivek Nagar, Akurdi, Pimpri-Chinchwad, Pune, Maharashtra 411035",
  //         "image": "assets/imgs/products/boy-user.jpg",
  //       },
  //       {
  //         "id": 3,
  //         "title": "Raghav Milind Mane",
  //         "description": "Katraj Dairy, Pune – Satara Road, Opp. Rajiv Gandhi Udyan, Katraj, Pune – 411046",
  //         "image": "assets/imgs/products/boy-user.jpg",
  //       },
  //       {
  //         "id": 4,
  //         "title": "Ganesh Raghav Patil",
  //         "description": "Tech Park One, Tower E, 191 Yerwada, Pune - 411 006",
  //         "image": "assets/imgs/products/boy-user.jpg",
  //       },
        
  //     ]
  //   }

  constructor( private router: Router,
              //  private deliveryAgentSer: DeliveryAgentService, 
               ) { }

  ngOnInit() {
    this.vendor_id = localStorage.getItem("vendor_id")
    this.getAgents(this.vendor_id);
  }

  getAgents(id){
    //setTimeout(() => {
    // this.deliveryAgentSer.getItems(id)
    //   .subscribe((res) => {
    //       this.data = res;
    //       this.data.agents[1].leave = true
    //       console.log("data",  res)
    //   })
    // },1000);
  }

  changeStatus(agent_id){
    this.router.navigate(['/schedule/check-schedule/' + agent_id + '/order-status'])
  }

  sheduleDetails(id){
    this.router.navigate(['schedule/' + id]);
  }

  sheduleEdit(id){
    this.router.navigate(['schedule/edit']);
  }
  
}
