import { Component, OnInit,ViewChild  } from '@angular/core';
import { ActivatedRoute, Params, Router} from '@angular/router';
import { Validator, FormBuilder, FormGroup, FormControl} from '@angular/forms'
import { ScheduleService } from '../../../../services/schedule.service';
import { AlertController } from "@ionic/angular";
import { DatePipe } from '@angular/common';
import { NotifyService } from '../../../../notifications/notify.service'; 
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.page.html',
  styleUrls: ['./order-status.page.scss'],
  providers: [DatePipe, NotifyService]
})
export class OrderStatusPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  frmFG: FormGroup;
  id;
  vendor_id;
  agent_id;
  data;
  public today = new Date();
  
   loadData(event) {
    setTimeout(() => {
      //console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      /* if (this.data.length == 1000) {
        event.target.disabled = true;
      } */
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  constructor(private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private scheduleSer: ScheduleService,
              private alertCtrl: AlertController,
              private notify: NotifyService, ) { 
                this.activatedRoute.params.subscribe((params: Params) => {
                  this.agent_id = params['agent_id'];
                })
                activatedRoute.params.subscribe(val => {
                  this.vendor_id = localStorage.getItem("vendor_id")
                  this.getOrderList();
                });
              }

  ngOnInit() {
    this.formBuilder()
  }

  formBuilder(){
    this.frmFG = this.fb.group({
      status: new FormControl('')
    })
  }

  getOrderList(){
    var order_date = this.datePipe.transform(this.today, 'yyyy-MM-dd');
    var detail = {
      vendor_id: this.vendor_id,
      agent_id: this.agent_id,
      status: 'scheduled',
      order_date: order_date
    }
    // this.deliveryAgentSer.getAgentwiseCustomer(detail)
    // .subscribe((res) => {
    //   this.data = res['agentCustomers']
    //   console.log("data", this.data)
    // })
  }

  onStatusChange(data){
    const status = this.frmFG.get("status").value;
    let details = {
      quantity : data.quantity,
      status : status,
      delivered_by : this.agent_id
    }

    this.scheduleSer.changeOrderStatus(data.id,details)
    .subscribe((res) => {
    this.notify.success("Update status successfully")
    this.frmFG.get("status").setValue('');
    this.getOrderList();
    },
    (err) => {
      this.notify.error("Please try again")
    })
    
  }

}
