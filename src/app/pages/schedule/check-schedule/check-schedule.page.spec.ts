import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckSchedulePage } from './check-schedule.page';

describe('CheckSchedulePage', () => {
  let component: CheckSchedulePage;
  let fixture: ComponentFixture<CheckSchedulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckSchedulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckSchedulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
