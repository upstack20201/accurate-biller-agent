import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl} from "@angular/forms";
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { NotifyService } from '../../notifications/notify.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [NotifyService]
})
export class LoginPage implements OnInit {
  frmFG: FormGroup;
  userName;
  psw;
  password: string = 'password';
  passwordIcon: string = 'eye-off';
  data = {
    'headerTitle'     : 'Login',
    "background"      : "assets/imgs/background/30.jpg",
    "forgotPassword"  : "Forgot password?",
    "labelUsername"   : "USERNAME",
    "labelPassword"   : "PASSWORD",
    "title"           : "Gausampada",
    "subtitle"        : "Welcome To",
    "username"        : "Enter your username",
    "password"        : "Enter your password",
    "login"           : "LOG IN",
    "facebookLogin"   : "Login with",
    "logo"            : "assets/imgs/logo/logo.png",
  }
  constructor( private fb: FormBuilder,
               private router: Router, 
               private authser: AuthService,
               private notify: NotifyService ) { }

  ngOnInit() {
    this.userName = localStorage.getItem("userName");
    this.psw = localStorage.getItem("password");

    this.formBuilder();

    if(this.userName && this.psw){
      this.frmFG.get("email").setValue(this.userName);
      this.frmFG.get("password").setValue(this.psw);
      }
  }

  formBuilder(){
    this.frmFG = this.fb.group({
      email: new FormControl(null,  [Validators.required,
        Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$"),
      ]),
      password: new FormControl("", Validators.required)
    })
  }

  hideShowPassword() {
    this.password = this.password === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
}

onForgotPassword(){
  this.router.navigate(['/forgot-password'])
}

  onLoginFunc(form: any){
    var data = this.frmFG.value;
    this.router.navigate(['/dashboard']);
    //this.authser.login(data)
    // .subscribe((res) => {
    //   if(!res){
    //     this.notify.error("Login failed. Please try again.");
    //     this.router.navigate(['/login']);
    //   }
    //   if(res){
    //     this.notify.success("Logged in successfully");
    //     this.router.navigate(['/dashboard']);
    //     localStorage.setItem("userName", form.email);
    //     localStorage.setItem("password", form.password);
    //     }else{
    //       this.notify.error("Sorry!! These credentials cannot be used");
    //     }
    // },
    // (err) => {
    //   this.notify.error('Please check the login credentials')
    // })
  }

}
