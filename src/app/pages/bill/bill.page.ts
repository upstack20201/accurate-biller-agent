import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from "@ionic/angular";
//import { NotifyService} from '../../notifications/notify.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.page.html',
  styleUrls: ['./bill.page.scss'],
})
export class BillPage implements OnInit {
   data;
    vendor_id;
  
  constructor( private router: Router,
               private alertCtrl: AlertController,
               //private notify: NotifyService,
               private activatedRoute: ActivatedRoute) {
                activatedRoute.params.subscribe(val => {
                  this.vendor_id = localStorage.getItem("vendor_id")
                  this.getCustomers(this.vendor_id);
                });
                }

  ngOnInit() { }

  getCustomers(id){
    //  setTimeout(() => {
    //   this.customerSer.getItems(id)
    //   .subscribe((res) => {
    //       this.data = res;
    //       console.log("data",  this.data)
    //   })
    // },1000);
  }

  addCustomer(){this.router.navigate(['/customers/add'])}

  onItemClickFunc(item, ev){
    console.log("item", item)
    console.log("event", ev)
  }

  customerDetail(id){
    this.router.navigate(['/customers/'+ id +'/detail'])
  }

  edit(id){
    this.router.navigate(['/customers/'+ id +'/edit'])
  }

  async remove(data){console.log("data", data)
        let alert = await this.alertCtrl.create({
            header: 'Remove customer',
            subHeader: 'Do you want to remove this customer?',
            buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
                },
                {
                text: 'Remove',
                handler: () => {
                  //  this.customerSer.deleteItem(data)
                  //  .subscribe((res) => {
                  //   //this.notify.success("Customer deleted successfully")
                  //   this.getCustomers(this.vendor_id);
                  //  },
                  //  (err) => {
                  //   // this.notify.error("Please try again")
                  //  })
                }
                }
            ]
        });
        await alert.present();
      } 
}
