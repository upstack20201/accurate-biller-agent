
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
/* import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx'; */

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

/* import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth"; */

import { AppSettings } from './services/app-settings';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

//import { IonicStorageModule } from '@ionic/storage';

/* import { SuperTabsModule } from '@ionic-super-tabs/angular';*/
import { ServiceWorkerModule } from '@angular/service-worker'; 
import { environment } from '../environments/environment';

//Auth interceptor import
import { AuthInterceptor } from './services/auth.interceptor';
import { HomePage } from './pages/home/home.page';

@NgModule({
  declarations: [AppComponent, HomePage],
  entryComponents: [],
  imports: [
    /* AngularFireModule.initializeApp(AppSettings.FIREBASE_CONFIG),
    AngularFireDatabaseModule, AngularFireAuthModule, */
    BrowserModule, HttpModule, HttpClientModule,
    IonicModule.forRoot(),
    //IonicStorageModule.forRoot(),
   /*  SuperTabsModule.forRoot(), */
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
   /*  StatusBar, BarcodeScanner,
    SplashScreen, */
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
