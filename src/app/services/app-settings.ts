export const AppSettings = {
    'IS_FIREBASE_ENABLED': false,
    'SHOW_START_WIZARD': true,
    'SUBSCRIBE': true,
    'FIREBASE_CONFIG': {
      'apiKey': "AIzaSyCPAqlz26L6i5kneXPC0fpYza9qsr-7OiY",
      'authDomain': "ionic-coppy-thema.firebaseapp.com",
      'databaseURL': "https://ionic-coppy-thema.firebaseio.com",
      'projectId': "ionic-coppy-thema",
      'storageBucket': "ionic-coppy-thema.appspot.com",
      'messagingSenderId': "357007187485",
      'appId': "1:357007187485:web:5ab4d659b20be5953130f4",
      'measurementId': "G-QMR0VP80ZZ"
    }
};
