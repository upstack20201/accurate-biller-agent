import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  base_href = environment.apiURL;
  createSchedule = 'customerSchedulers';
  schedulers = 'customerSchedulers/';
  getSchedule = 'customersSchedules/';
  getProdSchedule = 'getcustomerProductSchedule/';
  signes = '/';
  updateSchedule = 'customerTransactions/';

  constructor(private http: HttpClient) { }

  getItem(id){
    return this.http.get(`${this.base_href}${this.getSchedule}${id}`)
  }

  addItem(data){
    return this.http.post(`${this.base_href}${this.createSchedule}`, this.getData(data, 'subscription'))
  }

  addOneItem(data){
    return this.http.post(`${this.base_href}${this.createSchedule}`, this.getData(data, 'once'))
  }

  getProductSchedule(data){
    return this.http.get(`${this.base_href}${this.getProdSchedule}${data.cust_id}${this.signes}${data.product_id}${this.signes}${data.vendor_id}`)
  }

  deleteItem(data){
    return this.http.delete(`${this.base_href}${this.schedulers}${data.cust_id}${this.signes}${data.product_id}${this.signes}${data.vendor_id}`)
  }

  changeOrderStatus(schedule_id,data){
    const formData = new FormData();
    formData.append("quantity", data.quantity),
    formData.append("status", data.status),
    formData.append("delivered_by", data.agent_id),
    formData.append("method", 'PUT')
    return this.http.post(`${this.base_href}${this.updateSchedule}${schedule_id}`, formData)
  }

  getData(data,type){
    const formData = new FormData();
    formData.append("customer_id", data.cust_id),
    formData.append("product_id", data.product_id),
    formData.append("unit", data.unit),
    formData.append("vendor_id", data.vendor_id),
    formData.append("weekday", data.weekdays),
    formData.append("quantity", data.quantity),
    formData.append("start_date", data.start_date)
    if(type === 'subscription'){
      formData.append("onetime", "false")
    }
    if(type === 'once'){
      formData.append("onetime", "true")
    }
    return formData;
  }
}
