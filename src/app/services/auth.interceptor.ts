import { HTTP_INTERCEPTORS, HttpEvent, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  user: any;
  constructor(
    private authSer: AuthService,
    private router: Router,
  ) {  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(request);
    
    if (!request) {
      console.log(request);
        return next.handle(request);
    }
        let token = this.authSer.getToken();
        let authorizedRequest = request;
        var modify_url = request.url.split('/public')
        if (modify_url[1] != '/login' && modify_url[1] != '/password/reset-request') {
            let modified_url = modify_url[0] + "/public/v1/api" + modify_url[1];
            authorizedRequest = request.clone({
                url: modified_url,
                headers: request.headers.set('Authorization', 'Bearer ' + token)
                //.set('Content-Type', 'application/json')
                // .set("User", this.user)
        });
        }
        return next.handle(authorizedRequest);
    }
 }

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];