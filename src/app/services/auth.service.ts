import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Observable,throwError, BehaviorSubject  } from 'rxjs';
import { environment } from '../../environments/environment';
import { map, catchError  } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  base_href = environment.apiURL;   
  loginparam = 'login';
  forgotpsw = 'password/reset-request';

  constructor( private http: HttpClient) {}

  ngOnInit() {

  }

  login(credentials){
     const formData = new FormData();
     formData.append('email', credentials.email);
     formData.append('password', credentials.password)
  return this.http.post(`${this.base_href}${this.loginparam}`, formData)    
  .pipe(
      map((res: any) => {
        localStorage.setItem('token', res.access_token);
        localStorage.setItem('email', res.user.email);
        localStorage.setItem('name', res.user.name);
        localStorage.setItem('company', res.user.company);
        localStorage.setItem('vendor_id', res.user.id);
        localStorage.setItem('mobile', res.user.mobile);
        localStorage.setItem('profilepic', res.user.profilepic);
        localStorage.setItem('role', res.user.role);
        localStorage.setItem('role_id', res.user.role_id);
        return res;
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  forgotPsw(email): Observable<any>{
    const formData = new FormData();
    formData.append('email', email);
    return this.http.post(`${this.base_href}${this.forgotpsw}`, formData)
  }

  getToken(){
    const token = localStorage.getItem('token'); 
    return token; 
  } 
  getUserHeader() { 
    let headers = new HttpHeaders();
    const token = localStorage.getItem('token');   
    headers = headers.set('Authorization', `Bearer ${token}`);
    return headers;
  }
}
