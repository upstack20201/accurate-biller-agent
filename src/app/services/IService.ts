import { NavController } from '@ionic/angular';

export interface IService {
  getAllThemes(role: any): Array<any>;
  getTitle(): string;
}
