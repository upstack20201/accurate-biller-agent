import { IService } from './IService';
//import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
@Injectable({ providedIn: 'root' })
export class MenuService implements IService {

  constructor() { }

  getId = (): string => 'menu';

  getTitle = (): string => 'UIAppTemplate';

  //* Data Set for main menu
  getAllThemes = (role): Array<any> => {
    
      if( role == 'Agent'){
        return [
      {
        'url': 'schedule/check-schedule',
        'title': 'Schedule',
        'theme': 'login',
        'icon': 'icon-timer',
        'listView': false,
        'component': '',
        'singlePage': false
      },
      {
        'url': 'leave',
        'title': 'Leave',
        'theme': 'payment',
        'icon': 'icon-cash',
        'listView': false,
        'component': '',
        'singlePage': false
      },
      {
        'url': 'bill',
        'title': 'Bill',
        'theme': 'payment',
        'icon': 'icon-cash',
        'listView': false,
        'component': '',
        'singlePage': false
      },
      {
        'url': 'login',
        'title': 'Logout',
        'theme': 'login',
        'icon': 'icon-logout',
        'listView': false,
        'component': '',
        'singlePage': false
      }
    ]
    }
  }

  getDataForTheme = (menuItem: any) => {
    return {
      'url': 'profile',
      'background': 'assets/imgs/background/16.jpg',
      'image': 'assets/imgs/logo/logo.png',
      'title': 'Swati Choudhary'
    };
  }

  getEventsForTheme = (menuItem: any): any => {
    return {};
  }

  prepareParams = (item: any) => {
    return {
      title: item.title,
      data: {},
      events: this.getEventsForTheme(item)
    };
  }
}
