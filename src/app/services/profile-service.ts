import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ProfileService  {

    base_href = environment.apiURL;
    user = 'users/';

    constructor(private http: HttpClient) { }

 getItem(id){
    return this.http.get(`${this.base_href}${this.user}${id}`)
  }

  updateItem(data, id){
    return this.http.post(`${this.base_href}${this.user}${id}`, this.getData(data, 'update'))
  }

  getData(data, type){
    const formData = new FormData();
    formData.append("name", data.name),
    formData.append("email", data.email),
    formData.append("mobile", data.mobile),
    formData.append("address", data.address),
    formData.append("zipcode", data.zipcode),
    formData.append('city', '2763'),
    formData.append('state', '22'),
    formData.append('country', 'India')
    if(type === 'update'){
      if(data.file != undefined){
        formData.append("file", data.file)
      }
     formData.append("method", 'PUT')
    }
    return formData;
  }
   
}
